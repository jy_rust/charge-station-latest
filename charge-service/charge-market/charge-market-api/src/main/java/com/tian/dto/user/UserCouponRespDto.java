package com.tian.dto.user;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * {@code @description:} 修改优惠券参数
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024/1/23 9:36
 * {@code @version:} 1.0
 */
@Data
@ApiModel(value = "CouponAddReqDto", description = "新增优惠券参数")
public class UserCouponRespDto {
    @ApiModelProperty("用户优惠券主键id")
    private Long id;
    @ApiModelProperty("用户主键id")
    private Long userId;
    @ApiModelProperty("优惠券主键id")
    private Integer couponId;
    @ApiModelProperty("优惠券优惠券")
    private Date period;
    @ApiModelProperty("用户优惠券状态")
    private Integer status;
    @ApiModelProperty("用户优惠券使用时间")
    private Date useTime;
    @ApiModelProperty("用户优惠券领取时间")
    private Date createTime;
}
