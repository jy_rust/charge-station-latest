package com.tian.enums;

import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * {@code @description:} 优惠券使用类型
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024/1/23 10:56
 * {@code @version:} 1.0
 */
@Getter
public enum CouponUseTypeEnum {
    GENERAL(0, "通用"),
    SPECIAL(1, "专用");
    private final int type;
    private final String desc;

    private static final Map<Integer, CouponUseTypeEnum> couponUseTypeEnumMap = new HashMap<>();

    static {
        for (CouponUseTypeEnum value : CouponUseTypeEnum.values()) {
            couponUseTypeEnumMap.put(value.getType(), value);
        }
    }

    CouponUseTypeEnum(int type, String desc) {
        this.type = type;
        this.desc = desc;
    }

    public static String getDescByType(int type) {
        return couponUseTypeEnumMap.get(type).desc;
    }
}
