package com.tian.enums;

import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * {@code @description:} 我的优惠券状态枚举
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024/1/23 18:05
 * {@code @version:} 1.0
 */
@Getter
public enum CouponTypeEnum {
    INIT(0, "通用"),
    USED(1, "通用"),
    EXPIRED(2, "专用");
    private final int status;
    private final String desc;

    private static final Map<Integer, CouponTypeEnum> COUPON_STATUS_ENUM_MAP = new HashMap<>();

    static {
        for (CouponTypeEnum value : CouponTypeEnum.values()) {
            COUPON_STATUS_ENUM_MAP.put(value.getStatus(), value);
        }
    }

    CouponTypeEnum(int status, String desc) {
        this.status = status;
        this.desc = desc;
    }

    public static String getDesByStatus(int status) {
        return COUPON_STATUS_ENUM_MAP.get(status).desc;
    }
}
