package com.tian.enums;

import lombok.Getter;

/**
 * {@code @description:} 优惠券状态
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-03-25 15:17
 * {@code @version:} 1.0
 */
@Getter
public enum CouponStatusEnum {

    UN_SUBMIT(0, "待提交"),
    UN_APPROVAL(1, "待审核"),
    UN_START(2, "待开始"),
    ACTIVE(3, "活动中"),
    EXPIRED(4, "已过期"),
    DEACTIVATED(5, "已停用"),
    REJECTED(6, "已驳回");

    private final int status;
    private final String desc;

    CouponStatusEnum(int status, String desc) {
        this.status = status;
        this.desc = desc;
    }
}
