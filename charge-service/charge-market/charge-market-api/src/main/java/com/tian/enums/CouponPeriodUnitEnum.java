package com.tian.enums;

import com.tian.util.DateUtils;
import lombok.Getter;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * {@code @description:} 优惠券有效期类型 时、兲、月、年
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024/1/23 10:52
 * {@code @version:} 1.0
 */
@Getter
public enum CouponPeriodUnitEnum {
    HOUR(0, "小时"),
    DAY(1, "天"),
    MONTH(2, "月"),
    YEAR(3, "年");
    private final int unit;
    private final String desc;

    private static final Map<Integer, CouponPeriodUnitEnum> periodUnitEnumMap = new HashMap<>();

    static {
        for (CouponPeriodUnitEnum value : CouponPeriodUnitEnum.values()) {
            periodUnitEnumMap.put(value.getUnit(), value);
        }
    }

    CouponPeriodUnitEnum(int unit, String desc) {
        this.unit = unit;
        this.desc = desc;
    }
    public static String getDescByUnit(int unit) {
        return periodUnitEnumMap.get(unit).desc;
    }

    public static Date caculatePeriodDate(Date date,int period,int unit){
        if(unit==HOUR.unit){
          return DateUtils.addHour(date,period);
        }
        if(unit==DAY.unit){
            return DateUtils.addDay(date,period);
        }
        if(unit==MONTH.unit){
            return DateUtils.addMonth(date,period);
        }
        if(unit==YEAR.unit){
            return DateUtils.addYear(date,period);
        }
        return date;
    }
}
