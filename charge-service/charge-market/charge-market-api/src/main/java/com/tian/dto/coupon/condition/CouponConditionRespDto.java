package com.tian.dto.coupon.condition;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * {@code @description:} 修改优惠券参数
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024/1/23 9:36
 * {@code @version:} 1.0
 */
@Data
@ApiModel(value = "CouponAddReqDto", description = "新增优惠券参数")
public class CouponConditionRespDto {
    @ApiModelProperty("优惠券主键id")
    private Integer id;
    @ApiModelProperty("优惠券条件")
    private Integer condition;
    @ApiModelProperty("优惠券面值")
    private Long faceValue;
    @ApiModelProperty("优惠券条件类型")
    private Integer type;
    @ApiModelProperty("优惠券条件类型")
    private String typeStr;
    @ApiModelProperty("优惠券条件创建时间")
    private Date createTime;
}
