package com.tian.dto.user;

import lombok.Data;

import java.util.Date;

/**
 * {@code @description:} 分页查询我的优惠券
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-05-28 15:15
 * {@code @version:} 1.0
 */
@Data
public class UserCouponPageRespDto {
    private Long id;
    private Long userId;
    private Integer couponId;
    private Date period;
    private Integer status;
    private String statusStr;
    private Date useTime;
    private Date createTime;
}
