package com.tian.dto.coupon.condition;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * {@code @description:} 新增优惠券条件参数
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024/1/23 10:17
 * {@code @version:} 1.0
 */
@Data
@ApiModel(value = "CouponConditionAddReqDto", description = "新增优惠券条件参数")
public class CouponConditionAddReqDto {
    @ApiModelProperty("优惠券条件")
    private Integer condition;
    @ApiModelProperty("优惠券面值")
    private Long faceValue;
    @ApiModelProperty("优惠券条件类型")
    private Integer type;
}
