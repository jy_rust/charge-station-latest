package com.tian.client;

import com.tian.common.CommonResult;
import com.tian.dto.coupon.condition.CouponConditionRespDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * {@code @description:} 优惠券条件
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-03-23 23:08
 * {@code @version:} 1.0
 */
@Component
@FeignClient(contextId = "couponConditionClient", value = "charge-market")
public interface CouponConditionClient {
    @GetMapping("/condition/{id}")
    CommonResult<CouponConditionRespDto> findById(@PathVariable(value = "id") Integer id);

    @GetMapping("/condition/coupon/{couponId}")
    CommonResult<CouponConditionRespDto> findByCouponId(@PathVariable(value = "couponId") Integer id);
}
