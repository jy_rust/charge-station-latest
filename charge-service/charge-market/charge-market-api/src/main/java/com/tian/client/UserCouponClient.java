package com.tian.client;

import com.tian.common.CommonResult;
import com.tian.dto.user.UserCouponRespDto;
import com.tian.dto.user.UserCouponUpdateReqDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * {@code @description:} 用户优惠券相关远程操作
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-03-15 15:07
 * {@code @version:} 1.0
 */
@Component
@FeignClient(contextId = "userCouponClient", value = "charge-market")
public interface UserCouponClient {
    @GetMapping("/user/coupon/use")
    CommonResult<Boolean> use(@RequestBody UserCouponUpdateReqDto userCouponUpdateReqDto);

    @GetMapping("/user/coupon/{id}")
    CommonResult<UserCouponRespDto> findById(@PathVariable(value = "id") Long id);
}
