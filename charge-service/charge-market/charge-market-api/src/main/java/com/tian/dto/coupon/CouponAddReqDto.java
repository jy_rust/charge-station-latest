package com.tian.dto.coupon;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * {@code @description:} 新增优惠券参数
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024/1/23 10:17
 * {@code @version:} 1.0
 */
@Data
@ApiModel(value = "CouponAddReqDto", description = "新增优惠券参数")
public class CouponAddReqDto {

    @ApiModelProperty("优惠券名称")
    private String name;
    @ApiModelProperty("优惠券条件主键id")
    private Integer conditionId;
    @ApiModelProperty("优惠券使用类型")
    private Integer useType;
    @ApiModelProperty("优惠券叠加")
    private Integer overlay;
    @ApiModelProperty("优惠券有效期")
    private Integer period;
    @ApiModelProperty("优惠券领取次数")
    private Integer times;
    @ApiModelProperty("优惠券有效期单位")
    private Integer periodUnit;
    @ApiModelProperty("优惠券有效期单位")
    private Integer point;
}
