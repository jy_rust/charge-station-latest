package com.tian.dto.user;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * {@code @description:} 新增优惠券参数
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024/1/23 10:17
 * {@code @version:} 1.0
 */
@Data
@ApiModel(value = "CouponAddReqDto", description = "新增优惠券参数")
public class UserCouponAddReqDto {
    @ApiModelProperty("优惠券名称")
    private Integer couponId;
}
