package com.tian.client;

import com.tian.common.CommonResult;
import com.tian.dto.SendCodeReqDto;
import com.tian.dto.ValidCodeReqDto;
import com.tian.dto.coupon.CouponRespDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * {@code @description:} TODO
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-03-23 23:07
 * {@code @version:} 1.0
 */
@Component
@FeignClient(contextId = "sendCodeClient", value = "charge-market")
public interface SendCodeClient {
    @PostMapping("/code/send")
    CommonResult<Boolean> sendCode4Login(@RequestBody SendCodeReqDto sendCodeReqDto);
    @PostMapping("/code/valid")
    CommonResult<Boolean> codeValid(@RequestBody ValidCodeReqDto validCodeReqDto);
}
