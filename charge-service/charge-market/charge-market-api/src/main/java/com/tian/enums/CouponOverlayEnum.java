package com.tian.enums;

import lombok.Getter;

/**
 * {@code @description:} 优惠券是否可叠加使用
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024/1/23 20:05
 * {@code @version:} 1.0
 */
@Getter
public enum CouponOverlayEnum {
    UN_OVERLAY(0, "不可叠加"),
    OVERLAY(1, "可叠加");
    private final int overlay;
    private final String desc;

    CouponOverlayEnum(int overlay, String desc) {
        this.overlay = overlay;
        this.desc = desc;
    }
}
