package com.tian.enums;

import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * {@code @description:} 优惠条件类型
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024/1/23 11:33
 * {@code @version:} 1.0
 */
@Getter
public enum CouponConditionType {
    REDUCTION(0, "满减"),
    DIRECT_REDUCTION(1, "直减"),
    DISCOUNT(2, "折扣");

    private final int type;
    private final String desc;
    private static final Map<Integer, CouponConditionType> couponConditionTypeMap = new HashMap<>();

    static {
        for (CouponConditionType value : CouponConditionType.values()) {
            couponConditionTypeMap.put(value.getType(), value);
        }
    }

    CouponConditionType(int type, String desc) {
        this.type = type;
        this.desc = desc;
    }

    public static String getDescByType(int type) {
        return couponConditionTypeMap.get(type).desc;
    }

    public static CouponConditionType getCouponConditionTypeByType(int type) {
        return couponConditionTypeMap.get(type);
    }
}
