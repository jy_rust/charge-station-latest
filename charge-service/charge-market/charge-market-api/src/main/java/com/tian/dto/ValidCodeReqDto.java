package com.tian.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年05月11日 17:16
 * 博客地址：<a href="http://woaijava.cc/">博客地址</a>
 * <p>
 * 验证验证码
 */
@Data
@ApiModel("请求参数")
public class ValidCodeReqDto {
    /**
     * 手机号码
     */
    @ApiModelProperty(value = "手机号码")
    private String phone;
    /**
     * 验证码
     */
    @ApiModelProperty(value = "缓存key")
    private String code;
}
