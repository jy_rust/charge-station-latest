package com.tian.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class Coupon implements Serializable {
    private Integer id;

    private String name;

    private Integer conditionId;

    private Integer useType;

    private Integer overlay;

    private Integer period;

    private Integer point;

    private Integer times;

    private Integer periodUnit;

    private Integer status;

    private static final long serialVersionUID = 1L;



    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", name=").append(name);
        sb.append(", conditionId=").append(conditionId);
        sb.append(", useType=").append(useType);
        sb.append(", overlay=").append(overlay);
        sb.append(", period=").append(period);
        sb.append(", times=").append(times);
        sb.append(", periodUnit=").append(periodUnit);
        sb.append(", status=").append(status);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}