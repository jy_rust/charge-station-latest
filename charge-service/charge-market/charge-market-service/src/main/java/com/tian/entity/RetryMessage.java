package com.tian.entity;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

@Data
@ToString
public class RetryMessage implements Serializable {
    private Long id;

    private Integer type;

    private Integer retry;

    private Integer retriedTimes;

    private Date createTime;

    private Integer status;

    private String content;

    private static final long serialVersionUID = 1L;

}