package com.tian.service;

import com.tian.common.CommonResult;
import com.tian.common.PageResult;
import com.tian.dto.user.*;

import java.util.List;

/**
 * {@code @description:} TODO
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024/1/23 9:32
 * {@code @version:} 1.0
 */
public interface UserCouponService {
    /**
     * 领取优惠券
     *
     * @param couponAddReqDto 参数
     * @return 是否成功
     */
    CommonResult<Boolean> add(UserCouponAddReqDto couponAddReqDto);

    /**
     * 使用优惠券
     *
     * @param couponUpdateReqDto 参数
     * @return 是否成功
     */
    CommonResult<Boolean> use(UserCouponUpdateReqDto couponUpdateReqDto);

    /**
     * 分页查询
     *
     * @param couponPageReqDto 查询参数
     * @return 优惠券列表
     */
    CommonResult<PageResult<List<UserCouponPageRespDto>>> page(UserCouponPageReqDto couponPageReqDto);

    /**
     * 优惠券详情
     *
     * @param id 优惠券id
     * @return 优惠券详情
     */
    CommonResult<UserCouponRespDto> findById(Long id);
}
