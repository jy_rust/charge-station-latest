package com.tian.service;

import com.tian.common.CommonResult;
import com.tian.common.PageResult;
import com.tian.dto.coupon.CouponAddReqDto;
import com.tian.dto.coupon.CouponPageReqDto;
import com.tian.dto.coupon.CouponRespDto;
import com.tian.dto.coupon.CouponUpdateReqDto;

import java.util.List;

/**
 * {@code @description:} 优惠券
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024/1/23 9:58
 * {@code @version:} 1.0
 */
public interface CouponService {

    /**
     * 新增优惠券
     *
     * @param couponAddReqDto 参数
     * @return 是否成功
     */
    CommonResult<Boolean> add(CouponAddReqDto couponAddReqDto);

    /**
     * 修改优惠券
     *
     * @param couponUpdateReqDto 参数
     * @return 是否成功
     */
    CommonResult<Boolean> update(CouponUpdateReqDto couponUpdateReqDto);

    /**
     * 分页查询
     *
     * @param couponPageReqDto 查询参数
     * @return 优惠券列表
     */
    CommonResult<PageResult<List<CouponRespDto>>> page(CouponPageReqDto couponPageReqDto);

    /**
     * 优惠券详情
     *
     * @param id 优惠券id
     * @return 优惠券详情
     */
    CommonResult<CouponRespDto> findById(Integer id);

    /**
     * 优惠券列表
     *
     * @param idList 优惠券id列表
     * @return 优惠券详情
     */
    CommonResult<List<CouponRespDto>> findByIdList(List<Integer> idList);

    /**
     * 校验优惠券
     *
     * @param idList 优惠券id列表
     * @return 优惠券详情
     */
    CommonResult<Boolean> checkCoupon(List<Integer> idList);
}
