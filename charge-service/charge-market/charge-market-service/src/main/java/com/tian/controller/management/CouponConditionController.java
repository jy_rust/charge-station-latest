package com.tian.controller.management;

import com.tian.common.CommonResult;
import com.tian.common.PageResult;
import com.tian.dto.coupon.condition.CouponConditionAddReqDto;
import com.tian.dto.coupon.condition.CouponConditionPageReqDto;
import com.tian.dto.coupon.condition.CouponConditionRespDto;
import com.tian.dto.coupon.condition.CouponConditionUpdateReqDto;
import com.tian.service.CouponConditionService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * {@code @description:} 优惠券条件
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024/1/23 9:59
 * {@code @version:} 1.0
 */
@RestController
@RequestMapping("/management/condition")
@Api(tags = "优惠券条件 控制器")
public class CouponConditionController {
    @Resource
    private CouponConditionService couponConditionService;

    @ApiOperation(value = "新增优惠券条件", notes = "新增数据")
    @ApiResponses({@ApiResponse(code = 200000, message = "操作成功")})
    @PostMapping("/add")
    public CommonResult<Boolean> add(@RequestBody CouponConditionAddReqDto couponConditionAddReqDto) {
        return couponConditionService.add(couponConditionAddReqDto);
    }

    @ApiOperation(value = "修改优惠券条件", notes = "修改数据")
    @ApiResponses({@ApiResponse(code = 200000, message = "操作成功")})
    @PostMapping("/update")
    public CommonResult<Boolean> update(@RequestBody CouponConditionUpdateReqDto couponConditionUpdateReqDto) {
        return couponConditionService.update(couponConditionUpdateReqDto);
    }

    @ApiOperation(value = "分页优惠券条件", notes = "分页数据")
    @ApiResponses({@ApiResponse(code = 200000, message = "操作成功")})
    @PostMapping("/page")
    public CommonResult<PageResult<List<CouponConditionRespDto>>> page(@RequestBody CouponConditionPageReqDto couponConditionPageReqDto) {
        return couponConditionService.page(couponConditionPageReqDto);
    }

    @ApiOperation(value = "优惠券详情条件", notes = "优惠券详情数据")
    @ApiResponses({@ApiResponse(code = 200000, message = "操作成功")})
    @GetMapping("/{id}")
    public CommonResult<CouponConditionRespDto> findById(@PathVariable("id") Integer id) {
        return couponConditionService.findById(id);
    }
    @ApiOperation(value = "优惠券id查询优惠券详情条件", notes = "优惠券id查询优惠券详情条件")
    @ApiResponses({@ApiResponse(code = 200000, message = "操作成功")})
    @GetMapping("/coupon/{couponId}")
    public CommonResult<CouponConditionRespDto> findByCouponId(@PathVariable(value = "couponId") Integer id) {
        return couponConditionService.findByCouponId(id);
    }
}
