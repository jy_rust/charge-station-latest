package com.tian.service;

import com.tian.common.CommonResult;
import com.tian.common.PageResult;
import com.tian.dto.coupon.condition.CouponConditionAddReqDto;
import com.tian.dto.coupon.condition.CouponConditionPageReqDto;
import com.tian.dto.coupon.condition.CouponConditionRespDto;
import com.tian.dto.coupon.condition.CouponConditionUpdateReqDto;

import java.util.List;

/**
 * {@code @description:} 优惠券条件
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024/1/23 9:59
 * {@code @version:} 1.0
 */
public interface CouponConditionService {
    /**
     * 新增优惠券条件
     *
     * @param couponConditionAddReqDto 参数
     * @return 是否成功
     */
    CommonResult<Boolean> add(CouponConditionAddReqDto couponConditionAddReqDto);
    /**
     * 修改优惠券条件
     *
     * @param couponConditionUpdateReqDto 参数
     * @return 是否成功
     */
    CommonResult<Boolean> update(CouponConditionUpdateReqDto couponConditionUpdateReqDto);
    /**
     * 分页查询条件
     *
     * @param couponConditionPageReqDto 查询参数
     * @return 优惠券条件列表
     */
    CommonResult<PageResult<List<CouponConditionRespDto>>> page(CouponConditionPageReqDto couponConditionPageReqDto);
    /**
     * 优惠券条件详情
     *
     * @param id 优惠券id
     * @return 优惠券详情
     */
    CommonResult<CouponConditionRespDto> findById(Integer id);
    CommonResult<CouponConditionRespDto> findByCouponId(Integer couponId);
}
