package com.tian.mapper;

import com.tian.entity.RetryMessage;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface RetryMessageMapper {
    int deleteByPrimaryKey(Long id);

    int insert(RetryMessage record);

    RetryMessage selectByPrimaryKey(Long id);

    List<RetryMessage> selectAll();

    List<RetryMessage> selectRetryMessage(@Param("type") Integer type, @Param("status") Integer status);
    int countRetryMessage(@Param("type") Integer type, @Param("status") Integer status);

    int updateByPrimaryKey(RetryMessage record);
}