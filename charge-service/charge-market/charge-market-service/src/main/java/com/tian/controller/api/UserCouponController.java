package com.tian.controller.api;

import com.tian.annotation.LoginCheckAnnotation;
import com.tian.common.CommonResult;
import com.tian.common.PageResult;
import com.tian.dto.user.*;
import com.tian.enums.UserCouponSatusEnum;
import com.tian.service.UserCouponService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;

/**
 * {@code @description:} 用户的优惠券
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024/1/23 9:33
 * {@code @version:} 1.0
 */
@RestController
@RequestMapping("/api/user/coupon")
@Api(tags = "我的礼券 控制器")
public class UserCouponController {
    @Resource
    private UserCouponService userCouponService;

    @ApiOperation(value = "领取优惠券", notes = "新增数据")
    @ApiResponses({@ApiResponse(code = 200000, message = "操作成功")})
    @PostMapping("/add")
    public CommonResult<Boolean> add(@RequestBody UserCouponAddReqDto userCouponAddReqDto) {
        return userCouponService.add(userCouponAddReqDto);
    }

    @ApiOperation(value = "预备使用优惠券", notes = "修改数据")
    @ApiResponses({@ApiResponse(code = 200000, message = "操作成功")})
    @PostMapping("/pre/use")
    public CommonResult<Boolean> preUse(@RequestBody UserCouponUpdateReqDto userCouponUpdateReqDto) {
        userCouponUpdateReqDto.setStatus(UserCouponSatusEnum.PRE_USE.getStatus());
        return userCouponService.use(userCouponUpdateReqDto);
    }

    @ApiOperation(value = "使用优惠券", notes = "修改数据")
    @ApiResponses({@ApiResponse(code = 200000, message = "操作成功")})
    @PostMapping("/do/use")
    public CommonResult<Boolean> doUse(@RequestBody UserCouponUpdateReqDto userCouponUpdateReqDto) {
        userCouponUpdateReqDto.setStatus(UserCouponSatusEnum.USED.getStatus());
        return userCouponService.use(userCouponUpdateReqDto);
    }

    @ApiOperation(value = "分页查询我的优惠券", notes = "分页数据")
    @ApiResponses({@ApiResponse(code = 200000, message = "操作成功")})
    @PostMapping("/page")
    public CommonResult<PageResult<List<UserCouponPageRespDto>>> page(@RequestBody UserCouponPageReqDto userCouponPageReqDto) {
        return userCouponService.page(userCouponPageReqDto);
    }

    @ApiOperation(value = "优惠券详情", notes = "优惠券详情数据")
    @ApiResponses({@ApiResponse(code = 200000, message = "操作成功")})
    @GetMapping("/{id}")
    @LoginCheckAnnotation
    public CommonResult<UserCouponRespDto> findById(@PathVariable("id") Long id) {
        return userCouponService.findById(id);
    }
}
