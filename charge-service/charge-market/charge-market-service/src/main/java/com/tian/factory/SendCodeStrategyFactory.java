package com.tian.factory;


import com.tian.enums.SendCodeTyeEnum;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年10月14日 15:19
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 */
@Component
public class SendCodeStrategyFactory {
    private static Map<Integer, Class<?>> STRATEGY_MAP = new HashMap<>();
    @Resource
    private ApplicationContext applicationContext;
//    public SmsSender getInvokeStrategy(int type) {
//        return (SmsSender) applicationContext.getBean(STRATEGY_MAP.get(type));
//    }
//    static {
//        STRATEGY_MAP.put(SendCodeTyeEnum.ALI.getType(), AliSmsSender.class);
//        STRATEGY_MAP.put(SendCodeTyeEnum.BAIDU.getType(), BaiduSmsSender.class);
//        STRATEGY_MAP.put(SendCodeTyeEnum.HUAWIE.getType(), HuaweiSmsSender.class);
//    }
}
