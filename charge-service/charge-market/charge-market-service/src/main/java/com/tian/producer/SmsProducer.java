package com.tian.producer;

import com.alibaba.fastjson.JSON;
import com.tian.constant.RabbitMQConstant;
import com.tian.entity.RetryMessage;
import com.tian.enums.RabbitMQConstantEnum;
import com.tian.factory.ApplicationContextFactory;
import com.tian.mapper.RetryMessageMapper;
import com.tian.message.IncomeMessage;
import com.tian.message.InvitedRegistryMessage;
import com.tian.message.SmsSendMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.MessageDeliveryMode;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Date;
import java.util.UUID;

/**
 * {@code @description:} 短信发送
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024/1/26 20:59
 * {@code @version:} 1.0
 */
@Slf4j
@Component
public class SmsProducer {
    @Resource
    private RetryMessageMapper retryMessageMapper;

    public void send(SmsSendMessage smsSendMessage) {

        String content = JSON.toJSONString(smsSendMessage);

        RabbitTemplate rabbitTemplate = ApplicationContextFactory.getBean(RabbitTemplate.class);

        CorrelationData correlationId = new CorrelationData(UUID.randomUUID().toString());

        rabbitTemplate.setConfirmCallback((correlationData, ack, cause) -> {
            if (ack) {
                log.info("UserPointConfirm ConfirmCallback 关联数据：{},投递成功,确认情况：{}", correlationData, ack);
            } else {
                RetryMessage retryMessage = new RetryMessage();
                retryMessage.setContent(content);
                retryMessage.setRetry(5);
                retryMessage.setCreateTime(new Date());
                retryMessage.setStatus(0);
                retryMessage.setRetriedTimes(0);
                retryMessage.setType(0);
                retryMessageMapper.insert(retryMessage);
                log.info("UserPointConfirm ConfirmCallback 关联数据：{},投递失败,确认情况：{}，原因：{}", correlationData, ack, cause);
            }
        });

        rabbitTemplate.setReturnCallback((msg, replyCode, replyText, exchange, routingKey) -> {
            log.info("UserPointConfirm ReturnsCallback 消息：{},回应码：{},回应信息：{},交换机：{},路由键：{}"
                    , msg, replyCode
                    , replyText, exchange
                    , routingKey);
        });
        rabbitTemplate.convertAndSend(RabbitMQConstantEnum.USER_INVITED_REGISTRY.getExchange() + "1"
                , RabbitMQConstantEnum.USER_INVITED_REGISTRY.getRoutingKey(), content, message1 -> {
                    message1.getMessageProperties().setDeliveryMode(MessageDeliveryMode.PERSISTENT); // 设置消息持久化
                    return message1;
                }, correlationId);
    }
}
