package com.tian.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.tian.common.CommonResult;
import com.tian.common.PageResult;
import com.tian.dto.coupon.CouponRespDto;
import com.tian.dto.coupon.condition.CouponConditionAddReqDto;
import com.tian.dto.coupon.condition.CouponConditionPageReqDto;
import com.tian.dto.coupon.condition.CouponConditionRespDto;
import com.tian.dto.coupon.condition.CouponConditionUpdateReqDto;
import com.tian.entity.Coupon;
import com.tian.entity.CouponCondition;
import com.tian.enums.CouponConditionType;
import com.tian.enums.ResultCode;
import com.tian.mapper.CouponConditionMapper;
import com.tian.mapper.CouponMapper;
import com.tian.service.CouponConditionService;
import com.tian.service.CouponService;
import com.tian.util.RedisConstantPre;
import lombok.extern.slf4j.Slf4j;
/*import org.redisson.api.RBucket;
import org.redisson.api.RedissonClient;*/
import org.redisson.api.RBucket;
import org.redisson.api.RedissonClient;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * {@code @description:} 油糊圈条件
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024/1/23 9:59
 * {@code @version:} 1.0
 */
@Slf4j
@Service
public class CouponConditionServiceImpl implements CouponConditionService {
    @Resource
    private CouponConditionMapper couponConditionMapper;
    @Resource
    private CouponMapper couponMapper;

    @Resource
    private  CouponService couponService;
    @Resource
    private RedissonClient redissonClient;

    @Override
    public CommonResult<Boolean> add(CouponConditionAddReqDto couponConditionAddReqDto) {
        CouponCondition couponCondition = new CouponCondition();
        BeanUtils.copyProperties(couponConditionAddReqDto, couponCondition);
        couponConditionMapper.insert(couponCondition);
        RBucket<CouponCondition> bucket = redissonClient.getBucket(RedisConstantPre.COUPON_CONDITION_PRE + couponCondition.getId());
        if (bucket.get() != null) {
            bucket.delete();
        }
        bucket.set(couponCondition);
        return CommonResult.success(Boolean.TRUE);
    }

    @Override
    public CommonResult<Boolean> update(CouponConditionUpdateReqDto couponConditionUpdateReqDto) {
        CouponCondition couponCondition = new CouponCondition();
        BeanUtils.copyProperties(couponConditionUpdateReqDto, couponCondition);
        int update = couponConditionMapper.updateByPrimaryKey(couponCondition);
        return CommonResult.success(update != 0);
    }

    @Override
    public CommonResult<PageResult<List<CouponConditionRespDto>>> page(CouponConditionPageReqDto couponConditionPageReqDto) {
        Page<Object> page = PageHelper.startPage(couponConditionPageReqDto.getCurrentPage(), couponConditionPageReqDto.getPageSize());
        List<CouponCondition> couponConditionList = couponConditionMapper.page(couponConditionPageReqDto);

        List<CouponConditionRespDto> couponRespDtoList = new ArrayList<>();
        for (CouponCondition couponCondition : couponConditionList) {
            CouponConditionRespDto couponConditionRespDto = new CouponConditionRespDto();
            BeanUtils.copyProperties(couponCondition, couponConditionRespDto);
            couponConditionRespDto.setTypeStr(CouponConditionType.getDescByType(couponCondition.getType()));
            couponRespDtoList.add(couponConditionRespDto);
        }
        PageResult<List<CouponConditionRespDto>> listPageResult = PageResult.setData((int) page.getTotal(), page.getPageSize(), page.getPages(), page.getPageNum(), couponRespDtoList);
        return CommonResult.success(listPageResult);
    }

    @Override
    public CommonResult<CouponConditionRespDto> findById(Integer id) {
        RBucket<CouponCondition> bucket = redissonClient.getBucket(RedisConstantPre.COUPON_CONDITION_PRE + id);
        CouponCondition couponCondition = bucket.get();
        if (couponCondition == null) {
            couponCondition = couponConditionMapper.selectByPrimaryKey(id);
            if (couponCondition == null) {
                bucket.set(new CouponCondition());
                log.error("优惠券条件id={} 不存在", id);
                return CommonResult.failed(ResultCode.PARAMETER_ERROR);
            }
        }
        if (couponCondition.getId() == null) {
            log.error("缓存穿透 优惠券条件id={} 不存在", id);
            return CommonResult.failed(ResultCode.PARAMETER_ERROR);
        }
        CouponConditionRespDto couponConditionRespDto = new CouponConditionRespDto();
        BeanUtils.copyProperties(couponCondition, couponConditionRespDto);

        couponConditionRespDto.setTypeStr(CouponConditionType.getDescByType(couponCondition.getType()));
        return CommonResult.success(couponConditionRespDto);
    }

    @Override
    public CommonResult<CouponConditionRespDto> findByCouponId(Integer couponId) {
        CommonResult<CouponRespDto> couponServiceById = couponService.findById(couponId);
        return this.findById(couponServiceById.getData().getConditionId());
    }
}
