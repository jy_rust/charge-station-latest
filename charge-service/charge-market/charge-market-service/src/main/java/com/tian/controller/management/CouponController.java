package com.tian.controller.management;

import com.tian.common.CommonResult;
import com.tian.common.PageResult;
import com.tian.dto.coupon.CouponAddReqDto;
import com.tian.dto.coupon.CouponPageReqDto;
import com.tian.dto.coupon.CouponRespDto;
import com.tian.dto.coupon.CouponUpdateReqDto;
import com.tian.dto.user.UserCouponUpdateReqDto;
import com.tian.service.CouponService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * {@code @description:} 优惠券
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024/1/23 10:00
 * {@code @version:} 1.0
 */
@RestController
@RequestMapping("/management/coupon")
@Api(tags = "优惠券 控制器")
public class CouponController {
    @Resource
    private CouponService couponService;

    @ApiOperation(value = "新增优惠券", notes = "新增数据")
    @ApiResponses({@ApiResponse(code = 200000, message = "操作成功")})
    @PostMapping("/add")
    public CommonResult<Boolean> add(@RequestBody CouponAddReqDto couponAddReqDto) {
        return couponService.add(couponAddReqDto);
    }

    @ApiOperation(value = "修改优惠券", notes = "修改数据")
    @ApiResponses({@ApiResponse(code = 200000, message = "操作成功")})
    @PostMapping("/update")
    public CommonResult<Boolean> update(@RequestBody CouponUpdateReqDto couponUpdateReqDto) {
        return couponService.update(couponUpdateReqDto);
    }

    @ApiOperation(value = "我的礼券", notes = "分页数据")
    @ApiResponses({@ApiResponse(code = 200000, message = "操作成功")})
    @PostMapping("/page")
    public CommonResult<PageResult<List<CouponRespDto>>> page(@RequestBody CouponPageReqDto couponPageReqDto) {
        return couponService.page(couponPageReqDto);
    }

    @ApiOperation(value = "优惠券详情", notes = "优惠券详情数据")
    @ApiResponses({@ApiResponse(code = 200000, message = "操作成功")})
    @GetMapping("/{id}")
    public CommonResult<CouponRespDto> findById(@PathVariable("id") Integer id) {
        return couponService.findById(id);
    }

    /*@ApiOperation(value = "优惠券校验", notes = "优惠券校验数据")
    @ApiResponses({@ApiResponse(code = 200000, message = "操作成功")})
    @PostMapping("/check")
    public CommonResult<Boolean> checkCoupon(@RequestBody UserCouponUpdateReqDto userCouponUpdateReqDto) {
        return couponService.checkCoupon(userCouponUpdateReqDto.getIdList());
    }*/
}
