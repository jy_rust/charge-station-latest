package com.tian.config;

import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RedissonClient;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年06月16日 10:28
 * 博客地址：<a href="http://woaijava.cc/">博客地址</a>
 * <p>
 * 线程池异步处理（延迟处理）
 */
@Slf4j
@Service
public class ThreadDelayTaskService {
    @Resource
    private RedissonClient redissonClient;

    public static final long DELAY_TIME=1000;

    @Async
    public void delayDeleteCache(String key, long time) {
        try {
            //这个延迟多久呢？也就是你业务最多能接受数据不一致多久。
            Thread.sleep(time);
        } catch (InterruptedException e) {
            log.info("delete cache failed:", e);
        }
        redissonClient.getBucket(key).delete();
    }
}
