package com.tian.mapper;

import com.tian.dto.coupon.CouponPageReqDto;
import com.tian.entity.Coupon;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface CouponMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Coupon record);

    Coupon selectByPrimaryKey(Integer id);

    List<Coupon> selectAll();
    List<Coupon> selectByIdList(List<Integer> list);

    int updateByPrimaryKey(Coupon record);

    List<Coupon>  page(CouponPageReqDto couponPageReqDto);
}