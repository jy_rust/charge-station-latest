package com.tian.dto;

import com.tian.entity.MessageTemplate;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年05月26日 21:04
 * 博客地址：<a href="http://woaijava.cc/">博客地址</a>
 */
public class MessageTemplateDto extends MessageTemplate {
}
