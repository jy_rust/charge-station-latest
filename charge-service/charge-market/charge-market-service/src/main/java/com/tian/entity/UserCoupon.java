package com.tian.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class UserCoupon implements Serializable {
    private Long id;

    private Long userId;

    private Integer couponId;

    private Date period;

    private Integer status;

    private Date useTime;

    private Date createTime;

    private static final long serialVersionUID = 1L;

}