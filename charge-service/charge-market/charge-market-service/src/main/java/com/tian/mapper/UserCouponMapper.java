package com.tian.mapper;

import com.tian.dto.user.UserCouponPageReqDto;
import com.tian.entity.UserCoupon;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface UserCouponMapper {
    int deleteByPrimaryKey(Long id);

    int insert(UserCoupon record);

    UserCoupon selectByPrimaryKey(Long id);

    List<UserCoupon> selectAll();

    List<UserCoupon> selectByIdList(List<Long> list);

    int updateByPrimaryKey(UserCoupon record);

    int use(UserCoupon record);

    List<UserCoupon> page(UserCouponPageReqDto userCouponPageReqDto);

    int countTimes(@Param("userId") Long userId, @Param("couponId") Integer couponId);
//    int updateUserPoint(@Param("userId") Integer userId, @Param("point")Integer point);
}