package com.tian.util;

import org.redisson.api.RRateLimiter;
import org.redisson.api.RateIntervalUnit;
import org.redisson.api.RateType;
import org.redisson.api.RedissonClient;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年05月27日 09:13
 * 博客地址：<a href="http://woaijava.cc/">博客地址</a>
 * <p>
 * 限流
 */
@Component
public class LimiterUtil {
    @Resource
    private RedissonClient redissonClient;
    /**
     * intervalTime 时间内 最多只能访问5次
     *
     * @param key          缓存key
     * @param intervalTime 有效期
     * @param limitTime    限流次数
     * @return true 限流 false 放行
     */
    public boolean slidingWindow(String key, Long intervalTime, RateIntervalUnit rateIntervalUnit, int limitTime) {

        RRateLimiter rateLimiter = redissonClient.getRateLimiter(key);
        rateLimiter.trySetRate(RateType.OVERALL, limitTime, intervalTime, rateIntervalUnit);

        if (rateLimiter.tryAcquire()) {
            return false;
        } else {
            return true;
        }
    }
}
