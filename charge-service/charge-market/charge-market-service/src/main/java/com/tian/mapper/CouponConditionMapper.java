package com.tian.mapper;

import com.tian.dto.coupon.condition.CouponConditionPageReqDto;
import com.tian.entity.Coupon;
import com.tian.entity.CouponCondition;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface CouponConditionMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(CouponCondition record);

    CouponCondition selectByPrimaryKey(Integer id);

    List<CouponCondition> selectAll();

    int updateByPrimaryKey(CouponCondition record);

    List<CouponCondition> page(CouponConditionPageReqDto paramDto);
}