package com.tian.mqtt.enums;

import com.tian.mqtt.process.*;

/**
 * @author tianwc 公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月06日 19:46
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 */
public enum MqttTopicEnum {
    /**
     * 开始充电--topic--beanClass
     */
    START_CHARGING("start-charging", "充电开始", CarChargingProcess.class),
    /**
     * 汽车连接充电桩
     */
    CHARGER_CAR_CONNECT("charger-car-connect", "汽车连接充电桩", CarConnectProcess.class),
    /**
     * 汽车与充电枪断开连接
     */
    CHARGER_CAR_DISCONNECT("charger-car-connect", "汽车与充电枪断开连接", CarDisconnectProcess.class),
    /**
     * 汽车充电数据上报
     */
    CAR_CHARGING_DATA("car-charging-data", "汽车充电数据上报", StartChargeDataProcess.class),
    /**
     * 开始结束,设备端订阅这个topic
     */
    CHARGER_STOP_CHARGER_GUN("charger-ready-stop", "充电结束", null),
    /**
     * 准备充电
     */
    CHARGER_READY_CHARGE("charger-ready-charge", "准备充电", null),
    /**
     * 停止充电--topic--beanClass
     */
    CHARGER_STOP("charger-stop", "充电结束", StopChargeProcess.class);

    private final String topicName;
    private final String topicNameDesc;
    private final Class<?> processBeanClass;

    MqttTopicEnum(String topicName, String topicNameDesc, Class beanClass) {
        this.topicName = topicName;
        this.topicNameDesc = topicNameDesc;
        this.processBeanClass = beanClass;
    }

    public String getTopicName() {
        return topicName;
    }

    public String getTopicNameDesc() {
        return topicNameDesc;
    }

    public Class<?> getProcessBeanClass() {
        return processBeanClass;
    }

    /**
     * 通过topic name 查询对应 beanClass
     *
     * @param topicName topic
     * @return processBeanName
     */
    public static Class<?> getBeanClassByTopicName(String topicName) {
        for (MqttTopicEnum value : MqttTopicEnum.values()) {
            if (value.getTopicName().equals(topicName)) {
                return value.getProcessBeanClass();
            }
        }
        return null;
    }
}
