package com.tian.netty.message.resp;

import com.tian.netty.util.BCDUtil;

import java.util.Map;

/**
 * {@code @description:} 开始充电
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-05-17 14:20
 * {@code @version:} 1.0
 */
public class ChargerStartResp extends ResponseMsgBase<Map<String, Object>> {
    /**
     * 消息体长度
     */
    private static final int LENGTH = 17;

    public ChargerStartResp(short msgType) {
        super(msgType);
    }

    @Override
    public void format() {
        this.msgObject = new byte[LENGTH];
        int index = 0;
        this.msgObject[index++] = MSG_HEAD;
        this.msgObject[index++] = (byte) msgType;

        byte[] chargerNoBytes = BCDUtil.str2Bcd(responseBody.get("chargerNo").toString());
        for (int i = 0; i < 8 - chargerNoBytes.length; i++) {
            this.msgObject[index++] = FILLED_VAL;
        }
        for (byte b : chargerNoBytes) {
            this.msgObject[index++] = b;
        }

        byte[] currentTimeBytes = BCDUtil.str2Bcd(responseBody.get("currentTime").toString());
        for (byte b : currentTimeBytes) {
            this.msgObject[index++] = b;
        }
    }
}
