package com.tian.es.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

/**
 * {@code @description:} TODO
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-07-23 15:10
 * {@code @version:} 1.0
 */
@Data
@Document(indexName = "item",type = "_doc",createIndex = false)
public class Item {
    @Id
    private Long id;
    // title使用ik进行分词
    @Field(type = FieldType.Text,analyzer = "ik_max_word")
    private String title;
    // brand 不被分词
    @Field(type=FieldType.Keyword)
    private String brand;
    @Field(type=FieldType.Double)
    private Double price;
    // brand 不被分词，且不创建索引
    @Field(index = false,type = FieldType.Keyword)
    private String images;
}