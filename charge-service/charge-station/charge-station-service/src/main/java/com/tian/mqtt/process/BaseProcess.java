package com.tian.mqtt.process;

import lombok.Data;
import org.eclipse.paho.client.mqttv3.MqttMessage;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月07日 09:53
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 */
@Data
public abstract class BaseProcess implements Runnable {
    protected String topic;
    protected MqttMessage mqttMessage;
    @Override
    public void run() {

    }
}
