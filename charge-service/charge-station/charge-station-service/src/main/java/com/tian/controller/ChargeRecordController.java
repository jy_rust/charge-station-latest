package com.tian.controller;

import com.tian.common.CommonResult;
import com.tian.dto.ChargeTimesCountRespDto;
import com.tian.service.ChargeRecordService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * {@code @description:} 充电记录
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-03-23 16:12
 * {@code @version:} 1.0
 */
@RestController
@RequestMapping("/record")
public class ChargeRecordController {
    @Resource
    private ChargeRecordService chargeRecordService;

    @GetMapping("/charge/times/count/{date}")
    public CommonResult<ChargeTimesCountRespDto> chargeTimesCount(@PathVariable String date) {
        return chargeRecordService.chargeTimesCount(date);
    }

    /**
     * 查询上月充电次数
     */
    @GetMapping("/charge/times/count/last/month")
    public CommonResult<ChargeTimesCountRespDto> chargeTimesCount( ) {
        return chargeRecordService.chargeTimesCountLasMonth();
    }
}
