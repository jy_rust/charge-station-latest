package com.tian.netty.core;

import com.tian.netty.executor.ResponseExecutorBase;
import com.tian.netty.util.Utility;
import io.netty.channel.socket.SocketChannel;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * {@code @description:} 响应分发器
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-05-17 14:34
 * {@code @version:} 1.0
 */
public class RespDispatcher {
    private static final int MAX_THREAD_NUM = 8;

    private final static ExecutorService EXECUTOR_SERVICE = Executors.newFixedThreadPool(MAX_THREAD_NUM);
    public static Future<Object> submit(short msgType, SocketChannel channel, Object msgObject) {

        if(msgObject == null) {
            return null;
        }

        String beanName = "MSG_" + msgType;

        ResponseExecutorBase executor = Utility.getBean(beanName, ResponseExecutorBase.class);
        executor.setMsgType(msgType);
        executor.setChannel(channel);
        executor.setMsgObject(msgObject);

        return EXECUTOR_SERVICE.submit(executor);
    }
}
