package com.tian.netty.message;

import lombok.Data;

import java.util.Date;

/**
 * {@code @description:} 电桩通信日志
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-06-07 11:31
 * {@code @version:} 1.0
 */
@Data
public class ChargerCommLog {
    private Long id;

    /**
     * 电桩编号
     */
    private String chargerCode;

    /**
     * 电桩接口
     */
    private String chargerConn;

    /**
     * 日志类型
     */
    private LogType logType;

    /**
     * 操作参数
     */
    private String params;

    /**
     * 日志时间
     */
    private Date logTime;
}
