package com.tian.netty.core;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.bytes.ByteArrayDecoder;

import java.util.List;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月05日 22:59
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 * <p>
 *  解码
 */
public class MsgByteArrayDecoder extends ByteArrayDecoder {

    byte[] buffer;

    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf msg, List<Object> out) throws Exception {
        byte[] temp = new byte[msg.readableBytes()];
        if (buffer != null) {
            msg.clear();
            msg.writeBytes(buffer);
            msg.writeBytes(temp);
            buffer = null;
        }
        if (msg.readableBytes() > 0) {
            msg.readBytes(temp);
            out.add(temp);
        }
    }
}
