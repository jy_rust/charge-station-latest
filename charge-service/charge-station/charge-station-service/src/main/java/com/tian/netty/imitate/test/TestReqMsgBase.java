package com.tian.netty.imitate.test;

import com.tian.netty.message.AbstractMsg;

/**
 * {@code @description:} TODO
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-05-13 22:38
 * {@code @version:} 1.0
 */
public abstract class TestReqMsgBase <T> extends AbstractMsg {
    protected T responseBody;

    public TestReqMsgBase(short msgType) {
        super(msgType);
    }
    public abstract void format();

    public T getResponseBody() {
        return responseBody;
    }

    public void setResponseBody(T responseBody) {
        this.responseBody = responseBody;
    }
}
