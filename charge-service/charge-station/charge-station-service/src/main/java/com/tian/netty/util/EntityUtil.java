package com.tian.netty.util;

import com.tian.entity.ChargeStationGun;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * {@code @description:} TODO
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-05-17 14:44
 * {@code @version:} 1.0
 */
public class EntityUtil {
    /**
     * 充电桩编号与ID之间映射关系
     */
    private static Map<String, Long> chargerCode2IdMap  =   new ConcurrentHashMap<>();
    /**
     * 充电桩集合 key：充电桩id
     */
    private static Map<Long, ChargeStationGun> chargerMap        =   new ConcurrentHashMap<>();
    public static ChargeStationGun getCharger(String chargeNo) {
        if(chargeNo != null && chargerCode2IdMap.containsKey(chargeNo)) {
            return getCharger(chargerCode2IdMap.get(chargeNo));
        }
        return null;
    }
    public static ChargeStationGun getCharger(Long id) {
        if(id != null && chargerMap.containsKey(id)) {
            return chargerMap.get(id);
        }
        return null;
    }


}
