package com.tian.service.impl;

import com.tian.common.CommonResult;
import com.tian.config.GeospatialService;
import com.tian.dto.*;
import com.tian.entity.StationInfo;
import com.tian.es.entity.ChargeStationLocation;
import com.tian.es.service.ChargeStationEsService;
import com.tian.mapper.ChargeStationInfoMapper;
import com.tian.service.ChargeStationService;
import com.tian.util.StringUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.geo.GeoPoint;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * {@code @description:} 充电站
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024/1/18 18:52
 * {@code @version:} 1.0
 */
@Slf4j
@Service
public class ChargeChargeStationServiceImpl implements ChargeStationService {
    @Resource
    private ChargeStationInfoMapper chargeStationInfoMapper;
    @Autowired
    private GeospatialService geospatialService;
    @Resource
    private ChargeStationEsService chargeStationEsService;

    @Override
    public String test(String msg) {
        List<StationInfo> stationInfos = chargeStationInfoMapper.selectAll();
        log.info("station demo={}", stationInfos.size());
        return "station impl resp:" + msg;
    }


    @Override
    public CommonResult<Boolean> add(StationDto stationDto) {
        int count = chargeStationInfoMapper.selectStation(stationDto);
        if (count > 0) {
            return CommonResult.failed("该充电站信息已入库");
        }
        StationInfo stationInfo = new StationInfo();
        BeanUtils.copyProperties(stationDto, stationInfo);
        chargeStationInfoMapper.insert(stationInfo);
        //添加到redis中 用于地图查询
        geospatialService.addStationLocation(stationInfo.getId().toString(), stationDto.getLongitude().doubleValue(),
                stationDto.getLatitude().doubleValue());
        ChargeStationLocation chargeStationLocation = new ChargeStationLocation();
        chargeStationLocation.setName(stationDto.getStationName());
        GeoPoint geoPoint = new GeoPoint(stationDto.getLatitude().doubleValue(), stationDto.getLongitude().doubleValue());
        chargeStationLocation.setGeoPoint(geoPoint);
        chargeStationLocation.setId(Long.valueOf(stationInfo.getId()));
        chargeStationEsService.add(chargeStationLocation);
        return CommonResult.success(Boolean.TRUE);
    }

    @Override
    public CommonResult<Boolean> update(StationDto stationDto) {
        StationInfo record = new StationInfo();
        record.setId(stationDto.getId());
        record.setStationName(StringUtil.isBlank(stationDto.getStationName()) ? record.getStationName() : stationDto.getStationName());
        record.setAddress(StringUtil.isBlank(stationDto.getAddress()) ? record.getAddress() : stationDto.getAddress());
        // TODO: 2024-03-17 可以再这里判断，也可以在mapper.xml中尽显判断是否需要更新
        record.setDedicatedType(stationDto.getDedicatedType());
        record.setChargingSpeedType(stationDto.getChargingSpeedType());
        record.setOriginalPrice(stationDto.getOriginalPrice());
        record.setPrice(stationDto.getPrice());
        record.setParkFee(stationDto.getParkFee());
        record.setLounge(stationDto.getLounge());
        record.setToilet(stationDto.getToilet());
        record.setShoppingMall(stationDto.getShoppingMall());
        record.setWashCar(stationDto.getWashCar());
        record.setWashFee(stationDto.getWashFee());
        record.setBusinessStartTime(stationDto.getBusinessStartTime());
        record.setBusinessEndTime(stationDto.getBusinessEndTime());
        record.setDiet(stationDto.getDiet());
        record.setLongitude(stationDto.getLongitude());
        record.setLatitude(stationDto.getLatitude());
        record.setStatus(stationDto.getStatus());
        record.setCreateTime(stationDto.getCreateTime());
        chargeStationInfoMapper.updateByPrimaryKey(record);
        return CommonResult.success(Boolean.TRUE);
    }

    @Override
    public CommonResult<Boolean> delete(Integer id) {
        StationInfo stationInfo = chargeStationInfoMapper.selectByPrimaryKey(id);
        // TODO: 2024-07-24 写个状态枚举替换掉
        stationInfo.setStatus(0);
        chargeStationInfoMapper.updateByPrimaryKey(stationInfo);
        chargeStationEsService.delete(id);
        return CommonResult.success(Boolean.TRUE);
    }

    @Override
    public CommonResult<StationQueryPageRespDto> listByPage(ChargeStationQueryPageReqDto chargeStationQueryPageReqDto) {
        chargeStationQueryPageReqDto.setStart(chargeStationQueryPageReqDto.getCurrentPage() * chargeStationQueryPageReqDto.getPageSize());
        int total = chargeStationInfoMapper.count(chargeStationQueryPageReqDto);
        if (total == 0) {
            StationQueryPageRespDto stationQueryPageRespDto = new StationQueryPageRespDto();
            stationQueryPageRespDto.setPageSize(chargeStationQueryPageReqDto.getPageSize());
            stationQueryPageRespDto.setCurrentPage(chargeStationQueryPageReqDto.getCurrentPage());
            stationQueryPageRespDto.setTotal(total);
            return CommonResult.success(stationQueryPageRespDto);
        }
        List<StationInfo> stationInfos = chargeStationInfoMapper.listByPage(chargeStationQueryPageReqDto);
        StationQueryPageRespDto stationQueryPageRespDto = new StationQueryPageRespDto();
        List<StationRespDto> data = new ArrayList<StationRespDto>();
        for (StationInfo stationInfo : stationInfos) {
            StationRespDto stationRespDto = new StationRespDto();
            BeanUtils.copyProperties(stationInfo, stationRespDto);
            data.add(stationRespDto);
        }
        stationQueryPageRespDto.setPageSize(chargeStationQueryPageReqDto.getPageSize());
        stationQueryPageRespDto.setCurrentPage(chargeStationQueryPageReqDto.getCurrentPage());
        stationQueryPageRespDto.setTotal(total);
        stationQueryPageRespDto.setData(data);

        return CommonResult.success(stationQueryPageRespDto);
    }

    @Override
    public CommonResult<StationLocationRespDto> findByLocation(ChargeStationLocationReqDto chargeStationLocationReqDto) {
        List<String> chargingStationsNearby = geospatialService.findChargingStationsNear(chargeStationLocationReqDto.getLongitude().doubleValue(), chargeStationLocationReqDto.getLatitude().doubleValue(), chargeStationLocationReqDto.getRadius());
        StationLocationRespDto stationLocationRespDto = new StationLocationRespDto();
        if (chargingStationsNearby != null) {
            List<Integer> stationIdList = new ArrayList<>();
            for (String string : chargingStationsNearby) {
                stationIdList.add(Integer.parseInt(string));
            }
            stationLocationRespDto.setStationIdList(stationIdList);
            stationLocationRespDto.setTotal(stationIdList.size());
        }
        return CommonResult.success();
    }


}
