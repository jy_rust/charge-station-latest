package com.tian.netty.executor;

import io.netty.channel.socket.SocketChannel;

import java.util.concurrent.Callable;

/**
 * {@code @description:} 处理服务器下发请求的业务逻辑基类
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-05-17 14:36
 * {@code @version:} 1.0
 */
public abstract class ResponseExecutorBase implements Callable<Object> {

    protected short msgType;

    protected SocketChannel channel;

    protected Object msgObject;

    public ResponseExecutorBase() {

    }

    public short getMsgType() {
        return msgType;
    }

    public void setMsgType(short msgType) {
        this.msgType = msgType;
    }

    public SocketChannel getChannel() {
        return channel;
    }

    public void setChannel(SocketChannel channel) {
        this.channel = channel;
    }

    public Object getMsgObject() {
        return msgObject;
    }

    public void setMsgObject(Object msgObject) {
        this.msgObject = msgObject;
    }

    public abstract Object call() throws Exception;
}
