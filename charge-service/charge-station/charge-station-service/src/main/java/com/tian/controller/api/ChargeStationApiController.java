package com.tian.controller.api;

import com.tian.common.CommonResult;
import com.tian.dto.*;
import com.tian.es.entity.NearByPoiParam;
import com.tian.es.service.ChargeStationEsService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * {@code @description:} 站点信息
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024/1/18 18:51
 * {@code @version:} 1.0
 */
@RestController
@RequestMapping("/api/station")
public class ChargeStationApiController {
    @Resource
    private ChargeStationEsService chargeStationEsService;

    /**
     * 根据经纬度获取附近充电站
     *
     * @param nearByPoiParam 经纬度
     * @return 充电站信息
     */
    @PostMapping("/find/location")
    public CommonResult<List<StationInfoIndexRespDto>> getNearStationsByLocation(@RequestBody NearByPoiParam nearByPoiParam) {
        return chargeStationEsService.getNearByLocation(nearByPoiParam);
    }
}
