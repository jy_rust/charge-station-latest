package com.tian.netty.message.req;

import com.tian.netty.util.BCDUtil;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * {@code @description:} 模拟充电桩设备进行请求数据转换
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-05-13 23:52
 * {@code @version:} 1.0
 */
public class TestReq extends RequestMsgBase<MyUser> {
    public TestReq(short msgType, byte[] msgObject) {
        super(msgType, msgObject);
    }

    @Override
    public MyUser parse() {
        byte[] data = Arrays.copyOfRange(msgObject, 2, msgObject.length);
        //第一个是占位符
        //第二个是类型
        int index = 0;
        MyUser myUser = new MyUser();
        myUser.setChargerNo(BCDUtil.bcd2Str(Arrays.copyOfRange(data, index, 8)));
        return myUser;
    }
}
