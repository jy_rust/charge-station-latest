package com.tian.es.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * {@code @description:} TODO
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-07-23 16:01
 * {@code @version:} 1.0
 */
@Data
public class NearByPoiParam implements Serializable {
    private static final long serialVersionUID = 4865877962216516316L;
    // 经度
    private Double lon;
    // 纬度
    private Double lat;
    // 默认展示数量
    private Integer size = 15;
    // 默认查找多少（km，m...）范围内的POI点，默认半径为10，单位后面在代码指定
    private Integer distance = 10;
}


