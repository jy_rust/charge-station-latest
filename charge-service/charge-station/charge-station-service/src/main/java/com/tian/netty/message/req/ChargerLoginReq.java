package com.tian.netty.message.req;

import com.tian.netty.util.BCDUtil;

import java.util.Arrays;

/**
 * {@code @description:} TODO
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-05-17 14:16
 * {@code @version:} 1.0
 */
public class ChargerLoginReq extends RequestMsgBase<ChargerLoginMsg> {

    public ChargerLoginReq(short msgType, byte[] msgObject) {
        super(msgType, msgObject);
    }

    @Override
    public ChargerLoginMsg parse() {
        byte[] datas = Arrays.copyOfRange(msgObject, 4, msgObject.length - 2);
        int index = 0;
        ChargerLoginMsg chargerLoginMsg = new ChargerLoginMsg();
        chargerLoginMsg.setChargeNo(BCDUtil.bcd2Str(Arrays.copyOfRange(datas, index, index += 8)));
        chargerLoginMsg.setChargerType(Integer.toString(datas[index], 16));
        return chargerLoginMsg;
    }
}
