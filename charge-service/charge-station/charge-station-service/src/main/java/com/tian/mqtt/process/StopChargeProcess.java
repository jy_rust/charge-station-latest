package com.tian.mqtt.process;

import com.alibaba.fastjson.JSON;
import com.tian.mapper.ChargeRecordMapper;
import com.tian.mapper.ChargeStationGunMapper;
import com.tian.mqtt.message.ChargerStopSubscribeMessage;
import com.tian.service.ChargeRecordService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月07日 09:50
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 */
@Slf4j
@Component
public class StopChargeProcess extends BaseProcess {
    @Resource
    private ChargeRecordService chargeRecordService;

    @Override
    public void run() {
        log.info("充电结束，订阅到消息msg={}", mqttMessage.getPayload());
        ChargerStopSubscribeMessage chargerStopSubscribeMessage = JSON.parseObject(mqttMessage.getPayload(), ChargerStopSubscribeMessage.class);
        chargeRecordService.doStopCharge(chargerStopSubscribeMessage);
    }
}
