package com.tian.mqtt.subscribe;

import com.alibaba.fastjson.JSONObject;
import com.tian.influx.entity.ChargingData;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

/**
 * {@code @description:} 充电状态时，实时订阅，防止消息丢失
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-04-14 15:11
 * {@code @version:} 1.0
 */
@Service
@Slf4j
public class ChargingMqttSubscribeAction implements MqttSubscribeAction {
/*    @Autowired
    private InfluxdbTemplate influxDBTemplate;*/

    @Override
    public void action(MqttMessage mqttMessage) {
        JSONObject jsonObject = JSONObject.parseObject(new String(mqttMessage.getPayload()));
        //用户id
        Long userId = jsonObject.getLong("userId");
        //充电桩枪编号
        String gunNo = jsonObject.getString("gunNo");
        //用电度数
        BigDecimal chargeNum = jsonObject.getBigDecimal("chargeNum");
        //充电记录id
        Long chargeRecordId = jsonObject.getLong("chargeRecordId");

        ChargingData chargingData = new ChargingData();
        /*chargingData.setGunNo(gunNo);
        chargingData.setChargeRecordId(chargeRecordId.toString());
        chargingData.setUserId(userId);
        chargingData.setChargeNum(chargeNum);*/
//        influxDBTemplate.insert(chargingData);
    }
}
