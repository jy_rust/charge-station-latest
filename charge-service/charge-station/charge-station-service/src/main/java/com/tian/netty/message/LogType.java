package com.tian.netty.message;

import lombok.Getter;

/**
 * {@code @description:} 日志类型
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-06-07 11:31
 * {@code @version:} 1.0
 */
@Getter
public enum LogType {
    ONLINE("上线"), OFFLINE("下线");

    private final String title;

    LogType(String title) {
        this.title = title;
    }
}
