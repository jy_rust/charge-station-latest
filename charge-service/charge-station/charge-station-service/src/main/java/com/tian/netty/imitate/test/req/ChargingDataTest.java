package com.tian.netty.imitate.test.req;

import java.math.BigDecimal;
import java.util.Date;

/**
 * {@code @description:} TODO
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-05-16 10:03
 * {@code @version:} 1.0
 */
public class ChargingDataTest {
    public static void main(String[] args) {
        ChargingDataReq req = new ChargingDataReq((short) 0x01);
        ChargingDataDto chargingDataDto=new ChargingDataDto();
        chargingDataDto.setChargeNo("1000002");
        chargingDataDto.setChargingTime(new Date());
        chargingDataDto.setChargingNum(new BigDecimal("100050"));
        req.setResponseBody(chargingDataDto);
        req.format();
        System.out.println(req.getMsgObject());
    }
}
