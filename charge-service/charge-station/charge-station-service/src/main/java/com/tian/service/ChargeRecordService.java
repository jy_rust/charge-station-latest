package com.tian.service;

import com.tian.common.CommonResult;
import com.tian.dto.*;
import com.tian.entity.ChargingDataRespDto;
import com.tian.mqtt.message.ChargerStopSubscribeMessage;

/**
 * {@code @description:} TODO
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024/2/20 20:48
 * {@code @version:} 1.0
 */
public interface ChargeRecordService {

    CommonResult<Boolean> startCharge(ChargingStartReqDto chargingStartReqDto);

    /**
     * 开始充电
     * @param chargerRecordAddReqDto mqtt获取的信息
     */
    void doStartCharge(ChargeRecordAddReqDto chargerRecordAddReqDto);

    /**
     * 用户手动停止充电
     * @param chargerRecordUpdateReqDto 充电记录
     * @return 是否发错停止充电成功
     */
    CommonResult<ChargeRecordUpdateRespDto> readyStopCharge(ChargeRecordUpdateReqDto chargerRecordUpdateReqDto);

    /**
     * 真正的收到 mqtt 充电结束
     * @param chargerStopSubscribeMessage mqtt 回调信息
     */
    void doStopCharge(ChargerStopSubscribeMessage chargerStopSubscribeMessage);

    CommonResult<ChargeRecordRespDto> selectById(Long id);

    CommonResult<ChargeRecordPageRespDto> page(ChargeRecordPageReqDto chargerRecordPageReqDto);

    CommonResult<ChargeTimesCountRespDto> chargeTimesCount(String date);

    CommonResult<ChargeTimesCountRespDto> chargeTimesCountLasMonth();

    CommonResult<ChargingDataRespDto> chargingData(ChargingStartReqDto chargingStartReqDto);
}
