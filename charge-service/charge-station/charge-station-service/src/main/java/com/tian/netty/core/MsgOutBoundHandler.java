package com.tian.netty.core;

import com.tian.netty.util.JConverter;
import io.netty.channel.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * {@code @description:} 处理向客户端发送消息的业务逻辑
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-03-10 10:58
 * {@code @version:} 1.0
 */
public class MsgOutBoundHandler extends ChannelOutboundHandlerAdapter {
    private static Logger logger = LoggerFactory.getLogger(MsgOutBoundHandler.class);
    
    @Override
    public void write(final ChannelHandlerContext ctx, final Object msg, ChannelPromise promise) throws Exception {
        if (msg instanceof byte[]) {
            try {
                ctx.write(msg,promise).addListener(new ChannelFutureListener(){
        
                    @Override
                    public void operationComplete(ChannelFuture future) throws Exception {
                        logger.info("S==>>C(" + ctx.channel().remoteAddress() + "):" + JConverter.bytes2String((byte[])msg));
                    }
                    
                });
            } catch (Exception e) {
                logger.error("MsgOutBoundHandler Exception", e);
                throw e;
            }
        }
    }

}
