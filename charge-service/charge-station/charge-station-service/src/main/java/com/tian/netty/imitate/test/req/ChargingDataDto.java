package com.tian.netty.imitate.test.req;

import lombok.Data;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.Date;

/**
 * {@code @description:} TODO
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-05-14 20:51
 * {@code @version:} 1.0
 */
@ToString
@Data
public class ChargingDataDto {
    private String chargeNo;
    private BigDecimal chargingNum;
    private Date chargingTime;
}
