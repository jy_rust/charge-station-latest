package com.tian.netty.message.req;

import com.tian.netty.message.AbstractMsg;

/**
 * {@code @description:} 请求消息基类
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-05-13 23:58
 * {@code @version:} 1.0
 */
public abstract class RequestMsgBase<T> extends AbstractMsg {

    public RequestMsgBase(short msgType, byte[] msgObject) {
        super(msgType, msgObject);
    }

    /**
     * 将二进制消息解析成制定类型的对象
     */
    public abstract T parse();

}
