package com.tian.mapper;

import com.tian.dto.ChargeStationQueryPageReqDto;
import com.tian.dto.StationDto;
import com.tian.entity.StationInfo;
import org.apache.ibatis.annotations.Mapper;

import java.util.Date;
import java.util.List;

@Mapper
public interface ChargeStationInfoMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(StationInfo record);

    StationInfo selectByPrimaryKey(Integer id);

    List<StationInfo> selectAll();
    List<StationInfo> listByPage(ChargeStationQueryPageReqDto chargeStationQueryPageReqDto);
    int count(ChargeStationQueryPageReqDto chargeStationQueryPageReqDto);

    int updateByPrimaryKey(StationInfo record);

    int selectStation(StationDto stationDto);

    List<StationInfo>  selectByIdList(List<Integer> idList);
}