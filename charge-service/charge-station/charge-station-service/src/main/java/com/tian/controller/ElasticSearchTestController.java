package com.tian.controller;

import com.tian.es.ElasticBook;
import com.tian.es.service.ElasticSearchService;
import org.elasticsearch.action.admin.indices.create.CreateIndexRequest;
import org.elasticsearch.action.admin.indices.create.CreateIndexResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.List;

/**
 * {@code @description:} TODO
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-06-10 12:20
 * {@code @version:} 1.0
 */
@RestController
@RequestMapping("/es")
public class ElasticSearchTestController {
    @Resource
    public RestHighLevelClient elasticsearchClient;

    @Autowired
    private ElasticSearchService elasticSearchService;


    @GetMapping("/connecTest")
    public void connecTest() throws IOException {
        CreateIndexRequest request = new CreateIndexRequest("test");
        CreateIndexResponse response = elasticsearchClient.indices().create(request, RequestOptions.DEFAULT);
        // 查看是否创建成功
        System.out.println(response.isAcknowledged());
        elasticsearchClient.close();
    }

    //保存
  @GetMapping("/saveTest")
    public void saveTest() throws IOException {
        ElasticBook book = ElasticBook.builder().id(1).name("书名1").summary("霸道的程序猿").price(100).build();
        elasticSearchService.save(book);
        elasticsearchClient.close();
        System.out.println("保存成功");
    }

    //根据主键查询
    @GetMapping("/findTest/{id}")
    public void findTest(@PathVariable("id") Integer id) throws IOException {
        ElasticBook book = ElasticBook.builder().id(id).name("书名2").summary("霸道的程序猿").price(80).build();
        elasticSearchService.save(book);
        ElasticBook book1 = elasticSearchService.findById(id);
        elasticsearchClient.close();
        System.out.println("查询成功");
        System.out.println(book1);
    }

    //根据主键更新
    @GetMapping("/updateTest")
    public void updateTest() throws IOException {
        ElasticBook book = ElasticBook.builder().id(2).name("书名2更新").summary("霸道的程序猿").price(80).build();
        elasticSearchService.update(book);
        ElasticBook book1 = elasticSearchService.findById(2);
        elasticsearchClient.close();
        System.out.println("更新成功");
        System.out.println(book1);
    }

    //根据主键删除
    @GetMapping("/deleteTest")
    public void deleteTest() throws IOException {
        elasticSearchService.deleteById(1);
        elasticsearchClient.close();
        System.out.println("删除成功");
    }

    //派生查询
    @GetMapping("/findByNameTest")
    public void findByNameTest() throws IOException {
        List<ElasticBook> bookList = elasticSearchService.findByName("书名");
        elasticsearchClient.close();
        System.out.println("查询成功");
        System.out.println(bookList);
    }
}
