package com.tian.netty.core;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.bytes.ByteArrayEncoder;
import io.netty.handler.timeout.IdleStateHandler;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月05日 22:59
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 * <p>
 * 定义消息管道初始化流程
 */
public class MsgChannelInitializer extends ChannelInitializer<SocketChannel>{
    
    @Override
    protected void initChannel(SocketChannel ch) throws Exception {       
        //自定义解码器，处理TCP粘包/拆包的问题
        ch.pipeline().addLast("bytesDecoder", new MsgByteArrayDecoder());
        ch.pipeline().addLast(new MsgInBoundHandler());
    }
}
