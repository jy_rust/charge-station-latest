package com.tian.netty.message.req;

import com.tian.netty.imitate.test.req.ChargingDataDto;
import com.tian.netty.util.BCDUtil;
import com.tian.util.DateUtils;

import java.math.BigDecimal;
import java.util.Arrays;

/**
 * {@code @description:} 实时充电数据上报
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-05-14 20:50
 * {@code @version:} 1.0
 */
public class ChargingDataReq extends RequestMsgBase<ChargingDataDto> {
    public ChargingDataReq(short msgType, byte[] msgObject) {
        super(msgType, msgObject);
    }

    @Override
    public ChargingDataDto parse() {
        ChargingDataDto chargingDataDto = new ChargingDataDto();
        chargingDataDto.setChargeNo(BCDUtil.bcd2Str(Arrays.copyOfRange(msgObject, 2, 10)));
        chargingDataDto.setChargingTime(DateUtils.parse(BCDUtil.bcd2Str(Arrays.copyOfRange(msgObject, 11, 18)),DateUtils.FORMAT_NO_SECOND));
        BigDecimal chargeNum=new BigDecimal(BCDUtil.bcd2Str(Arrays.copyOfRange(msgObject, 18, msgObject.length)));
        chargingDataDto.setChargingNum(chargeNum.divide(new BigDecimal(100),2,BigDecimal.ROUND_HALF_UP));
        return chargingDataDto;
    }
}
