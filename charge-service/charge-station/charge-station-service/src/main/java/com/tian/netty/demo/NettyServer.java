package com.tian.netty.demo;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import lombok.extern.slf4j.Slf4j;

/**
 * {@code @description:} TODO
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-05-14 9:04
 * {@code @version:} 1.0
 */
@Slf4j
public class NettyServer {
    /**
     * 用于接收发来的连接请求
     */
    private static EventLoopGroup bossGroup;

    /**
     * 用于处理boss接受并且注册给worker的连接中的信息
     */
    private static EventLoopGroup workerGroup;
    private static final int port = 8081;

    public static void main(String[] args) throws Exception {
        try {
            //两个线程组--无限循环
            //boss处理连接请求
            bossGroup = new NioEventLoopGroup();
            //处理真正的业务
            workerGroup = new NioEventLoopGroup();

            /*
             * server 启动程序
             */
            ServerBootstrap bootstrap = new ServerBootstrap();
            //配置参数
            //设置两个线程组
            bootstrap.group(bossGroup, workerGroup)
                    //设置服务器通道使用的是NioServerSocketChannel作为通道的实现
                    .channel(NioServerSocketChannel.class)
                    //设置线程队列的连接数
                    .option(ChannelOption.SO_BACKLOG, 128)
                    .childOption(ChannelOption.SO_KEEPALIVE, true)
                    //给我们的workGroup的EventLoop对应的管道设置处理器
                    .childHandler(new MyServerChannelInitializer());

            // 服务器绑定端口监听 并且同步
            ChannelFuture future = bootstrap.bind(port).sync();
            // 监听服务器关闭监听，此方法会阻塞
            future.channel().closeFuture().sync();
        } finally {
            //优雅退出，释放线程池资源
            bossGroup.shutdownGracefully();
            workerGroup.shutdownGracefully();
        }
    }
}
