package com.tian.service;

import com.tian.common.CommonResult;
import com.tian.dto.*;

/**
 * {@code @description:} 站点信息
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024/1/18 18:52
 * {@code @version:} 1.0
 */
public interface ChargeStationService {
    String test(String msg);

    CommonResult<Boolean> add(StationDto stationDto);

    CommonResult<Boolean> update(StationDto stationDto);

    CommonResult<Boolean> delete(Integer id);

    CommonResult<StationQueryPageRespDto> listByPage(ChargeStationQueryPageReqDto chargeStationQueryPageReqDto);

    CommonResult<StationLocationRespDto> findByLocation(ChargeStationLocationReqDto chargeStationLocationReqDto);
}
