package com.tian.es.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.data.elasticsearch.annotations.GeoPointField;
import org.springframework.data.elasticsearch.core.geo.GeoPoint;

import java.io.Serializable;

/**
 * {@code @description:} TODO
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-07-23 15:43
 * {@code @version:} 1.0
 */
@Data
//@Document(indexName = "charge_station")
@Document(indexName = "location_03")
public class ChargeStationLocation implements Serializable {
    private static final long serialVersionUID = 2693696237747348257L;

    @Id
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long id;

    @Field(type = FieldType.Text)
    private String name;

    @GeoPointField
    private GeoPoint geoPoint;

    @Field(type = FieldType.Text)
    private String address;

}

