package com.tian.es.service;

import com.tian.es.ElasticBook;

import java.util.List;
/**
 * {@code @description:} TODO
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-06-10 11:45
 * {@code @version:} 1.0
 */


public interface ElasticSearchService {

    void save(ElasticBook book);

    ElasticBook findById(Integer id);

    void update(ElasticBook book);

    void deleteById(Integer id);

    List<ElasticBook> findByName(String name);
}
