package com.tian.config;

import org.redisson.api.*;
import org.redisson.api.geo.GeoSearchArgs;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * {@code @description:} 查询充电站信息
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-04-26 21:12
 * {@code @version:} 1.0
 */


@Component
public class GeospatialService {

    @Resource
    private RedissonClient redissonClient;

    private final String GEO_KEY = "station:location";

    public void addStationLocation(String stationId, double longitude, double latitude) {
        RGeo<String> geo = redissonClient.getGeo(GEO_KEY);
        geo.add(longitude, latitude, stationId);
    }

    public Map<String, Double> findNearbyStations(String stationId, double radius, GeoUnit unit) {
        RGeo<String> geo = redissonClient.getGeo(GEO_KEY);
        return geo.radiusWithDistance(stationId, radius, unit);
    }

    public Map<String, Double> findChargingStationsNearby(double longitude, double latitude, double radius) {
        RGeo<String> geo = redissonClient.getGeo(GEO_KEY); // 使用正确的地理位置键名
        return geo.radiusWithDistance(longitude, latitude, radius, GeoUnit.KILOMETERS); // 使用正确的半径单位
    }
    public List<String> findChargingStationsNear(double longitude, double latitude, double radius) {
        RGeo<String> geo = redissonClient.getGeo(GEO_KEY); // 使用正确的地理位置键名
        return geo.search(GeoSearchArgs.from(longitude, latitude).radius(radius, GeoUnit.KILOMETERS)); // 使用正确的半径单位
    }
}