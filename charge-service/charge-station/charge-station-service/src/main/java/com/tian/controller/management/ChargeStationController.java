package com.tian.controller.management;

import com.tian.common.CommonResult;
import com.tian.dto.*;
import com.tian.service.ChargeStationService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * {@code @description:} 站点信息
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024/1/18 18:51
 * {@code @version:} 1.0
 */
@RestController
@RequestMapping("/management/station")
public class ChargeStationController {
    @Resource
    private ChargeStationService chargeStationService;

    @GetMapping("/test/{message}")
    public String test(@PathVariable String message) {
        return chargeStationService.test(message);
    }


    /**
     * 添加站点信息
     */
    @PostMapping("/add")
    public CommonResult<Boolean> add(@RequestBody StationDto stationDto) {
        return chargeStationService.add(stationDto);
    }

    @PostMapping("/page")
    public CommonResult<StationQueryPageRespDto> listByPage(@RequestBody ChargeStationQueryPageReqDto chargeStationQueryPageReqDto) {
        return chargeStationService.listByPage(chargeStationQueryPageReqDto);
    }
    @PostMapping("/find/location")
    public CommonResult<StationLocationRespDto> findByLocation(@RequestBody ChargeStationLocationReqDto chargeStationLocationReqDto){
        return chargeStationService.findByLocation(chargeStationLocationReqDto);
    }

    @PostMapping("/update")
    public CommonResult<Boolean> update(@RequestBody StationDto stationDto) {
        return chargeStationService.update(stationDto);
    }

    @PostMapping("/delete")
    public CommonResult<Boolean> delete(@RequestBody StationDto stationDto) {
        return chargeStationService.delete(stationDto.getId());
    }
}
