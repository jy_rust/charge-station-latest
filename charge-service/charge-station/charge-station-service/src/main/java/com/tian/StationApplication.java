package com.tian;

import com.tian.factory.ApplicationContextFactory;
import com.tian.netty.core.ChargeCenterServer;
import com.tian.netty.util.Utility;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * 项目启动类
 */
@Slf4j
@SpringBootApplication
public class StationApplication {
    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(StationApplication.class, args);
        ApplicationContextFactory.setApplicationContext(context);
        Utility.setApplicationContext(context);
        ChargeCenterServer chargeCenterServer = new ChargeCenterServer();
        try {
            chargeCenterServer.start();
        } catch (Exception e) {
            log.error("netty start failed", e);
        }
    }
}
