package com.tian.mqtt.subscribe;

import org.eclipse.paho.client.mqttv3.MqttMessage;

/**
 * {@code @description:} 订阅客户端上下线消息
 *   - "$SYS/brokers/+/clients/#"
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-04-14 15:11
 * {@code @version:} 1.0
 */
public class OnOffLineMqttSubscribeAction implements MqttSubscribeAction {
    @Override
    public void action(MqttMessage mqttMessage) {

    }
}
