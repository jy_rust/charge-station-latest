package com.tian.service.impl;

import com.tian.common.CommonResult;
import com.tian.dto.ChargeStationGunReqDto;
import com.tian.dto.ChargeStationGunRespDto;
import com.tian.mapper.ChargeStationGunMapper;
import com.tian.service.ChargeStationGunService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * {@code @description:} 充电枪信息
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-03-13 11:10
 * {@code @version:} 1.0
 */
@Service
public class ChargeStationGunServiceImpl implements ChargeStationGunService {
    @Resource
    private ChargeStationGunMapper chargeStationGunMapper;


    @Override
    public CommonResult<Boolean> add(ChargeStationGunReqDto chargeStationGunReqDto) {
        return null;
    }

    @Override
    public CommonResult<Boolean> update(ChargeStationGunReqDto chargeStationGunReqDto) {
        return null;
    }

    @Override
    public CommonResult<ChargeStationGunRespDto> queryById(Integer id) {
        return null;
    }

    @Override
    public CommonResult<Boolean> list() {
        return null;
    }
}
