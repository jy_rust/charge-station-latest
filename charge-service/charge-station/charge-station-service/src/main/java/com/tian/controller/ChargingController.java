package com.tian.controller;

import com.tian.annotation.LoginCheckAnnotation;
import com.tian.common.CommonResult;
import com.tian.dto.ChargeRecordUpdateReqDto;
import com.tian.dto.ChargeRecordUpdateRespDto;
import com.tian.dto.ChargingStartReqDto;
import com.tian.entity.ChargingDataRespDto;
import com.tian.service.ChargeRecordService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * {@code @description:} 充电相关API
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-04-12 19:42
 * {@code @version:} 1.0
 */
@Slf4j
@RestController
@RequestMapping(value = "/charging")
public class ChargingController {
    @Resource
    private ChargeRecordService chargerRecordService;

    /**
     * 开启充电服务
     */
    @PostMapping(value = "/start")
    @LoginCheckAnnotation
    public CommonResult<Boolean> start(@RequestBody ChargingStartReqDto chargingStartReqDto) {
        return chargerRecordService.startCharge(chargingStartReqDto);
    }

    /**
     * 用户手动启发关闭充电
     */
    @PostMapping("/stop")
    @LoginCheckAnnotation
    public CommonResult<ChargeRecordUpdateRespDto> stop(@RequestBody ChargeRecordUpdateReqDto chargingStartReqDto) {
        return chargerRecordService.readyStopCharge(chargingStartReqDto);
    }

    /**
     * 用户充电实时数据
     */
    @PostMapping("/data")
    @LoginCheckAnnotation
    public CommonResult<ChargingDataRespDto> data(@RequestBody ChargingStartReqDto chargingStartReqDto) {
        return chargerRecordService.chargingData(chargingStartReqDto);
    }
}
