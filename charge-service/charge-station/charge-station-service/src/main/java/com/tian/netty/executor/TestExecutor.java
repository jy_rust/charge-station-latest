package com.tian.netty.executor;

import com.tian.entity.ChargeStationGun;
import com.tian.mapper.ChargeStationGunMapper;
import com.tian.netty.message.req.MyUser;
import com.tian.netty.message.req.TestReq;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * {@code @description:} 测试Bean
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-05-14 1:27
 * {@code @version:} 1.0
 */
@Component("MSG_97")
public class TestExecutor extends RequestExecutorBase {

    @Resource
    private ChargeStationGunMapper chargeStationGunMapper;

    @Override
    public void run() {
        TestReq testReq = new TestReq(msgObject[1], msgObject);
        MyUser myUser = testReq.parse();
        ChargeStationGun chargeStationGun = chargeStationGunMapper.selectByGunNo(myUser.getChargerNo());
        if (chargeStationGun != null) {
            //业务相关
            chargeStationGunMapper.updateByPrimaryKey(chargeStationGun);
        }
    }
}
