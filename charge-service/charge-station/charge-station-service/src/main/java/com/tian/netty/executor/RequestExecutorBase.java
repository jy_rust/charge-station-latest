package com.tian.netty.executor;

import io.netty.channel.socket.SocketChannel;

/**
 * {@code @description:} 处理电桩请求的业务逻辑基类
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-03-10 14:36
 * {@code @version:} 1.0
 */
public class RequestExecutorBase implements Runnable {

    protected SocketChannel channel;

    protected byte[] msgObject;

    public SocketChannel getChannel() {
        return channel;
    }

    public void setChannel(SocketChannel channel) {
        this.channel = channel;
    }

    public byte[] getMsgObject() {
        return msgObject;
    }

    public void setMsgObject(byte[] msgObject) {
        this.msgObject = msgObject;
    }

    @Override
    public void run() {

    }
}