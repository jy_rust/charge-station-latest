package com.tian.netty.imitate.test.req;

import com.tian.netty.imitate.test.TestReqMsgBase;
import com.tian.netty.util.BCDUtil;

import java.util.Map;

/**
 * {@code @description:} TODO
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-05-13 22:40
 * {@code @version:} 1.0
 */
public class TestEventReq extends TestReqMsgBase<Map<String, Object>> {
    /**
     * 消息体长度
     */
    private static final int LENGTH = 10;

    /**
     * @param msgType
     */
    public TestEventReq(short msgType) {
        super(msgType);
    }


    @Override
    public void format() {
        this.msgObject = new byte[LENGTH];
        int index = 0;
        //0---0x68
        this.msgObject[index++] = MSG_HEAD;
        //消息类型
        //1---0x68
        this.msgObject[index++] = (byte) msgType;

        //2到10  充电桩编号
        byte[] chargerNoBytes = BCDUtil.str2Bcd(responseBody.get("chargerNo").toString());

        for (int i = 0; i < 8 - chargerNoBytes.length; i++) {
            this.msgObject[index++] = FILLED_VAL;
        }
        for (byte b : chargerNoBytes) {
            this.msgObject[index++] = b;
        }
    }
}
