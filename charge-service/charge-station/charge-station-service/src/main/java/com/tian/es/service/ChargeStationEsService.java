package com.tian.es.service;

import com.tian.common.CommonResult;
import com.tian.dto.ChargeStationLocationReqDto;
import com.tian.dto.StationInfoIndexRespDto;
import com.tian.es.entity.ChargeStationLocation;
import com.tian.es.entity.NearByPoiParam;

import java.util.List;

/**
 * {@code @description:} 充电站信息与ES索引服务
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-07-23 18:45
 * {@code @version:} 1.0
 */
public interface ChargeStationEsService {
    void createIndex();

    /**
     * 充电站信息录入到ES中
     */
    void add(ChargeStationLocation chargeStationLocation);
    /**
     * 从ES中删除充电站信息
     */
    void delete(Integer id);

    /**
     * 根据经纬度获取附近充电站信息
     */
    CommonResult<List<StationInfoIndexRespDto>> getNearByLocation(NearByPoiParam nearByPoiParam);
}
