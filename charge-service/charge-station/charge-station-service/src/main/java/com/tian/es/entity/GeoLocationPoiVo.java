package com.tian.es.entity;

import lombok.Data;
import org.springframework.data.elasticsearch.annotations.GeoPointField;
import org.springframework.data.elasticsearch.core.geo.GeoPoint;

import java.io.Serializable;

/**
 * {@code @description:} TODO
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-07-23 16:02
 * {@code @version:} 1.0
 */
@Data
public class GeoLocationPoiVo implements Serializable {
    private static final long serialVersionUID = 7244264082382770349L;

    private String id;
    private String name;

    @GeoPointField
    private GeoPoint geoPoint;

    private String address;

    private String phone;

    private String catalog;

    private String specificDistance;

}
