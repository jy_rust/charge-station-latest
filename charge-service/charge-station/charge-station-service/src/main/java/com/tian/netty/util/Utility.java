package com.tian.netty.util;

import org.springframework.context.ApplicationContext;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 常用工具类
 * @author bwang
 */
public class Utility {
    
    private static ApplicationContext applicationContext;
    
    private static ThreadLocal<SimpleDateFormat> threadLocal = new ThreadLocal<SimpleDateFormat>();
    
    public static void setApplicationContext(ApplicationContext context) {
        applicationContext = context;
    }
       
    public static <T>T getBean(String name, Class<T> type) {
        if(applicationContext == null) {
            return null;
        }
        return applicationContext.getBean(name, type);
    }
    
    public static <T>T getBean(Class<T> type) {
        if(applicationContext == null) {
            return null;
        }
        return applicationContext.getBean(type);
    }
    
    public static String getEnvVar(String key) {
        return applicationContext.getEnvironment().getProperty(key);
    }
    
}
