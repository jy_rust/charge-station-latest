package com.tian.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
//import plus.ojbk.influxdb.autoconfigure.properties.InfluxdbProperties;
//import plus.ojbk.influxdb.core.InfluxdbTemplate;

import javax.annotation.Resource;

/**
 * {@code @description:} Influx的数据库连接配置
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-04-14 20:00
 * {@code @version:} 1.0
 */
//@Configuration
public class InfluxDBConfig {
//    @Resource
//    private InfluxdbProperties influxdbProperties;
//
//    @Bean
//    public InfluxdbTemplate influxDBTemplate() {
//        return new InfluxdbTemplate(influxdbProperties);
//    }
}
