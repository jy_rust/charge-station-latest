package com.tian.mqtt.callbeck;

import com.tian.mqtt.ReqDispatcher;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended;
import org.eclipse.paho.client.mqttv3.MqttMessage;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月05日 22:59
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 * <p>
 * 回调
 */
@Slf4j
public class MqttCallback implements MqttCallbackExtended {
    @Override
    public void connectionLost(Throwable throwable) {
        //可以用来做 设备的 下线处理
        System.out.println("连接丢失！");
    }

    @Override
    public void messageArrived(String topic, MqttMessage mqttMessage) throws Exception {
        //订阅回调分发
        ReqDispatcher.submit(topic, mqttMessage);
        log.info("接收到topic={},消息内容message={}", topic, mqttMessage.toString());
    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {

    }

    @Override
    public void connectComplete(boolean b, String s) {
        //设备上线
        System.out.println("连接成功！");
    }
}
