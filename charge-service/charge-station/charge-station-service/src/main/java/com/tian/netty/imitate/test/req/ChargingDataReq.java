package com.tian.netty.imitate.test.req;

import com.tian.netty.imitate.test.TestReqMsgBase;
import com.tian.netty.util.BCDUtil;
import com.tian.util.DateUtils;

import java.util.Map;

/**
 * {@code @description:} TODO
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-05-13 22:40
 * {@code @version:} 1.0
 */
public class ChargingDataReq extends TestReqMsgBase<ChargingDataDto> {
    /**
     * 消息体长度
     */
    private static final int LENGTH = 22;

    /**
     * @param msgType
     */
    public ChargingDataReq(short msgType) {
        super(msgType);
    }


    @Override
    public void format() {
        this.msgObject = new byte[LENGTH];
        int index = 0;
        //0---0x68
        this.msgObject[index++] = MSG_HEAD;
        //消息类型
        //1---0x68
        this.msgObject[index++] = (byte) msgType;

        //8位---2到10  充电桩编号
        byte[] chargerNoBytes = BCDUtil.str2Bcd(responseBody.getChargeNo());

        for (int i = 0; i < 8 - chargerNoBytes.length; i++) {
            this.msgObject[index++] = FILLED_VAL;
        }
        for (byte b : chargerNoBytes) {
            this.msgObject[index++] = b;
        }
        //8位 [32, 36, 5, 22, 16, 9, 55]
        byte[] chargingTime = BCDUtil.str2Bcd(DateUtils.format(responseBody.getChargingTime(), DateUtils.FORMAT_NO_SECOND));
        for (int i = 0; i < 8 - chargingTime.length; i++) {
            this.msgObject[index++] = FILLED_VAL;
        }
        for (byte b : chargingTime) {
            this.msgObject[index++] = b;
        }
        byte[] chargingNum = BCDUtil.str2Bcd(responseBody.getChargingNum().toString());
        //4 位 [16, 0, 117]
        for (int i = 0; i < 4 - chargingNum.length; i++) {
            this.msgObject[index++] = FILLED_VAL;
        }
        for (byte b : chargingNum) {
            this.msgObject[index++] = b;
        }
    }
}
