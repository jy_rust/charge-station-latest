package com.tian.netty.demo;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;

/**
 * {@code @description:} TODO
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-05-14 9:05
 * {@code @version:} 1.0
 */
public class NettyClient {
    public static void main(String[] args) throws Exception {
        //客户端只需要一个事件循环组
        EventLoopGroup eventLoopGroup = new NioEventLoopGroup();
        try {
            //创建客户端启动对象
            Bootstrap bootstrap = new Bootstrap();
            //设置理想国参数
            bootstrap.group(eventLoopGroup)
                    .channel(NioSocketChannel.class)
                    //设置具体处理Handler
                    .handler(new MyClientChannelInitializer());

            //启动客户端
            ChannelFuture future = bootstrap.connect("127.0.0.1", 8081).sync();
            future.channel().closeFuture().sync();
        }finally {
            eventLoopGroup.shutdownGracefully();
        }
    }
}
