package com.tian.netty.executor;

import com.alibaba.fastjson.JSONObject;

import java.util.concurrent.Future;

/**
 * {@code @description:} TODO
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-05-17 14:42
 * {@code @version:} 1.0
 */
public interface ChargingFlowService {
    /**
     * 运营平台远程控制充电桩启动
     *
     * @param params 下发启动电桩报文需要相关参数
     * @return
     */
    Future<Object> sendStarChargerMsg(JSONObject params);
}
