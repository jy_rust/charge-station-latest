package com.tian.es;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
/**
 * {@code @description:} GPS设备实体类
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-06-10 10:38
 * {@code @version:} 1.0
 */


@Data
@AllArgsConstructor
@NoArgsConstructor
public class GpsInfo {

    private String id;
    private String vehicleId;
    private String deviceId;
    private String location;
    private Long gpsTime;
    private Long createTime;

}
