package com.tian.mqtt.subscribe;

import org.eclipse.paho.client.mqttv3.MqttMessage;

/**
 * {@code @description:} mqtt订阅响应接口
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-04-14 15:10
 * {@code @version:} 1.0
 */
public interface MqttSubscribeAction {
    void action(MqttMessage mqttMessage);
}
