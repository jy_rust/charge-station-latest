package com.tian.mapper;

import com.tian.dto.ChargeTimesCountRespDto;
import com.tian.entity.ChargeRecord;
import org.apache.ibatis.annotations.Mapper;

import java.util.Date;
import java.util.List;

@Mapper
public interface ChargeRecordMapper {
    int deleteByPrimaryKey(Long id);

    int insert(ChargeRecord record);

    ChargeRecord selectByPrimaryKey(Long id);

    List<ChargeRecord> selectAll();

    int updateByPrimaryKey(ChargeRecord record);

    ChargeRecord selectByChargeGunId(Integer chargerGunId);

    boolean checkRecordExists(Integer chargerGunId);

    int chargeTimesCount(Date queryDate);
}