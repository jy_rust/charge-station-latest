package com.tian.influx.entity;

import lombok.Data;
import lombok.ToString;
//import org.influxdb.annotation.Column;
//import org.influxdb.annotation.Measurement;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * {@code @description:} 充电进度实时数据
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-04-14 21:06
 * {@code @version:} 1.0
 */
@ToString
@Data
//@Measurement(name = "charging_data")
public class ChargingData {
//    @Column(name = "gun_no")
//    private String gunNo;
//    @Column(name = "charge_record_id")
//    private String chargeRecordId;
//    @Column(name = "user_id")
//    private Long userId;
//    @Column(name = "charge_num")
//    private BigDecimal chargeNum;
//    @Column(name = "time")
//    private LocalDateTime time;

}
