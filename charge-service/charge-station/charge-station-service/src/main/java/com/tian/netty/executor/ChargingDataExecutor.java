package com.tian.netty.executor;

import com.tian.entity.ChargeRecord;
import com.tian.entity.ChargeStationGun;
import com.tian.influx.entity.ChargingData;
import com.tian.mapper.ChargeRecordMapper;
import com.tian.mapper.ChargeStationGunMapper;
import com.tian.netty.imitate.test.req.ChargingDataDto;
import com.tian.netty.message.MsgType;
import com.tian.netty.message.req.ChargingDataReq;
import com.tian.netty.message.req.MyUser;
import com.tian.netty.message.req.TestReq;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
//import plus.ojbk.influxdb.core.InfluxdbTemplate;

import javax.annotation.Resource;
import java.math.BigDecimal;

/**
 * {@code @description:} 充电数据实时上报
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-05-14 1:27
 * {@code @version:} 1.0
 */
@Slf4j
@Component(MsgType.MSG_TYPE_PRE+ MsgType._0X_40)
public class ChargingDataExecutor extends RequestExecutorBase {

    @Resource
    private ChargeStationGunMapper chargeStationGunMapper;
/*    @Autowired
    private InfluxdbTemplate influxDBTemplate;*/
    @Resource
    private ChargeRecordMapper chargeRecordMapper;

    @Override
    public void run() {
        ChargingDataReq chargingDataReq = new ChargingDataReq(msgObject[1], msgObject);
        ChargingDataDto chargingDataDto = chargingDataReq.parse();
        ChargeStationGun chargeStationGun = chargeStationGunMapper.selectByGunNo(chargingDataDto.getChargeNo());
        if (chargeStationGun == null) {
            log.error("枪号不存在{}", chargingDataDto);
        }
        ChargeRecord chargeRecord = chargeRecordMapper.selectByChargeGunId(chargeStationGun.getId());

        /*ChargingData chargingData = new ChargingData();
        chargingData.setGunNo(chargingDataDto.getChargeNo());
        chargingData.setChargeRecordId(chargeRecord.getId().toString());
        chargingData.setUserId(chargeRecord.getUserId());
        chargingData.setChargeNum(chargingDataDto.getChargingNum());
        influxDBTemplate.insert(chargingData);*/
    }
}
