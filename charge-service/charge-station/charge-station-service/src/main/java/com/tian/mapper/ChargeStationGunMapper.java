package com.tian.mapper;

import com.tian.entity.ChargeStationGun;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ChargeStationGunMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ChargeStationGun record);

    ChargeStationGun selectByGunNo(String gunNo);
    ChargeStationGun selectByPrimaryKey(Integer id);

    List<ChargeStationGun> selectAll();

    int updateByPrimaryKey(ChargeStationGun record);
}