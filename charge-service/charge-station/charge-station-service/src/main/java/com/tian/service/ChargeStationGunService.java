package com.tian.service;

import com.tian.common.CommonResult;
import com.tian.dto.ChargeStationGunReqDto;
import com.tian.dto.ChargeStationGunRespDto;

/**
 * {@code @description:} TODO
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-03-13 11:10
 * {@code @version:} 1.0
 */
public interface ChargeStationGunService {

    CommonResult<Boolean> add(ChargeStationGunReqDto chargeStationGunReqDto);

    CommonResult<Boolean> update(ChargeStationGunReqDto chargeStationGunReqDto);

    /**
     * 通过id查询充电枪信息
     *
     * @param id 注解id
     * @return 充电枪信息
     */
    CommonResult<ChargeStationGunRespDto> queryById(Integer id);

    CommonResult<Boolean> list();
}
