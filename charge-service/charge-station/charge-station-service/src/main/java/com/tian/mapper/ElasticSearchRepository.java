package com.tian.mapper;

import com.tian.es.ElasticBook;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

/**
 * {@code @description:} TODO
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-06-10 11:36
 * {@code @version:} 1.0
 */


@Repository
public interface ElasticSearchRepository extends ElasticsearchRepository<ElasticBook,Integer> {
    List<ElasticBook> findByName(String name);
}