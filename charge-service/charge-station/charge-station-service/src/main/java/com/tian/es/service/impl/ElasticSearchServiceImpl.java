package com.tian.es.service.impl;

import com.tian.es.ElasticBook;
import com.tian.es.service.ElasticSearchService;
import com.tian.mapper.ElasticSearchRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * {@code @description:} TODO
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-06-10 11:46
 * {@code @version:} 1.0
 */


@Service
public class ElasticSearchServiceImpl implements ElasticSearchService {

    @Autowired
    private ElasticSearchRepository repository;

    @Override
    public void save(ElasticBook book) {
        repository.save(book);
    }

    @Override
    public ElasticBook findById(Integer id) {
        return repository.findById(id).get();
    }

    @Override
    public void update(ElasticBook book) {
        repository.save(book);
    }

    @Override
    public void deleteById(Integer id) {
        repository.deleteById(id);
    }

    @Override
    public List<ElasticBook> findByName(String name) {
        return repository.findByName(name);
    }
}