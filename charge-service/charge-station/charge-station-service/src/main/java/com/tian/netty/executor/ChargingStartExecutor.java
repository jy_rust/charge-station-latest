package com.tian.netty.executor;

import com.alibaba.fastjson.JSONObject;
import com.tian.netty.message.MsgType;
import com.tian.netty.message.resp.ChargerStartResp;
import com.tian.netty.util.JConverter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * {@code @description:} 开始充电
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-05-17 14:32
 * {@code @version:} 1.0
 */
@Slf4j
@Service(MsgType.MSG_TYPE_PRE + MsgType._0X_54)
public class ChargingStartExecutor extends ResponseExecutorBase{
    @Override
    public Object call() throws Exception {
        ChargerStartResp appStartChargerResp = new ChargerStartResp(MsgType._0X_54);
        appStartChargerResp.setResponseBody((JSONObject)msgObject);
        appStartChargerResp.format();
        channel.writeAndFlush(appStartChargerResp.getMsgObject());

        log.info("app start charger message : [" + JConverter.bytes2String(appStartChargerResp.getMsgObject()) + "]");
        return "success";
    }
}
