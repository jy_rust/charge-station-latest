package com.tian.netty.core;

import io.netty.channel.socket.SocketChannel;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * {@code @description:} 管理客户端连接
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-03-10 10:58
 * {@code @version:} 1.0
 */
public class ClientChannelMap {
    
    /**
     * key   : 电桩编号
     * value ：电桩与电桩控制之间的连接通道
     */
    private static final Map<String, SocketChannel> chargerTChannelMap = new ConcurrentHashMap<String, SocketChannel>();
    
    /**
     * key   : SocketChannel的引用地址
     * value : 电桩编号
     */
    private static final Map<String, String> channelTChargerMap = new ConcurrentHashMap<String, String>();
    
    /**
     * 获取所有的连接
     */
    public static Map<String, SocketChannel> getAll() {
        return chargerTChannelMap;
    }
    
    /**
     * 保存电桩编号和对应的连接
     * @param chargerNo         电桩编号
     * @param socketChannel     客户端连接
     */
    public static void add(String chargerNo, SocketChannel socketChannel){  
        chargerTChannelMap.put(chargerNo, socketChannel);  
        channelTChargerMap.put(socketChannel.id().asLongText(), chargerNo);
    }
    
    /**
     * 根据电桩编号，获得对应的连接
     * @param chargerNo         电桩编号
     */
    public static SocketChannel getSocketChannel(String chargerNo){
        return chargerTChannelMap.get(chargerNo);  
    }  
    
    /**
     * 根据channelId获取电桩编号
     * @param channelId     SocketChannel全局唯一标识
     */
    public static String getChargerNo(String channelId) {
        return channelTChargerMap.get(channelId);
    }

    /**
     * 根据电桩编号，获得对应的连接
     * @param chargerNo         电桩编号
     * @return
     */
    public static SocketChannel getSockeChannel(String chargerNo){
        return chargerTChannelMap.get(chargerNo);
    }

    /**
     * 移除电桩编号对应的客户端连接
     * @param chargerNo     电桩编号
     */
    public static void remove(String chargerNo) {
        if(chargerTChannelMap.containsKey(chargerNo)) {
            SocketChannel socketChannel = chargerTChannelMap.remove(chargerNo);
            channelTChargerMap.remove(socketChannel.id().asLongText());
        }
    }
    
    /**
     * 当电桩控制器服务停止时，需清除有的连接
     */
    public static void clear() {
        chargerTChannelMap.clear();
        channelTChargerMap.clear();
    }
}
