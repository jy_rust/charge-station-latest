package com.tian.mqtt.callbeck;

import lombok.extern.slf4j.Slf4j;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended;
import org.eclipse.paho.client.mqttv3.MqttMessage;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月05日 22:59
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 * <p>
 * 回调 监控设备下线处理
 */
@Slf4j
public class MqttDisconnectCallback implements MqttCallbackExtended {
    @Override
    public void connectionLost(Throwable throwable) {
        //可以用来做 设备的 下线处理
        log.info("连接丢失！");
    }

    @Override
    public void messageArrived(String topic, MqttMessage mqttMessage) {
        log.info("接收消息主题 : " + topic);
        log.info("接收消息Qos : " + mqttMessage.getQos());
        log.info("接收消息内容 : " + new String(mqttMessage.getPayload()));
    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {

    }

    @Override
    public void connectComplete(boolean b, String s) {
        //设备上线
        log.info("连接成功！");
    }
}
