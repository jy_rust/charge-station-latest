package com.tian.client;

import com.tian.common.CommonResult;
import com.tian.dto.ChargeTimesCountRespDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * {@code @description:} 充电记录api
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-03-23 16:17
 * {@code @version:} 1.0
 */
@Component
@FeignClient(contextId = "chargeRecordClient", value = "charge-station")
public interface ChargeRecordClient {

    @GetMapping("/record/charge/times/count/{date}")
      CommonResult<ChargeTimesCountRespDto> chargeTimesCount(@PathVariable(value = "date") String date);
}
