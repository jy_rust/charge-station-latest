package com.tian.dto;

import com.tian.common.PageBase;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalTime;
import java.util.Date;

/**
 * {@code @description:} TODO
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024/1/18 19:03
 * {@code @version:} 1.0
 */
@Data
public class StationRespDto implements Serializable {
    private Integer id;

    private Long userId;

    private String stationName;

    private String address;

    private Integer dedicatedType;

    private Integer chargingSpeedType;

    private BigDecimal originalPrice;

    private BigDecimal price;

    private Integer parkFee;

    private Integer lounge;

    private Integer toilet;

    private Integer shoppingMall;

    private Integer washCar;

    private BigDecimal washFee;

    private LocalTime businessStartTime;

    private LocalTime businessEndTime;

    private Integer diet;

    private BigDecimal longitude;

    private BigDecimal latitude;

    private Integer status;

    private Date createTime;
}
