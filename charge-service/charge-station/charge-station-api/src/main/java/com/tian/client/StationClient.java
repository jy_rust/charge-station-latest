package com.tian.client;

import com.tian.common.CommonResult;
import com.tian.dto.ChargeStationQueryPageReqDto;
import com.tian.dto.StationDto;
import com.tian.dto.StationQueryPageRespDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * {@code @description:} 充电站信息API
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024/1/18 19:00
 * {@code @version:} 1.0
 */
@Component
@FeignClient(contextId = "stationClient", value = "charge-station")
public interface StationClient {
    @GetMapping("/station/test/{message}")
    String test(@PathVariable(value = "message") String message);

    @PostMapping("/station/add")
    CommonResult<Boolean> add(@RequestBody StationDto stationDto);

    @PostMapping("/station/page")
    CommonResult<StationQueryPageRespDto> listByPage(@RequestBody ChargeStationQueryPageReqDto chargeStationQueryPageReqDto);

    @PostMapping("/station/update")
    CommonResult<Boolean> update(@RequestBody StationDto stationDto);

    @PostMapping("/station/delete")
    CommonResult<Boolean> delete(@RequestBody StationDto stationDto);
}
