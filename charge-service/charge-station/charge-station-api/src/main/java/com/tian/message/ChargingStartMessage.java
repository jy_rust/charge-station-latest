package com.tian.message;

import lombok.Data;

/**
 * {@code @description:} 充电开始 发送emqx上的消息
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-04-15 17:01
 * {@code @version:} 1.0
 */
@Data
public class ChargingStartMessage {
    private String gunNo;
    private String chargeRecordId;
    private Long userId;
}
