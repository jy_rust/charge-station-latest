package com.tian.dto;

import java.math.BigDecimal;

/**
 * {@code @description:} 首页充电站信息
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-07-23 20:44
 * {@code @version:} 1.0
 */
public class StationInfoIndexRespDto {
    /**
     * 充电站id
     */
    private Integer id;
    /**
     * 充电站名称
     */
    private String stationName;
    /**
     * 充电站地址
     */
    private String address;
    /**
     * 充电站原价
     */
    private BigDecimal originalPrice;
    /**
     * 充电站现价
     */
    private BigDecimal price;
    /**
     * 充电站类型
     */
    private String dedicatedTypeStr;
    /**
     * 充电站枪支总数
     */
    private Integer stationGunTotal;
    /**
     * 充电站枪支正在使用总数
     */
    private Integer stationUsingTotal;
    /**
     * 充电站枪支空闲总数
     */
    private Integer stationFreeTotal;
}
