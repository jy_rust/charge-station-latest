package com.tian.enums;

import java.util.HashMap;
import java.util.Map;

/**
 * @author tianwc 公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月08日 10:01
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 *
 * 充电记录 充电状态
 */
public enum ChargerStatusEnum {

    CHARGING(0,"充电中"),
    IDLE(1,"空闲"),
    OFFLINE(2,"离线"),
    REPAIR(3,"检修中"),
    OCCUPIED(4,"占用"),
    CHARGER_FINISH(5,"充电完成");
    private final int chargerStatus;
    private final String desc;

    private static Map<Integer, ChargerStatusEnum> MAP= new HashMap<>();
    static {
        for (ChargerStatusEnum chargerStatusEnum : ChargerStatusEnum.values()) {
            MAP.put(chargerStatusEnum.getChargerStatus(),chargerStatusEnum);
        }
    }

    public static String getByStatus(int status){
        return MAP.get(status).getDesc();
    }

    ChargerStatusEnum(int chargerStatus, String desc) {
        this.chargerStatus = chargerStatus;
        this.desc = desc;
    }

    public int getChargerStatus() {
        return chargerStatus;
    }

    public String getDesc() {
        return desc;
    }
}
