package com.tian.entity;

import java.io.Serializable;
import java.util.Date;

public class ChargeStationGun implements Serializable {
    private Integer id;

    private String gunName;

    private String gunNo;

    private Integer stationId;

    private Integer gunType;

    private Integer gunPower;

    private Integer status;

    private Date createTime;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getGunName() {
        return gunName;
    }

    public void setGunName(String gunName) {
        this.gunName = gunName == null ? null : gunName.trim();
    }

    public String getGunNo() {
        return gunNo;
    }

    public void setGunNo(String gunNo) {
        this.gunNo = gunNo == null ? null : gunNo.trim();
    }

    public Integer getStationId() {
        return stationId;
    }

    public void setStationId(Integer stationId) {
        this.stationId = stationId;
    }

    public Integer getGunType() {
        return gunType;
    }

    public void setGunType(Integer gunType) {
        this.gunType = gunType;
    }

    public Integer getGunPower() {
        return gunPower;
    }

    public void setGunPower(Integer gunPower) {
        this.gunPower = gunPower;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", gunName=").append(gunName);
        sb.append(", gunNo=").append(gunNo);
        sb.append(", stationId=").append(stationId);
        sb.append(", gunType=").append(gunType);
        sb.append(", gunPower=").append(gunPower);
        sb.append(", status=").append(status);
        sb.append(", createTime=").append(createTime);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}