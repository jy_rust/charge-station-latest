package com.tian.dto;

import com.tian.entity.ChargeRecord;

import java.util.List;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月01日 11:08
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 */
public class ChargeRecordConvert {

    public static ChargeRecord convert4Add(ChargeRecordAddReqDto chargeRecordAddReqDto) {
        ChargeRecord chargerRecord = new ChargeRecord();

        return chargerRecord;
    }

    public static ChargeRecord convert4Update(ChargeRecordUpdateReqDto chargeRecordUpdateReqDto) {
        ChargeRecord chargerRecord = new ChargeRecord();

        return chargerRecord;
    }

    public static ChargeRecordPageRespDto convert4Page(List<ChargeRecord> chargerRecordList) {
        ChargeRecordPageRespDto chargeRecordPageRespDto = new ChargeRecordPageRespDto();

        return chargeRecordPageRespDto;
    }

    public static ChargeRecordRespDto convert4SelectById(ChargeRecord chargerRecord) {
        ChargeRecordRespDto chargeRecordRespDto = new ChargeRecordRespDto();

        return chargeRecordRespDto;
    }
}
