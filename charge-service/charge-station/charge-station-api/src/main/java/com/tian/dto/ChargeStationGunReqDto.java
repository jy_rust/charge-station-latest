package com.tian.dto;

import lombok.Data;

import java.util.Date;

/**
 * {@code @description:} 充电枪信息
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-03-13 11:12
 * {@code @version:} 1.0
 */
@Data
public class ChargeStationGunReqDto {
    private Integer id;

    private String gunName;

    private String gunNo;

    private Integer stationId;

    private Integer gunType;

    private Integer gunPower;

    private Integer status;

    private Date createTime;
}
