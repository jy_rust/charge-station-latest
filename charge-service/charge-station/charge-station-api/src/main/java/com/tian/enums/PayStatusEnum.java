package com.tian.enums;

/**
 * @author tianwc 公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月08日 10:05
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 * 支付状态
 */
public enum PayStatusEnum {
    INIT(0, "未支付"),
    UN_PAY(1, "支付中"),
    PAY_FAILED(2, "支付失败"),
    PAY_FINISH(3, "支付完成");
    private final int payStatus;
    private final String desc;

    PayStatusEnum(int payStatus, String desc) {
        this.payStatus = payStatus;
        this.desc = desc;
    }

    public int getPayStatus() {
        return payStatus;
    }

    public String getDesc() {
        return desc;
    }
}
