package com.tian.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * {@code @description:} TODO
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-04-29 16:22
 * {@code @version:} 1.0
 */
@Data
public class ChargeStationLocationReqDto {
    private BigDecimal longitude;
    private BigDecimal latitude;
    private Integer radius;
}
