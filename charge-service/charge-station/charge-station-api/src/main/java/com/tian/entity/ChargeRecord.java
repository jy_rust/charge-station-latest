package com.tian.entity;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
public class ChargeRecord implements Serializable {
    private Long id;

    private Long userId;

    private Integer stationGunId;

    private Date startTime;

    private Date endTime;

    private BigDecimal quantity;

    private BigDecimal price;

    private Integer chargeStatus;

    private Integer payStatus;

    private BigDecimal amount;

    private Date payTime;

    private Date createTime;

    private static final long serialVersionUID = 1L;

}