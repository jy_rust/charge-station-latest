package com.tian;

import com.tian.factory.ApplicationContextFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * {@code @description:} 项目启动类
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024/1/18 15:19
 * {@code @version:} 1.0
 */
@SpringBootApplication
public class PayJobApplication {
    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(PayJobApplication.class, args);
        ApplicationContextFactory.setApplicationContext(context);
    }
}
