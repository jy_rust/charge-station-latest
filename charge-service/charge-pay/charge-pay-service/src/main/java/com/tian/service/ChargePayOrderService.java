package com.tian.service;

import com.github.binarywang.wxpay.bean.order.WxPayMpOrderResult;
import com.github.binarywang.wxpay.exception.WxPayException;
import com.tian.common.CommonResult;
import com.tian.dto.WxPrePayReqDto;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * {@code @description:} TODO
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024/2/20 21:25
 * {@code @version:} 1.0
 */
public interface ChargePayOrderService {
    CommonResult<WxPayMpOrderResult> prePay(WxPrePayReqDto wxPrePayReqDto);

    String payNotify(HttpServletRequest request, HttpServletResponse response) throws Exception;
}
