package com.tian.entity;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
public class ChargePayOrder implements Serializable {
    private Long id;

    private Long userId;

    private String orderNo;

    private Integer orderType;

    private Long userCouponId;

    private Long chargeRecordId;

    private BigDecimal amount;

    private String payOrderNo;

    private Integer payStatus;

    private Date updateTime;

    private Date createTime;

    private static final long serialVersionUID = 1L;
}