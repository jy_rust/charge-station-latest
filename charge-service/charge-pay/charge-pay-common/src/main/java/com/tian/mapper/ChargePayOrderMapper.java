package com.tian.mapper;

import com.tian.entity.ChargePayOrder;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ChargePayOrderMapper {
    int deleteByPrimaryKey(Long id);
    int insert(ChargePayOrder record);
    ChargePayOrder selectByPrimaryKey(Long id);
    ChargePayOrder selectByPrimaryOrder(String orderNo);
    List<ChargePayOrder> selectAll();
    int updateByPrimaryKey(ChargePayOrder record);
}