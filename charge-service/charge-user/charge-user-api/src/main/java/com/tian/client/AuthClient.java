package com.tian.client;

import com.tian.common.CommonResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * {@code @description:} 认证接口
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-04-07 23:07
 * {@code @version:} 1.0
 */
@Component
@FeignClient(contextId = "authClient", value = "charge-auth")
public interface AuthClient {
    @GetMapping("/token/valid")
    CommonResult<String> validToken(@RequestParam("token") String token);
}
