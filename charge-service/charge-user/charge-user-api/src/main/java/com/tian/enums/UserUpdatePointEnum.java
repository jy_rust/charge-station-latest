package com.tian.enums;

import lombok.Getter;

/**
 * {@code @description:} 用户积分变化
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-03-15 9:56
 * {@code @version:} 1.0
 */
@Getter
public enum UserUpdatePointEnum {
    ADD(0, "增加"),
    DEDUCT(1, "扣减");
    private final int type;
    private final String desc;

    UserUpdatePointEnum(int type, String desc) {
        this.type = type;
        this.desc = desc;
    }
}
