package com.tian.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * {@code @description:} 登录请求参数
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024/1/25 19:23
 * {@code @version:} 1.0
 */
@Data
public class LoginReqDto implements Serializable {
    private String phone;
    private String code;
}
