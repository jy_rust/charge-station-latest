package com.tian.enums;

import lombok.Getter;

/**
 * {@code @description:} 登录类型枚举
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-04-07 16:42
 * {@code @version:} 1.0
 */
@Getter
public enum LoginTypeEnum {
    NORMAL(0, "帐号密码登录"),
    PHONE_PWD(1, "手机号与密码登录"),
    PHONE_CODE(2, "手机验证码登录"),
    WECHAT(3, "微信授权登录");

    private final int code;
    private final String memo;

    LoginTypeEnum(int code, String memo) {
        this.code = code;
        this.memo = memo;
    }
}
