package com.tian.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * {@code @description:} 车牌前缀信息
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-03-26 11:30
 * {@code @version:} 1.0
 */
@Data
public class CarAreaModelDto implements Serializable {
    /**
     * 车牌前两位
     */
    private String Hp;

    /**
     * 城市
     */
    private String city;

    /**
     * 省
     */
    private String province;

    /**
     * 拼音缩写
     */
    private String Pcode;

    /**
     * 地区代码
     */
    private String AreaCode;
}
