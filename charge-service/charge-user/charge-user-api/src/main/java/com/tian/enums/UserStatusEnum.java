package com.tian.enums;

import lombok.Getter;

/**
 * {@code @description:} 用户状态
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-03-15 10:05
 * {@code @version:} 1.0
 */
@Getter
public enum UserStatusEnum {
    INIT(0),
    DELETE(1);
    private final int status;

    UserStatusEnum(int status) {
        this.status = status;
    }
}
