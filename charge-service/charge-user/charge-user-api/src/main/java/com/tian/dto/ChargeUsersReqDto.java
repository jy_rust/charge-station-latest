package com.tian.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * {@code @description:} 用户信息列表
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-05-21 10:52
 * {@code @version:} 1.0
 */
@Data
public class ChargeUsersReqDto implements Serializable {

    private List<Long> userIdList;
}
