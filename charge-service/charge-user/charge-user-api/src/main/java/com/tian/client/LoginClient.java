package com.tian.client;

import com.tian.common.CommonResult;
import com.tian.dto.AuthLoginDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * {@code @description:} 用户登录
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-04-09 10:51
 * {@code @version:} 1.0
 */
@Component
@FeignClient(contextId = "loginClient", value = "charge-auth")
public interface LoginClient {
    @PostMapping("/user/login")
      CommonResult<String> getToken(@RequestBody @Validated AuthLoginDto authLoginDto, BindingResult bindingResult);
}
