package com.tian.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * {@code @description:} TODO
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-03-25 21:09
 * {@code @version:} 1.0
 */
@Data
public class CityInfoRespDto implements Serializable {
    private Integer id;

    private String areaCode;

    private String name;

    private String parentCode;

    private List<CityInfoRespDto> childList;
}
