package com.tian.dto;

import com.tian.enums.UserUpdatePointEnum;
import lombok.Data;

/**
 * {@code @description:} 积分变动请求参数
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-03-15 9:55
 * {@code @version:} 1.0
 */
@Data
public class UpdatePointReqDto {
    /**
     * 变动用户逐渐
     */
    private Long userId;
    /**
     * 变动积分
     */
    private Integer point;
    /**
     * 变动方式 {@link UserUpdatePointEnum}
     */
    private Integer type;
}
