package com.tian.dto;

import com.tian.enums.LoginTypeEnum;
import com.tian.exception.ValidException;
import lombok.Data;
import lombok.ToString;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;

import javax.validation.constraints.NotNull;

/**
 * {@code @description:} 登录请求参数
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-04-07 16:42
 * {@code @version:} 1.0
 */
@Data
@ToString
public class AuthLoginDto {

    private String username;
    private String password;

    private String phone;
    private String code;

    private String openId;

    /**
     * @see LoginTypeEnum
     */
    @NotNull(message = "登录类型不能为空")
    private Integer loginType;

    public void validData(BindingResult bindingResult){
        if(bindingResult.hasErrors()){
            StringBuilder stringBuilder=new StringBuilder();
            for(ObjectError oe:bindingResult.getAllErrors()){
                stringBuilder.append(oe.getDefaultMessage()+"\n");
            }
            throw new ValidException(stringBuilder.toString());
        }
    }
}
