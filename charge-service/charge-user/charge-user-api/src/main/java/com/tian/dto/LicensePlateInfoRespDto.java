package com.tian.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * {@code @description:} TODO
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-06-06 21:16
 * {@code @version:} 1.0
 */
@Data
public class LicensePlateInfoRespDto implements Serializable {
    private Integer id;

    private String hp;

    private String city;

    private String province;

    private String provinceCode;

    private String areaCode;
}
