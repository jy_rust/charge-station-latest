package com.tian.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * {@code @description:} 用户汽车信息列表返回对象
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-06-06 22:54
 * {@code @version:} 1.0
 */
@Data
public class ChargeCarInfoListRespDto implements Serializable {
    private List<ChargeCarInfoRespDto> data;
}
