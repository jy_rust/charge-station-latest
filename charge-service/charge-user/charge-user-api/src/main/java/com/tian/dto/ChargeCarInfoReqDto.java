package com.tian.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * {@code @description:} 用户车辆信息
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-06-06 22:57
 * {@code @version:} 1.0
 */
@Data
public class ChargeCarInfoReqDto implements Serializable {
    private Long id;

    private Long userId;

    private String carNo;

    private Integer carBrandId;

    private Integer fuelType;

    private Integer displacement;

    private String avatarImg;

    private Integer electromagneticType;

    private Integer batteryCapacity;

    private Integer range;

    private String vehicleIdentifyNo;

    private Integer maxSpeed;

    private Integer accelerationTime;

    private String modelParameters;

    private String energyConsumptionDesc;

    private String otherDesc;

    private Date createTime;
}
