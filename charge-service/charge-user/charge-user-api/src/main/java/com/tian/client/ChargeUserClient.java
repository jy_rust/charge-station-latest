package com.tian.client;

import com.tian.common.CommonResult;
import com.tian.dto.*;
import com.tian.intercptor.FeignConfig;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * {@code @description:} 客户端API
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024/1/18 15:15
 * {@code @version:} 1.0
 */
@Component
@FeignClient(contextId = "chargeUserClient", value = "charge-user",configuration = FeignConfig.class)
public interface ChargeUserClient {

    @GetMapping("/user/id")
    CommonResult<ChargeUserDto> queryById();

    @PostMapping("/user/login")
    CommonResult<ChargeUserLoginResDto> login(@RequestBody LoginReqDto loginReqDto);

    @PostMapping("/user/registry")
    CommonResult<Boolean> registry(@RequestBody ChargeUserRegistryReqDto chargeUserRegistryReqDto);

    @PostMapping("/user/update")
    CommonResult<Boolean> updateById(@RequestBody ChargeUserDto chargeUserDto);

    @PostMapping("/user/update/point")
    CommonResult<Boolean> updatePoint(@RequestBody UpdatePointReqDto updatePointReqDto);
    @PostMapping("/user/find/users")
    CommonResult<ChargeUsersRespDto> findUserByUserIdList(@RequestBody ChargeUsersReqDto chargeUsersReqDto);
}
