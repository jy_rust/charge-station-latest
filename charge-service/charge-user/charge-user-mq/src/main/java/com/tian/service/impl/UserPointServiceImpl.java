package com.tian.service.impl;

import com.tian.entity.ChargeUser;
import com.tian.entity.PointsChangeRecord;
import com.tian.enums.UserUpdatePointEnum;
import com.tian.mapper.ChargeUserMapper;
import com.tian.mapper.PointsChangeRecordMapper;
import com.tian.message.UserPointMessage;
import com.tian.service.UserPointService;
import com.tian.util.RedisConstantPre;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RBucket;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;

/**
 * {@code @description:} 用户积分
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-03-24 10:13
 * {@code @version:} 1.0
 */
@Slf4j
@Service
public class UserPointServiceImpl implements UserPointService {
    @Resource
    private ChargeUserMapper chargeUserMapper;
    @Resource
    private RedissonClient redissonClient;
    @Resource
    private PointsChangeRecordMapper pointsChangeRecordMapper;

    /**
     * 1：获取该用的分布式锁
     * 2：校验是否重复消费
     * 3：新增用户积分变更记录
     * 4：用户积分增加
     * 5：师范分布式锁
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateUserPoint(UserPointMessage userPointMessage) {
        RLock lock = redissonClient.getLock(RedisConstantPre.USER_INFO_ID_LOCK_PRE + userPointMessage.getUserId());
        lock.lock();
        try {
            int count = pointsChangeRecordMapper.selectByReqId(userPointMessage.getReqId());
            if (count > 0) {
                log.info(" 用户积分新增 消息 【重复消费】:{}", userPointMessage);
                return;
            }
            PointsChangeRecord pointsChangeRecord = new PointsChangeRecord();
            pointsChangeRecord.setUserId(userPointMessage.getUserId());
            pointsChangeRecord.setPoint(userPointMessage.getPoint());
            pointsChangeRecord.setType(userPointMessage.getType());
            pointsChangeRecord.setCreateTime(new Date());
            pointsChangeRecord.setReqId(userPointMessage.getReqId());
            //积分变动记录
            pointsChangeRecordMapper.insert(pointsChangeRecord);

            ChargeUser chargeUser = new ChargeUser();
            chargeUser.setPoint(userPointMessage.getPoint());
            chargeUser.setId(userPointMessage.getUserId());
            //用户积分变动
            chargeUserMapper.addUserPoint(chargeUser);
        } finally {
            lock.unlock();
        }
    }
}
