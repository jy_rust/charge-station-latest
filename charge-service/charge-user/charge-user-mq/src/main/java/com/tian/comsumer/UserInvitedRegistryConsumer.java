package com.tian.comsumer;

import com.alibaba.fastjson.JSON;
import com.rabbitmq.client.Channel;
import com.tian.constant.RabbitMqQueueConstant;
import com.tian.message.InvitedRegistryMessage;
import com.tian.service.UserInvitedRegistryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.IOException;

/**
 * {@code @description:} 消费者---邀请用户注册---邀请人余额=当前余额+收益
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-03-24 10:13
 * {@code @version:} 1.0
 */
@RabbitListener(queues = RabbitMqQueueConstant.USER_INVITED_REGISTRY_QUEUE)
@Component
@Slf4j
public class UserInvitedRegistryConsumer {

    @Resource
    private UserInvitedRegistryService userInvitedRegistryService;

    @RabbitHandler
    public void process(Object data, Channel channel, Message message) throws IOException {
        try {
            log.info("消费者接受到的消息是：{},消息体为：{}", data, message);
            InvitedRegistryMessage invitedRegistryMessage = JSON.parseObject(new String(message.getBody()), InvitedRegistryMessage.class);
            userInvitedRegistryService.invitedRegistry(invitedRegistryMessage);
            channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
        } catch (Exception exception) {
            channel.basicNack(message.getMessageProperties().getDeliveryTag(), false, false);
        }
    }
}
