package com.tian.service.impl;

import com.tian.entity.ChargeInviteRecord;
import com.tian.entity.ChargeUser;
import com.tian.entity.ChargeUserIncome;
import com.tian.entity.GlobalProperty;
import com.tian.mapper.ChargeInviteRecordMapper;
import com.tian.mapper.ChargeUserIncomeMapper;
import com.tian.mapper.ChargeUserMapper;
import com.tian.mapper.GlobalPropertyMapper;
import com.tian.message.InvitedRegistryMessage;
import com.tian.service.UserInvitedRegistryService;
import com.tian.util.RedisConstantPre;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;

/**
 * {@code @description:} TODO
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-03-30 16:30
 * {@code @version:} 1.0
 */
@Slf4j
@Service
public class UserInvitedRegistryServiceImpl implements UserInvitedRegistryService {
    @Resource
    private ChargeUserMapper chargeUserMapper;
    @Resource
    private ChargeUserIncomeMapper chargeUserIncomeMapper;
    @Resource
    private GlobalPropertyMapper globalPropertyMapper;
    @Resource
    private ChargeInviteRecordMapper chargeInviteRecordMapper;
    @Resource
    private RedissonClient redissonClient;

    @Transactional
    @Override
    public void invitedRegistry(InvitedRegistryMessage invitedRegistryMessage) {
        GlobalProperty chargeGlobalProperty = globalPropertyMapper.selectByPrimaryKey(1);
        Integer busyDefault = chargeGlobalProperty.getBusiDefault();
        Long userId = invitedRegistryMessage.getUserId();
        RLock lock = redissonClient.getLock(RedisConstantPre.USER_INFO_ID_LOCK_PRE + userId);
        lock.lock();
        try {
            int count = chargeInviteRecordMapper.countByReqId(invitedRegistryMessage.getReqId());
            if (count == 0) {
                log.info("【重复消费】，请求参数={}",invitedRegistryMessage);
                return;
            }
            //邀请人余额 增加
            ChargeUser chargeUser = chargeUserMapper.selectByPrimaryKey(userId);
            //新余额=当前余额+邀请收益
            chargeUser.setBalance(chargeUser.getBalance().add(invitedRegistryMessage.getAmount()));
            chargeUserMapper.updateByPrimaryKey(chargeUser);
            //邀请记录
            ChargeInviteRecord chargeInviteRecord = new ChargeInviteRecord();
            Date date = new Date();
            chargeInviteRecord.setInvitedTime(date);
            chargeInviteRecord.setInvitedUserId(invitedRegistryMessage.getNewUserId());
            chargeInviteRecord.setInviteUserId(invitedRegistryMessage.getUserId());
            chargeInviteRecordMapper.insert(chargeInviteRecord);
            //邀请收益记录
            ChargeUserIncome chargeUserIncome = new ChargeUserIncome();
            chargeUserIncome.setIncome(new BigDecimal(busyDefault));
            chargeUserIncome.setUserId(invitedRegistryMessage.getUserId());
            chargeUserIncome.setCreateTime(date);
            chargeUserIncomeMapper.insert(chargeUserIncome);
        } finally {
            lock.unlock();
        }
    }
}
