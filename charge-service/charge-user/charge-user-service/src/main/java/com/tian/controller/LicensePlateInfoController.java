package com.tian.controller;

import com.tian.common.CommonResult;
import com.tian.dto.LicensePlateInfoListRespDto;
import com.tian.service.LicensePlateInfoService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.io.IOException;

/**
 * {@code @description:} 城市车牌前缀信息
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-06-06 21:15
 * {@code @version:} 1.0
 */
@RestController
@RequestMapping("/license")
public class LicensePlateInfoController {

    @Resource
    private LicensePlateInfoService licensePlateInfoService;

    @GetMapping("/initData")
    public void initData() {
        // 初始化数据
        try {
            licensePlateInfoService.initData();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @GetMapping("/all")
    public CommonResult<LicensePlateInfoListRespDto> allLicensePlateInfo() {
        return licensePlateInfoService.getLicensePlateInfo();
    }

    @GetMapping("/list")
    public CommonResult<LicensePlateInfoListRespDto> getLicensePlateInfoByProvinceCode(@RequestParam("provinceCode") String provinceCode) {
        return licensePlateInfoService.getLicensePlateInfoByProvinceCode(provinceCode);
    }
}
