package com.tian.controller;

import com.tian.OssServiceImpl;
import com.tian.annotation.LoginCheckAnnotation;
import com.tian.common.CommonResult;
import com.tian.dto.*;
import com.tian.service.ChargeUserService;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * {@code @description:} 用户
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024/1/18 15:03
 * {@code @version:} 1.0
 */
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@RestController
@RequestMapping("/user")
public class ChargeUserController {
    @Resource
    private ChargeUserService chargeUserService;
    @Resource
    private OssServiceImpl ossServiceImpl;

    @GetMapping("/id")
    @LoginCheckAnnotation
    public CommonResult<ChargeUserDto> queryById() {
        return chargeUserService.queryById();
    }

    @PostMapping("/login")
    public CommonResult<ChargeUserLoginResDto> login(@RequestBody LoginReqDto loginReqDto) {
        return chargeUserService.login(loginReqDto.getPhone());
    }

    @PostMapping("/registry")
    public CommonResult<Boolean> registry(@RequestBody ChargeUserRegistryReqDto chargeUserRegistryReqDto) {
        return chargeUserService.register(chargeUserRegistryReqDto);
    }

    @PostMapping("/update/id")
    public CommonResult<Boolean> updateById(@RequestBody ChargeUserDto chargeUserDto) {
        return chargeUserService.updateById(chargeUserDto);
    }

    @PostMapping("/update/point")
    public CommonResult<Boolean> updatePoint(@RequestBody UpdatePointReqDto updatePointReqDto){
        return chargeUserService.updatePoint(updatePointReqDto);
    }

    @PostMapping("/find/users")
    public CommonResult<ChargeUsersRespDto> findUserByUserIdList(@RequestBody ChargeUsersReqDto chargeUsersReqDto) {
        return chargeUserService.findUserByUserIdList(chargeUsersReqDto);
    }
}
