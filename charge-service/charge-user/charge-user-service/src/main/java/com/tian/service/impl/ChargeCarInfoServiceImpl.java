package com.tian.service.impl;

import com.alibaba.fastjson.JSON;
import com.tian.common.CommonResult;
import com.tian.dto.ChargeCarInfoListRespDto;
import com.tian.dto.ChargeCarInfoReqDto;
import com.tian.dto.ChargeCarInfoRespDto;
import com.tian.dto.ChargeUserLoginResDto;
import com.tian.entity.ChargeCarInfo;
import com.tian.enums.ResultCode;
import com.tian.mapper.ChargeCarInfoMapper;
import com.tian.service.ChargeCarInfoService;
import com.tian.util.UserCacheUtil;
import org.redisson.api.RBucket;
import org.redisson.api.RedissonClient;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * {@code @description:} 用户车辆信息
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-06-06 23:04
 * {@code @version:} 1.0
 */
@Service
public class ChargeCarInfoServiceImpl implements ChargeCarInfoService {
    @Resource
    private ChargeCarInfoMapper chargeCarInfoMapper;
    @Resource
    private RedissonClient redissonClient;
    @Override
    public CommonResult<ChargeCarInfoListRespDto> getMyChargeCarInfo() {
        ChargeUserLoginResDto user = UserCacheUtil.getUser();
        String key="car.info.list.userId_"+user.getId();
        RBucket<String> bucket = redissonClient.getBucket(key);
        if (bucket.get()!=null){
            return CommonResult.success(JSON.parseObject(bucket.get(), ChargeCarInfoListRespDto.class));
        }
        List<ChargeCarInfo> chargeCarInfoList = chargeCarInfoMapper.selectByUserId(user.getId());
        ChargeCarInfoListRespDto chargeCarInfoListRespDto = new ChargeCarInfoListRespDto();
        List<ChargeCarInfoRespDto> chargeCarInfoRespDtoList = new ArrayList<>();
        for (ChargeCarInfo chargeCarInfo : chargeCarInfoList) {
            ChargeCarInfoRespDto chargeCarInfoRespDto = new ChargeCarInfoRespDto();
            BeanUtils.copyProperties(chargeCarInfo, chargeCarInfoRespDto);
            chargeCarInfoRespDtoList.add(chargeCarInfoRespDto);
        }
        chargeCarInfoListRespDto.setData(chargeCarInfoRespDtoList);
        bucket.set(JSON.toJSONString(chargeCarInfoListRespDto));
        return CommonResult.success(chargeCarInfoListRespDto);
    }

    @Override
    public CommonResult<Boolean> addChargeCarInfo(ChargeCarInfoReqDto chargeCarInfoReqDto) {
        ChargeCarInfo queryCondition = new ChargeCarInfo();
        queryCondition.setUserId(UserCacheUtil.getUser().getId());
        queryCondition.setCarNo(chargeCarInfoReqDto.getCarNo());
        int count = chargeCarInfoMapper.count(queryCondition);
        if (count > 0) {
            return CommonResult.failed(ResultCode.CAR_NO_EXIST);
        }
        ChargeCarInfo chargeCarInfo = new ChargeCarInfo();
        BeanUtils.copyProperties(chargeCarInfoReqDto, chargeCarInfo);
        chargeCarInfoMapper.insert(chargeCarInfo);
        return CommonResult.success(true);
    }

    @Override
    public CommonResult<Boolean> updateChargeCarInfo(ChargeCarInfoReqDto chargeCarInfoReqDto) {
        ChargeCarInfo chargeCarInfo = new ChargeCarInfo();
        BeanUtils.copyProperties(chargeCarInfoReqDto, chargeCarInfo);
        chargeCarInfoMapper.updateByPrimaryKey(chargeCarInfo);
        return CommonResult.success(true);
    }
}
