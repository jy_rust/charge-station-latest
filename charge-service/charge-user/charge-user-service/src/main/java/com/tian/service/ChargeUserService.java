package com.tian.service;

import com.tian.common.CommonResult;
import com.tian.dto.*;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * {@code @description:} 用户信息
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024/1/18 15:02
 * {@code @version:} 1.0
 */
public interface ChargeUserService {
    CommonResult<ChargeUserDto> queryById();

    /**
     * 用户注册
     *
     * @param phone       用户注册手机号
     * @param invitedCode 邀请码
     * @return 注册成功
     */
    CommonResult<Boolean> register(ChargeUserRegistryReqDto chargeUserRegistryReqDto);

    /**
     * 用户登录
     *
     * @param phone 手机号
     * @return 登录返回
     */
    CommonResult<ChargeUserLoginResDto> login(String phone);

    /**
     * 修改用户信息
     *
     * @param chargeUserDto 用户变更信息
     * @return 修改是否成功
     */
    CommonResult<Boolean> updateById(ChargeUserDto chargeUserDto);

    /**
     * 积分变动
     */
    CommonResult<Boolean> updatePoint(UpdatePointReqDto updatePointReqDto);

    CommonResult<ChargeUserPointsRankListRespDto> userPointRank();

    CommonResult<ChargeUsersRespDto> findUserByUserIdList(ChargeUsersReqDto chargeUsersReqDto);
}
