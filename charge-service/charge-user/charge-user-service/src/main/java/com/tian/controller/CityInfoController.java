package com.tian.controller;

import com.tian.common.CommonResult;
import com.tian.dto.CityInfoListRespDto;
import com.tian.service.CityInfoService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * {@code @description:} 省市区县
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-03-25 21:17
 * {@code @version:} 1.0
 */
@RestController
@RequestMapping("/city")
public class CityInfoController {
    @Resource
    private CityInfoService cityInfoService;

    @GetMapping("/list")
    public CommonResult<CityInfoListRespDto> list(String name,String areaCode) {
        return cityInfoService.getCityInfoTree(name, areaCode);
    }
}
