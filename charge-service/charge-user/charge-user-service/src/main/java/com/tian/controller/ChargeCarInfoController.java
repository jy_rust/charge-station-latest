package com.tian.controller;

import com.tian.common.CommonResult;
import com.tian.dto.ChargeCarInfoListRespDto;
import com.tian.dto.ChargeCarInfoReqDto;
import com.tian.service.ChargeCarInfoService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * {@code @description:} 用户汽车车辆信息
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-06-06 23:16
 * {@code @version:} 1.0
 */
@RestController
@RequestMapping("/car/info")
public class ChargeCarInfoController {
    @Resource
    private ChargeCarInfoService chargeCarInfoService;

    /**
     * 获取我的汽车信息列表
     * @return 汽车信息列表
     */
    @GetMapping("/mine")
    public CommonResult<ChargeCarInfoListRespDto>  getMyChargeCarInfoList() {
        return chargeCarInfoService.getMyChargeCarInfo();
    }

    /**
     * 添加我的汽车信息
     * @param chargeCarInfoReqDto 车辆信息
     * @return 是否添加成功
     *
     **/
    @PostMapping("/add")
    public CommonResult<Boolean> addChargeCarInfo(ChargeCarInfoReqDto chargeCarInfoReqDto) {
        return chargeCarInfoService.addChargeCarInfo(chargeCarInfoReqDto);
    }
    /**
     * 修改我的汽车信息
     * @param chargeCarInfoReqDto 车辆信息
     * @return 是否修改成功
     *
     **/
    @PostMapping("/update")
    public CommonResult<Boolean> updateChargeCarInfo(ChargeCarInfoReqDto chargeCarInfoReqDto) {
        return chargeCarInfoService.updateChargeCarInfo(chargeCarInfoReqDto);
    }
}
