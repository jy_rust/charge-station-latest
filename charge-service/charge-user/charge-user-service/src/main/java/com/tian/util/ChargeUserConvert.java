package com.tian.util;

import com.tian.dto.ChargeUserDto;
import com.tian.entity.ChargeUser;
import org.springframework.beans.BeanUtils;

/**
 * {@code @description:} 实体转换
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024/1/18 20:07
 * {@code @version:} 1.0
 */
public class ChargeUserConvert {

    public static ChargeUserDto convert(ChargeUser chargeUser) {
        ChargeUserDto chargeUserDto = new ChargeUserDto();
        BeanUtils.copyProperties(chargeUser, chargeUserDto);
        return chargeUserDto;
    }
}
