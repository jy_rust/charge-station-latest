package com.tian.service;

import com.tian.common.CommonResult;
import com.tian.dto.LicensePlateInfoListRespDto;

import java.io.IOException;

/**
 * {@code @description:} 城市车牌前缀信息
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-06-06 21:13
 * {@code @version:} 1.0
 */
public interface LicensePlateInfoService {

    CommonResult<LicensePlateInfoListRespDto> getLicensePlateInfoByProvinceCode(String provinceCode);
    CommonResult<LicensePlateInfoListRespDto> getLicensePlateInfo();

    void initData() throws IOException;
}
