package com.tian.service.impl;

import com.tian.common.CommonResult;
import com.tian.dto.CityInfoListRespDto;
import com.tian.dto.CityInfoRespDto;
import com.tian.entity.CityInfo;
import com.tian.mapper.CityInfoMapper;
import com.tian.service.CityInfoService;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * {@code @description:} 省市区县
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-03-25 21:10
 * {@code @version:} 1.0
 */
@Service
public class CityInfoServiceImpl implements CityInfoService {
    @Resource
    private CityInfoMapper cityInfoMapper;

    @Override
    public CommonResult<CityInfoListRespDto> getCityInfoTree(String name, String areaCode) {
        List<CityInfo> cityInfoList = cityInfoMapper.selectAll(name, areaCode);
        List<CityInfoRespDto> dataList = new ArrayList<>();
        for (CityInfo cityInfo : cityInfoList) {
            CityInfoRespDto cityInfoRespDto = new CityInfoRespDto();
            cityInfoRespDto.setId(cityInfo.getId());
            cityInfoRespDto.setAreaCode(cityInfo.getAreaCode());
            cityInfoRespDto.setName(cityInfo.getName());
            cityInfoRespDto.setParentCode(cityInfo.getParentCode());
            dataList.add(cityInfoRespDto);
        }
        CityInfoListRespDto cityInfoListRespDto = new CityInfoListRespDto();
        cityInfoListRespDto.setDataList(getChild("0", dataList));
        return CommonResult.success(cityInfoListRespDto);
    }

    /**
     * 构建树形结构
     *
     * @return
     */
    private List<CityInfoRespDto> getChild(String id, List<CityInfoRespDto> allList) {
        //存放子节点集合
        List<CityInfoRespDto> children = new ArrayList<>();
        for (CityInfoRespDto node : allList) {
            //如果根节点id等于集合内parentId，说明是根节点的子节点
            if (node.getParentCode().equals(id)) {
                //存入子节点集合
                children.add(node);
            }
        }
        for (CityInfoRespDto childrenNode : children) {
            //递归调用，如果子节点存在子节点，则再次调用
            List<CityInfoRespDto> CityChildren = getChild(childrenNode.getAreaCode(), allList);
            childrenNode.setChildList(CityChildren);
        }
        //递归结束条件：如果子节点不存在子节点，则递归结束
        if (CollectionUtils.isEmpty(children)) {
            return null;
        }
        return children;
    }
}
