package com.tian.jobhandler;

import com.tian.entity.RetryMessage;
import com.tian.mapper.RetryMessageMapper;
import com.tian.producer.InvitedRegistryProducer;
import com.xxl.job.core.context.XxlJobHelper;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**
 * {@code @description:} 每月数据统计（上个月累计充电次数、累计充电电量、累计支付单单数量、累计支付金额）
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-03-23 11:42
 * {@code @version:} 1.0
 */
@Slf4j
@Component
public class InvitedRegistryRetryMessageJob {

    @Resource
    private RetryMessageMapper retryMessageMapper;
    @Resource
    private InvitedRegistryProducer invitedRegistryIncomeProducer;

    @XxlJob("invitedRegistryIncomeRetryMessageJob")
    public void process() {
        log.info("开始执行 Invited user registry income retry messageJob 定时任务");
        XxlJobHelper.log("start userPointRetryMessageJob job");
        int countRetryMessage = retryMessageMapper.countRetryMessage(0, 0);
        if (countRetryMessage == 0) {
            log.info(" 执行结束  Invited user registry income retry messageJob 没有消息需要重发");
        }
        List<RetryMessage> retryMessages = retryMessageMapper.selectRetryMessage(0, 0);
        for (RetryMessage retryMessage : retryMessages) {
            invitedRegistryIncomeProducer.sendMessage(retryMessage);
        }
    }
}
