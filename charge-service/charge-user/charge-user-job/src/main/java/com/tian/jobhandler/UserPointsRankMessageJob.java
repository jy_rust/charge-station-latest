package com.tian.jobhandler;

import com.tian.entity.ChargeUser;
import com.tian.entity.PointsChangeRecord;
import com.tian.entity.RetryMessage;
import com.tian.mapper.ChargeUserMapper;
import com.tian.mapper.PointsChangeRecordMapper;
import com.tian.mapper.RetryMessageMapper;
import com.tian.producer.InvitedRegistryProducer;
import com.xxl.job.core.context.XxlJobHelper;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RScoredSortedSet;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * {@code @description:} 每月数据统计（上个月累计充电次数、累计充电电量、累计支付单单数量、累计支付金额）
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-03-23 11:42
 * {@code @version:} 1.0
 */
@Slf4j
@Component
public class UserPointsRankMessageJob {
    //2147483647
    public static final long MAX_INTEGER = Integer.MAX_VALUE;
    public static final double TIME_CONSTANT = Math.pow(10, -13);
    @Resource
    private ChargeUserMapper chargeUserMapper;
    @Resource
    private PointsChangeRecordMapper pointsChangeRecordMapper;

    @Resource
    private RedissonClient redissonClient;

    @Value("${user.points.rank.count}")
    private Integer rankCount;

    @XxlJob("invitedRegistryIncomeRetryMessageJob")
    public void process() {
        log.info("开始执行 Invited user registry income retry messageJob 定时任务");
        XxlJobHelper.log("start userPointRetryMessageJob job");
        List<ChargeUser> chargeUserList = chargeUserMapper.selectByRankCount(rankCount);
        List<Long> userIdList = chargeUserList.stream()
                .map(ChargeUser::getId)
                .collect(Collectors.toList());
        List<PointsChangeRecord> pointsChangeRecordList = pointsChangeRecordMapper.selectByUserIdList(userIdList);
        Map<Long, PointsChangeRecord> pointsChangeRecordMap = pointsChangeRecordList.stream()
                .collect(Collectors.toMap(PointsChangeRecord::getId, record -> record));

        // 创建有序集合
        RScoredSortedSet<Long> leaderboard = redissonClient.getScoredSortedSet("user.point.rank");
        //删除已有的数据
        leaderboard.delete();
        //放入新的数据
        for (ChargeUser chargeUser : chargeUserList) {
            leaderboard.add(calculateScore(chargeUser.getPoint(), pointsChangeRecordMap.get(chargeUser.getId()).getCreateTime().getTime()), chargeUser.getId());

        }
        log.info("结束执行 Invited user registry income retry messageJob 定时任务");
    }

    public static double calculateScore(int contribution, long timestamp) {
        return contribution + (MAX_INTEGER - timestamp) * TIME_CONSTANT;
    }
}
