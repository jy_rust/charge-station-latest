package com.tian.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;

/**
 * {@code @description:} 用户积分变动消息重发
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-03-24 10:57
 * {@code @version:} 1.0
 */
@Slf4j
@Configuration
public class UserPointConfirm {

   /* @Scope("prototype")
    @Bean
    public RabbitTemplate userPointConfirmRabbitTemplate(ConnectionFactory connectionFactory) {
        RabbitTemplate rabbitTemplate = new RabbitTemplate();
        rabbitTemplate.setConnectionFactory(connectionFactory);
        //设置消息投递失败的策略，有两种策略：自动删除或返回到客户端。
        //我们既然要做可靠性，当然是设置为返回到客户端(true是返回客户端，false是自动删除)
        rabbitTemplate.setMandatory(true);
        rabbitTemplate.setConfirmCallback(new UserPointConfirmCallback());
        return rabbitTemplate;
    }*/
}
