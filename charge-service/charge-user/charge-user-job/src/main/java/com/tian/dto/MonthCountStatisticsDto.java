package com.tian.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * {@code @description:} 每月数据统计
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-03-23 11:46
 * {@code @version:} 1.0
 */
@Data
public class MonthCountStatisticsDto {

    /**
     * 支付单数量
     */
    private int payOrderCount;
    /**
     * 支付金额总额
     */
    private BigDecimal payAmountSum;
    /**
     * 充电次数
     */
    private int chargeTimesCount;
    /**
     * 充电电量总度数
     */
    private int chargingSum;

    /**
     * 充电金额统计
     */
    private BigDecimal chargeAmountSum;
}
