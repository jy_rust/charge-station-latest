package com.tian.mapper;

import com.tian.entity.ChargeUserIncome;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 邀请收益
 */
@Mapper
public interface ChargeUserIncomeMapper {
    int deleteByPrimaryKey(Long id);

    int insert(ChargeUserIncome record);

    ChargeUserIncome selectByPrimaryKey(Long id);

    List<ChargeUserIncome> selectAll();

    int updateByPrimaryKey(ChargeUserIncome record);
}