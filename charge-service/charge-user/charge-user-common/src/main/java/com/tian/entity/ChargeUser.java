package com.tian.entity;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
@ToString
public class ChargeUser implements Serializable {
    private Long id;

    private String nickName;

    private String password;

    private String avatarImg;

    private String phone;

    private String email;

    private Integer authentication;

    private Integer vip;

    private String openId;

    private BigDecimal balance;

    private Integer point;

    private String inviteCode;

    private Integer type;

    private Integer status;

    private Date createTime;

    private static final long serialVersionUID = 1L;

}