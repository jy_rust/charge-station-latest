package com.tian.mapper;

import com.tian.entity.CityInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface CityInfoMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(CityInfo record);

    CityInfo selectByPrimaryKey(Integer id);

    List<CityInfo> selectAll(@Param("name") String name, @Param("areaCode") String areaCode);

    int updateByPrimaryKey(CityInfo record);
}