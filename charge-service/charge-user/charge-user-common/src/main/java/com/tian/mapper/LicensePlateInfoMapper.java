package com.tian.mapper;

import com.tian.entity.LicensePlateInfo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
@Mapper
public interface LicensePlateInfoMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(LicensePlateInfo record);

    LicensePlateInfo selectByPrimaryKey(Integer id);

    List<LicensePlateInfo> selectAll();

    int updateByPrimaryKey(LicensePlateInfo record);

    List<LicensePlateInfo> selectByProvinceCode(String provinceCode);
}