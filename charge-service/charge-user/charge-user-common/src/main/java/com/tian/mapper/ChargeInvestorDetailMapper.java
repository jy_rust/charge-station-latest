package com.tian.mapper;

import com.tian.entity.ChargeInvestorDetail;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ChargeInvestorDetailMapper {
    int deleteByPrimaryKey(Long id);

    int insert(ChargeInvestorDetail record);

    ChargeInvestorDetail selectByPrimaryKey(Long id);

    List<ChargeInvestorDetail> selectAll();

    int updateByPrimaryKey(ChargeInvestorDetail record);
}