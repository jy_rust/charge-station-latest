package com.tian.mapper;

import com.tian.entity.ChargeCarBrand;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ChargeCarBrandMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ChargeCarBrand record);

    ChargeCarBrand selectByPrimaryKey(Integer id);

    List<ChargeCarBrand> selectAll();

    int updateByPrimaryKey(ChargeCarBrand record);
}