package com.tian.mapper;

import com.tian.entity.ChargeInviteRecord;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
/**
 * 邀请记录
 */
@Mapper
public interface ChargeInviteRecordMapper {
    int deleteByPrimaryKey(Long id);

    int insert(ChargeInviteRecord record);

    ChargeInviteRecord selectByPrimaryKey(Long id);

    int countByReqId(String reqId);

    List<ChargeInviteRecord> selectAll();

    int updateByPrimaryKey(ChargeInviteRecord record);
}