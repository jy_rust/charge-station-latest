package com.tian.mapper;

import com.tian.entity.ChargeUser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ChargeUserMapper {
    int deleteByPrimaryKey(Long id);

    int insert(ChargeUser record);

    ChargeUser selectByPrimaryKey(Long id);
    ChargeUser selectByPhone(String phone);
    ChargeUser selectByName(String phone);

    List<ChargeUser> selectAll();

    int updateByPrimaryKey(ChargeUser record);
    int updateById(ChargeUser record);
    int addUserPoint(ChargeUser record);

    ChargeUser login(String phone);

    ChargeUser queryByInvitedCode(String invitedCode);

    List<ChargeUser> selectByRankCount(Integer rankCount);

    List<ChargeUser> selectByIdList(@Param("userIdList") List<Long> userIdList);
}