package com.tian.mapper;

import com.tian.entity.ChargeCarCityPrefix;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ChargeCarCityPrefixMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ChargeCarCityPrefix record);

    ChargeCarCityPrefix selectByPrimaryKey(Integer id);

    List<ChargeCarCityPrefix> selectAll();

    int updateByPrimaryKey(ChargeCarCityPrefix record);
}