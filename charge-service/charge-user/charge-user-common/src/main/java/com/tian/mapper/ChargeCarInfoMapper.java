package com.tian.mapper;


import com.tian.entity.ChargeCarInfo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
@Mapper
public interface ChargeCarInfoMapper {
    int deleteByPrimaryKey(Long id);

    int insert(ChargeCarInfo record);

    ChargeCarInfo selectByPrimaryKey(Long id);
    int count(ChargeCarInfo record);

    List<ChargeCarInfo> selectByUserId(Long userId);

    int updateByPrimaryKey(ChargeCarInfo record);
}