package com.tian.mapper;

import com.tian.entity.SystemBusinessSwitch;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface SystemBusinessSwitchMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(SystemBusinessSwitch record);

    SystemBusinessSwitch selectByPrimaryKey(Integer id);
    SystemBusinessSwitch selectByType(Integer type);

    List<SystemBusinessSwitch> selectAll();

    int updateByPrimaryKey(SystemBusinessSwitch record);
}