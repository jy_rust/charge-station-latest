package com.tian.entity;

import java.io.Serializable;
import java.util.Date;

public class ChargeCarInfo implements Serializable {
    private Long id;

    private Long userId;

    private String carNo;

    private Integer carBrandId;

    private Integer fuelType;

    private Integer displacement;

    private String avatarImg;

    private Integer electromagneticType;

    private Integer batteryCapacity;

    private Integer range;

    private String vehicleIdentifyNo;

    private Integer maxSpeed;

    private Integer accelerationTime;

    private String modelParameters;

    private String energyConsumptionDesc;

    private String otherDesc;

    private Date createTime;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getCarNo() {
        return carNo;
    }

    public void setCarNo(String carNo) {
        this.carNo = carNo == null ? null : carNo.trim();
    }

    public Integer getCarBrandId() {
        return carBrandId;
    }

    public void setCarBrandId(Integer carBrandId) {
        this.carBrandId = carBrandId;
    }

    public Integer getFuelType() {
        return fuelType;
    }

    public void setFuelType(Integer fuelType) {
        this.fuelType = fuelType;
    }

    public Integer getDisplacement() {
        return displacement;
    }

    public void setDisplacement(Integer displacement) {
        this.displacement = displacement;
    }

    public String getAvatarImg() {
        return avatarImg;
    }

    public void setAvatarImg(String avatarImg) {
        this.avatarImg = avatarImg == null ? null : avatarImg.trim();
    }

    public Integer getElectromagneticType() {
        return electromagneticType;
    }

    public void setElectromagneticType(Integer electromagneticType) {
        this.electromagneticType = electromagneticType;
    }

    public Integer getBatteryCapacity() {
        return batteryCapacity;
    }

    public void setBatteryCapacity(Integer batteryCapacity) {
        this.batteryCapacity = batteryCapacity;
    }

    public Integer getRange() {
        return range;
    }

    public void setRange(Integer range) {
        this.range = range;
    }

    public String getVehicleIdentifyNo() {
        return vehicleIdentifyNo;
    }

    public void setVehicleIdentifyNo(String vehicleIdentifyNo) {
        this.vehicleIdentifyNo = vehicleIdentifyNo == null ? null : vehicleIdentifyNo.trim();
    }

    public Integer getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(Integer maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public Integer getAccelerationTime() {
        return accelerationTime;
    }

    public void setAccelerationTime(Integer accelerationTime) {
        this.accelerationTime = accelerationTime;
    }

    public String getModelParameters() {
        return modelParameters;
    }

    public void setModelParameters(String modelParameters) {
        this.modelParameters = modelParameters == null ? null : modelParameters.trim();
    }

    public String getEnergyConsumptionDesc() {
        return energyConsumptionDesc;
    }

    public void setEnergyConsumptionDesc(String energyConsumptionDesc) {
        this.energyConsumptionDesc = energyConsumptionDesc == null ? null : energyConsumptionDesc.trim();
    }

    public String getOtherDesc() {
        return otherDesc;
    }

    public void setOtherDesc(String otherDesc) {
        this.otherDesc = otherDesc == null ? null : otherDesc.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", userId=").append(userId);
        sb.append(", carNo=").append(carNo);
        sb.append(", carBrandId=").append(carBrandId);
        sb.append(", fuelType=").append(fuelType);
        sb.append(", displacement=").append(displacement);
        sb.append(", avatarImg=").append(avatarImg);
        sb.append(", electromagneticType=").append(electromagneticType);
        sb.append(", batteryCapacity=").append(batteryCapacity);
        sb.append(", range=").append(range);
        sb.append(", vehicleIdentifyNo=").append(vehicleIdentifyNo);
        sb.append(", maxSpeed=").append(maxSpeed);
        sb.append(", accelerationTime=").append(accelerationTime);
        sb.append(", modelParameters=").append(modelParameters);
        sb.append(", energyConsumptionDesc=").append(energyConsumptionDesc);
        sb.append(", otherDesc=").append(otherDesc);
        sb.append(", createTime=").append(createTime);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}