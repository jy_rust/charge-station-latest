package com.tian.entity;

import java.io.Serializable;

/**
 * 投资人
 */
public class ChargeInvestorDetail implements Serializable {
    private Long id;

    private Long userId;

    private String businessLicenseUrl;

    private String contractUrl;

    private String idCradFrontUrl;

    private String idCradReverseUrl;

    private String bankCardNo;

    private String bankAccount;

    private String openBank;

    private Integer status;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getBusinessLicenseUrl() {
        return businessLicenseUrl;
    }

    public void setBusinessLicenseUrl(String businessLicenseUrl) {
        this.businessLicenseUrl = businessLicenseUrl == null ? null : businessLicenseUrl.trim();
    }

    public String getContractUrl() {
        return contractUrl;
    }

    public void setContractUrl(String contractUrl) {
        this.contractUrl = contractUrl == null ? null : contractUrl.trim();
    }

    public String getIdCradFrontUrl() {
        return idCradFrontUrl;
    }

    public void setIdCradFrontUrl(String idCradFrontUrl) {
        this.idCradFrontUrl = idCradFrontUrl == null ? null : idCradFrontUrl.trim();
    }

    public String getIdCradReverseUrl() {
        return idCradReverseUrl;
    }

    public void setIdCradReverseUrl(String idCradReverseUrl) {
        this.idCradReverseUrl = idCradReverseUrl == null ? null : idCradReverseUrl.trim();
    }

    public String getBankCardNo() {
        return bankCardNo;
    }

    public void setBankCardNo(String bankCardNo) {
        this.bankCardNo = bankCardNo == null ? null : bankCardNo.trim();
    }

    public String getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(String bankAccount) {
        this.bankAccount = bankAccount == null ? null : bankAccount.trim();
    }

    public String getOpenBank() {
        return openBank;
    }

    public void setOpenBank(String openBank) {
        this.openBank = openBank == null ? null : openBank.trim();
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", userId=").append(userId);
        sb.append(", businessLicenseUrl=").append(businessLicenseUrl);
        sb.append(", contractUrl=").append(contractUrl);
        sb.append(", idCradFrontUrl=").append(idCradFrontUrl);
        sb.append(", idCradReverseUrl=").append(idCradReverseUrl);
        sb.append(", bankCardNo=").append(bankCardNo);
        sb.append(", bankAccount=").append(bankAccount);
        sb.append(", openBank=").append(openBank);
        sb.append(", status=").append(status);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}