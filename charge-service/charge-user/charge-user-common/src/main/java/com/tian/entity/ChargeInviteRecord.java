package com.tian.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 邀请记录
 */
@Data
public class ChargeInviteRecord implements Serializable {
    private Long id;

    private Long inviteUserId;

    private String notes;

    private String reqId;

    private Long invitedUserId;

    private Date invitedTime;

    private static final long serialVersionUID = 1L;

}