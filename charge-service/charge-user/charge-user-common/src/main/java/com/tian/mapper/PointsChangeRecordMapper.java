package com.tian.mapper;

import com.tian.entity.PointsChangeRecord;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface PointsChangeRecordMapper {
    int deleteByPrimaryKey(Long id);

    int insert(PointsChangeRecord record);

    PointsChangeRecord selectByPrimaryKey(Long id);
    int selectByReqId(String reqId);

    List<PointsChangeRecord> selectAll();

    int updateByPrimaryKey(PointsChangeRecord record);

    List<PointsChangeRecord> selectByUserIdList(List<Long> userIdList);
}