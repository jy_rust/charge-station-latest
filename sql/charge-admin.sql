/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 80200
Source Host           : localhost:3306
Source Database       : charge-admin

Target Server Type    : MYSQL
Target Server Version : 80200
File Encoding         : 65001

Date: 2024-03-28 15:50:51
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `charge_station_gun`
-- ----------------------------
DROP TABLE IF EXISTS `charge_station_gun`;
CREATE TABLE `charge_station_gun` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT 'id',
  `gun_name` varchar(255) NOT NULL COMMENT '终端名称',
  `gun_no` varchar(255) NOT NULL COMMENT '终端编号',
  `station_id` int NOT NULL COMMENT '充电站id',
  `gun_type` int NOT NULL COMMENT '终端类型',
  `gun_power` int NOT NULL COMMENT '终端功率',
  `status` int NOT NULL COMMENT '状态',
  `create_time` datetime NOT NULL COMMENT '录入时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='充电枪信息';

-- ----------------------------
-- Records of charge_station_gun
-- ----------------------------

-- ----------------------------
-- Table structure for `charge_station_info`
-- ----------------------------
DROP TABLE IF EXISTS `charge_station_info`;
CREATE TABLE `charge_station_info` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT 'id',
  `user_id` bigint DEFAULT NULL COMMENT '投资人id',
  `station_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '站点名称',
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '详细地址',
  `dedicated_type` int NOT NULL COMMENT '专用类型',
  `charging_speed_type` int NOT NULL COMMENT '充电速度类型',
  `original_price` decimal(10,0) NOT NULL COMMENT '原价',
  `price` decimal(10,0) NOT NULL COMMENT '实际价格',
  `park_fee` int NOT NULL COMMENT '停车收费模式',
  `lounge` int NOT NULL COMMENT '是否有休息室',
  `toilet` int NOT NULL COMMENT '是否有卫生间',
  `shopping-mall` int NOT NULL COMMENT '是否有商场',
  `wash_car` int NOT NULL COMMENT '是否有洗车场',
  `wash_fee` decimal(10,0) NOT NULL COMMENT '洗车费用',
  `business_start_time` time NOT NULL COMMENT '营业开始时间',
  `business_end_time` time NOT NULL COMMENT '营业结束时间',
  `diet` int NOT NULL COMMENT '是否有饮食',
  `longitude` decimal(10,8) NOT NULL COMMENT '经度',
  `latitude` decimal(10,8) NOT NULL COMMENT '纬度',
  `status` int NOT NULL COMMENT '状态',
  `create_time` datetime NOT NULL COMMENT '录入时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='站点信息';

-- ----------------------------
-- Records of charge_station_info
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_content`
-- ----------------------------
DROP TABLE IF EXISTS `sys_content`;
CREATE TABLE `sys_content` (
  `id` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NOT NULL COMMENT '主键',
  `title` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL COMMENT '标题',
  `one_img` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL COMMENT '单图url',
  `multiple_img` varchar(500) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL COMMENT '多图url',
  `keywords` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL COMMENT '关键字',
  `type` int DEFAULT NULL COMMENT '文章类型',
  `content` longtext CHARACTER SET utf8mb3 COLLATE utf8mb3_bin COMMENT '内容',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_id` varchar(50) COLLATE utf8mb3_bin DEFAULT NULL COMMENT '创建人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin ROW_FORMAT=COMPACT COMMENT='文章管理';

-- ----------------------------
-- Records of sys_content
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_dept`
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept` (
  `id` varchar(64) NOT NULL COMMENT '主键',
  `dept_no` varchar(18) DEFAULT NULL COMMENT '部门编号(规则：父级关系编码+自己的编码)',
  `name` varchar(300) DEFAULT NULL COMMENT '部门名称',
  `pid` varchar(64) NOT NULL COMMENT '父级id',
  `status` tinyint DEFAULT NULL COMMENT '状态(1:正常；0:弃用)',
  `relation_code` varchar(3000) DEFAULT NULL COMMENT '为了维护更深层级关系',
  `dept_manager_id` varchar(64) DEFAULT NULL COMMENT '部门经理user_id',
  `manager_name` varchar(255) DEFAULT NULL COMMENT '部门经理名称',
  `phone` varchar(20) DEFAULT NULL COMMENT '部门经理联系电话',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `deleted` tinyint DEFAULT NULL COMMENT '是否删除(1未删除；0已删除)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='系统部门';

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES ('1', 'D000001', '总公司', '0', '1', 'D000001', null, '小李', '13888888888', null, null, '1');

-- ----------------------------
-- Table structure for `sys_dict`
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict`;
CREATE TABLE `sys_dict` (
  `id` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `name` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '字典名称',
  `remark` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '描述',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 ROW_FORMAT=COMPACT COMMENT='数据字典表';

-- ----------------------------
-- Records of sys_dict
-- ----------------------------
INSERT INTO `sys_dict` VALUES ('1255790029680242690', 'sex', '性别', '2020-04-30 17:24:09');
INSERT INTO `sys_dict` VALUES ('1255790029680242691', 'content_keyword', '关键字', '2020-04-30 17:24:09');
INSERT INTO `sys_dict` VALUES ('1282504369620430849', 'content_type', '文章类型略略略', '2020-07-13 10:37:24');

-- ----------------------------
-- Table structure for `sys_dict_detail`
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_detail`;
CREATE TABLE `sys_dict_detail` (
  `id` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `label` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '字典标签',
  `value` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '字典值',
  `sort` smallint DEFAULT NULL COMMENT '排序',
  `dict_id` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '字典id',
  `create_time` datetime DEFAULT NULL COMMENT '创建日期',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 ROW_FORMAT=COMPACT COMMENT='数据字典详情';

-- ----------------------------
-- Records of sys_dict_detail
-- ----------------------------
INSERT INTO `sys_dict_detail` VALUES ('1255790073535885314', '男', '1', '1', '1255790029680242690', '2020-04-30 17:24:19');
INSERT INTO `sys_dict_detail` VALUES ('1255790100115189761', '女', '2', '2', '1255790029680242690', '2020-04-30 17:24:25');
INSERT INTO `sys_dict_detail` VALUES ('1282504475715350530', '诗词', '1', '1', '1282504369620430849', '2020-07-13 10:37:49');
INSERT INTO `sys_dict_detail` VALUES ('1282504651729317889', '散文', '2', '2', '1282504369620430849', '2020-07-13 10:38:31');
INSERT INTO `sys_dict_detail` VALUES ('1282846022950842369', '剧本', '3', '3', '1282504369620430849', '2020-07-14 09:15:01');
INSERT INTO `sys_dict_detail` VALUES ('1282846022950842370', 'java', '1', '1', '1255790029680242691', '2020-07-14 09:15:01');
INSERT INTO `sys_dict_detail` VALUES ('1282846022950842371', 'mysql', '2', '2', '1255790029680242691', '2020-07-14 09:15:01');

-- ----------------------------
-- Table structure for `sys_files`
-- ----------------------------
DROP TABLE IF EXISTS `sys_files`;
CREATE TABLE `sys_files` (
  `id` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `url` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT 'URL地址',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `file_name` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '文件名称',
  `file_path` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 ROW_FORMAT=COMPACT COMMENT='文件上传';

-- ----------------------------
-- Records of sys_files
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_job`
-- ----------------------------
DROP TABLE IF EXISTS `sys_job`;
CREATE TABLE `sys_job` (
  `id` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '任务id',
  `bean_name` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT 'spring bean名称',
  `params` varchar(2000) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '参数',
  `cron_expression` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT 'cron表达式',
  `status` tinyint DEFAULT NULL COMMENT '任务状态  0：正常  1：暂停',
  `remark` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '备注',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 ROW_FORMAT=COMPACT COMMENT='定时任务';

-- ----------------------------
-- Records of sys_job
-- ----------------------------
INSERT INTO `sys_job` VALUES ('1252884495040782337', 'testTask', '1', '0 */1 * * * ?', '0', '1', '2020-04-22 16:58:35');

-- ----------------------------
-- Table structure for `sys_job_log`
-- ----------------------------
DROP TABLE IF EXISTS `sys_job_log`;
CREATE TABLE `sys_job_log` (
  `id` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '任务日志id',
  `job_id` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '任务id',
  `bean_name` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT 'spring bean名称',
  `params` varchar(2000) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '参数',
  `status` tinyint NOT NULL COMMENT '任务状态    0：成功    1：失败',
  `error` varchar(2000) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '失败信息',
  `times` int NOT NULL COMMENT '耗时(单位：毫秒)',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `job_id` (`job_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 ROW_FORMAT=COMPACT COMMENT='定时任务日志';

-- ----------------------------
-- Records of sys_job_log
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_log`
-- ----------------------------
DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log` (
  `id` varchar(64) NOT NULL,
  `user_id` varchar(64) DEFAULT NULL COMMENT '用户id',
  `username` varchar(50) DEFAULT NULL COMMENT '用户名',
  `operation` varchar(50) DEFAULT NULL COMMENT '用户操作',
  `time` int DEFAULT NULL COMMENT '响应时间',
  `method` varchar(200) DEFAULT NULL COMMENT '请求方法',
  `params` varchar(5000) DEFAULT NULL COMMENT '请求参数',
  `ip` varchar(64) DEFAULT NULL COMMENT 'IP地址',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='系统日志';

-- ----------------------------
-- Records of sys_log
-- ----------------------------
INSERT INTO `sys_log` VALUES ('1768987739007074305', null, null, '用户管理-退出', '1', 'com.tian.controller.UserController.logout()', null, '192.168.1.8', '2024-03-16 21:08:38');
INSERT INTO `sys_log` VALUES ('1768989541920153601', null, null, '用户管理-退出', '3', 'com.tian.controller.UserController.logout()', null, '192.168.1.8', '2024-03-16 21:15:48');
INSERT INTO `sys_log` VALUES ('1768989598547451906', '1', 'admin', '机构管理-树型组织列表', '13', 'com.tian.controller.DeptController.getTree()', '[null]', '192.168.1.8', '2024-03-16 21:16:01');
INSERT INTO `sys_log` VALUES ('1768989598677475329', '1', 'admin', '用户管理-分页获取用户列表', '42', 'com.tian.controller.UserController.pageInfo()', '[{\"queryPage\":{\"current\":\"1\",\"hitCount\":false,\"optimizeCountSql\":true,\"orders\":[],\"pages\":\"0\",\"records\":[],\"searchCount\":true,\"size\":\"10\",\"total\":\"0\"}}]', '192.168.1.8', '2024-03-16 21:16:01');
INSERT INTO `sys_log` VALUES ('1768989691434508289', '1', 'admin', '用户管理-新增用户', '9', 'com.tian.controller.UserController.addUser()', '[{\"createTime\":1710594983327,\"createWhere\":1,\"deleted\":1,\"deptId\":\"1\",\"deptName\":\"总公司\",\"id\":\"1768989691405148161\",\"password\":\"4cf00bebb18f1366754d7c1bf1ce47a0\",\"phone\":\"18257160372\",\"queryPage\":{\"current\":\"1\",\"hitCount\":false,\"optimizeCountSql\":true,\"orders\":[],\"pages\":\"0\",\"records\":[],\"searchCount\":true,\"size\":\"10\",\"total\":\"0\"},\"salt\":\"f42fd8c183224644aeec\",\"status\":1,\"updateTime\":1710594983327,\"username\":\"tian\"}]', '192.168.1.8', '2024-03-16 21:16:23');
INSERT INTO `sys_log` VALUES ('1768989691543560193', '1', 'admin', '用户管理-分页获取用户列表', '8', 'com.tian.controller.UserController.pageInfo()', '[{\"nickName\":\"\",\"queryPage\":{\"current\":\"1\",\"hitCount\":false,\"optimizeCountSql\":true,\"orders\":[],\"pages\":\"0\",\"records\":[],\"searchCount\":true,\"size\":\"10\",\"total\":\"0\"},\"username\":\"\"}]', '192.168.1.8', '2024-03-16 21:16:23');
INSERT INTO `sys_log` VALUES ('1768989731951484930', '1', 'admin', '用户管理-赋予角色-获取所有角色接口', '6', 'com.tian.controller.UserController.getUserOwnRole()', '[\"1768989691405148161\"]', '192.168.1.8', '2024-03-16 21:16:33');
INSERT INTO `sys_log` VALUES ('1768989752239333378', '1', 'admin', '角色管理-分页获取角色信息', '8', 'com.tian.controller.RoleController.pageInfo()', '[{\"queryPage\":{\"current\":\"1\",\"hitCount\":false,\"optimizeCountSql\":true,\"orders\":[],\"pages\":\"0\",\"records\":[],\"searchCount\":true,\"size\":\"10\",\"total\":\"0\"}}]', '192.168.1.8', '2024-03-16 21:16:38');
INSERT INTO `sys_log` VALUES ('1768989767141695489', '1', 'admin', '菜单权限管理-获取所有目录菜单树', '86', 'com.tian.controller.PermissionController.getAllPermissionTree()', null, '192.168.1.8', '2024-03-16 21:16:41');
INSERT INTO `sys_log` VALUES ('1768990140090818562', null, null, '用户管理-退出', '1', 'com.tian.controller.UserController.logout()', null, '192.168.1.8', '2024-03-16 21:18:10');
INSERT INTO `sys_log` VALUES ('1768990225742700546', '1', 'admin', '菜单权限管理-获取所有菜单权限', '70', 'com.tian.controller.PermissionController.getAllMenusPermission()', null, '192.168.1.8', '2024-03-16 21:18:31');
INSERT INTO `sys_log` VALUES ('1768990285150822402', '1', 'admin', '菜单权限管理-获取所有目录菜单树', '65', 'com.tian.controller.PermissionController.getAllMenusPermissionTree()', '[\"\"]', '192.168.1.8', '2024-03-16 21:18:45');
INSERT INTO `sys_log` VALUES ('1768990368420339713', '1', 'admin', '菜单权限管理-新增菜单权限', '8', 'com.tian.controller.PermissionController.addPermission()', '[{\"createTime\":1710595144733,\"deleted\":1,\"icon\":\"layui-icon-app\",\"id\":\"1768990368386785282\",\"name\":\"站点管理\",\"orderNum\":5,\"perms\":\"\",\"pid\":\"0\",\"pidName\":\"默认顶级菜单\",\"status\":1,\"target\":\"_self\",\"type\":1,\"updateTime\":1710595144733,\"url\":\"\"}]', '192.168.1.8', '2024-03-16 21:19:05');
INSERT INTO `sys_log` VALUES ('1768990368743301122', '1', 'admin', '菜单权限管理-获取所有菜单权限', '67', 'com.tian.controller.PermissionController.getAllMenusPermission()', null, '192.168.1.8', '2024-03-16 21:19:05');
INSERT INTO `sys_log` VALUES ('1768990450247016449', '1', 'admin', '菜单权限管理-获取所有目录菜单树', '62', 'com.tian.controller.PermissionController.getAllMenusPermissionTree()', '[\"1768893700559691777\"]', '192.168.1.8', '2024-03-16 21:19:24');
INSERT INTO `sys_log` VALUES ('1768990507897724930', '1', 'admin', '菜单权限管理-更新菜单权限', '9', 'com.tian.controller.PermissionController.updatePermission()', '[{\"icon\":\"layui-icon-component\",\"id\":\"1768893700559691777\",\"name\":\"chargeStationInfo\",\"orderNum\":10,\"perms\":\"\",\"pid\":\"1768990368386785282\",\"pidName\":\"站点管理\",\"status\":1,\"target\":\"_self\",\"type\":2,\"updateTime\":1710595177988,\"url\":\"index/chargeStationInfo\"}]', '192.168.1.8', '2024-03-16 21:19:38');
INSERT INTO `sys_log` VALUES ('1768990508182937602', '1', 'admin', '菜单权限管理-获取所有菜单权限', '56', 'com.tian.controller.PermissionController.getAllMenusPermission()', null, '192.168.1.8', '2024-03-16 21:19:38');
INSERT INTO `sys_log` VALUES ('1768990521948643330', '1', 'admin', '角色管理-分页获取角色信息', '4', 'com.tian.controller.RoleController.pageInfo()', '[{\"queryPage\":{\"current\":\"1\",\"hitCount\":false,\"optimizeCountSql\":true,\"orders\":[],\"pages\":\"0\",\"records\":[],\"searchCount\":true,\"size\":\"10\",\"total\":\"0\"}}]', '192.168.1.8', '2024-03-16 21:19:41');
INSERT INTO `sys_log` VALUES ('1768990543687720961', '1', 'admin', '菜单权限管理-获取所有目录菜单树', '73', 'com.tian.controller.PermissionController.getAllPermissionTree()', null, '192.168.1.8', '2024-03-16 21:19:47');
INSERT INTO `sys_log` VALUES ('1768990678031278082', '1', 'admin', '角色管理-新增角色', '26', 'com.tian.controller.RoleController.addRole()', '[{\"createTime\":1710595218536,\"deleted\":1,\"description\":\"仅次于超级管理员\",\"id\":\"1768990677943197698\",\"name\":\"管理员\",\"permissions\":[\"51\",\"24\",\"42\",\"23\",\"52\",\"56\",\"57\",\"25\",\"10\",\"41\",\"22\",\"5\",\"12\",\"38\",\"9\",\"1768990368386785282\",\"1768893700559691777\",\"1768893700559691779\",\"1768893700559691780\",\"1768893700559691781\",\"1768893700559691778\"],\"queryPage\":{\"current\":\"1\",\"hitCount\":false,\"optimizeCountSql\":true,\"orders\":[],\"pages\":\"0\",\"records\":[],\"searchCount\":true,\"size\":\"10\",\"total\":\"0\"},\"status\":1,\"updateTime\":1710595218536}]', '192.168.1.8', '2024-03-16 21:20:19');
INSERT INTO `sys_log` VALUES ('1768990678102581249', '1', 'admin', '角色管理-分页获取角色信息', '3', 'com.tian.controller.RoleController.pageInfo()', '[{\"name\":\"\",\"queryPage\":{\"current\":\"1\",\"hitCount\":false,\"optimizeCountSql\":true,\"orders\":[],\"pages\":\"0\",\"records\":[],\"searchCount\":true,\"size\":\"10\",\"total\":\"0\"}}]', '192.168.1.8', '2024-03-16 21:20:19');
INSERT INTO `sys_log` VALUES ('1768990687195832321', '1', 'admin', '机构管理-树型组织列表', '2', 'com.tian.controller.DeptController.getTree()', '[null]', '192.168.1.8', '2024-03-16 21:20:21');
INSERT INTO `sys_log` VALUES ('1768990687220998145', '1', 'admin', '用户管理-分页获取用户列表', '6', 'com.tian.controller.UserController.pageInfo()', '[{\"queryPage\":{\"current\":\"1\",\"hitCount\":false,\"optimizeCountSql\":true,\"orders\":[],\"pages\":\"0\",\"records\":[],\"searchCount\":true,\"size\":\"10\",\"total\":\"0\"}}]', '192.168.1.8', '2024-03-16 21:20:21');
INSERT INTO `sys_log` VALUES ('1768990702169497602', '1', 'admin', '用户管理-赋予角色-获取所有角色接口', '3', 'com.tian.controller.UserController.getUserOwnRole()', '[\"1768989691405148161\"]', '192.168.1.8', '2024-03-16 21:20:24');
INSERT INTO `sys_log` VALUES ('1768990723598196737', '1', 'admin', '用户管理-赋予角色-用户赋予角色接口', '13', 'com.tian.controller.UserController.setUserOwnRole()', '[\"1768989691405148161\",[\"1768990677943197698\"]]', '192.168.1.8', '2024-03-16 21:20:29');
INSERT INTO `sys_log` VALUES ('1768990723673694210', '1', 'admin', '用户管理-分页获取用户列表', '4', 'com.tian.controller.UserController.pageInfo()', '[{\"nickName\":\"\",\"queryPage\":{\"current\":\"1\",\"hitCount\":false,\"optimizeCountSql\":true,\"orders\":[],\"pages\":\"0\",\"records\":[],\"searchCount\":true,\"size\":\"10\",\"total\":\"0\"},\"username\":\"\"}]', '192.168.1.8', '2024-03-16 21:20:29');
INSERT INTO `sys_log` VALUES ('1768990741570789377', null, null, '用户管理-退出', '9', 'com.tian.controller.UserController.logout()', null, '192.168.1.8', '2024-03-16 21:20:34');
INSERT INTO `sys_log` VALUES ('1768992083479633921', null, null, '用户管理-退出', '1', 'com.tian.controller.UserController.logout()', null, '192.168.1.8', '2024-03-16 21:25:54');
INSERT INTO `sys_log` VALUES ('1768992551463366657', null, null, '用户管理-退出', '5', 'com.tian.controller.UserController.logout()', null, '192.168.1.8', '2024-03-16 21:27:45');
INSERT INTO `sys_log` VALUES ('1768992834557915137', null, null, '用户管理-退出', '2', 'com.tian.controller.UserController.logout()', null, '192.168.1.8', '2024-03-16 21:28:53');
INSERT INTO `sys_log` VALUES ('1768992865365078018', '1', 'admin', '菜单权限管理-获取所有菜单权限', '98', 'com.tian.controller.PermissionController.getAllMenusPermission()', null, '192.168.1.8', '2024-03-16 21:29:00');
INSERT INTO `sys_log` VALUES ('1768992883471888385', '1', 'admin', '菜单权限管理-获取所有目录菜单树', '72', 'com.tian.controller.PermissionController.getAllMenusPermissionTree()', '[\"1768893700559691777\"]', '192.168.1.8', '2024-03-16 21:29:04');
INSERT INTO `sys_log` VALUES ('1768992925360402433', '1', 'admin', '菜单权限管理-更新菜单权限', '11', 'com.tian.controller.PermissionController.updatePermission()', '[{\"icon\":\"layui-icon-component\",\"id\":\"1768893700559691777\",\"name\":\"站点信息\",\"orderNum\":10,\"perms\":\"\",\"pid\":\"1768990368386785282\",\"pidName\":\"站点管理\",\"status\":1,\"target\":\"_self\",\"type\":2,\"updateTime\":1710595754350,\"url\":\"index/chargeStationInfo\"}]', '192.168.1.8', '2024-03-16 21:29:14');
INSERT INTO `sys_log` VALUES ('1768992925712723969', '1', 'admin', '菜单权限管理-获取所有菜单权限', '72', 'com.tian.controller.PermissionController.getAllMenusPermission()', null, '192.168.1.8', '2024-03-16 21:29:14');
INSERT INTO `sys_log` VALUES ('1768992955710386178', null, null, '用户管理-退出', '1', 'com.tian.controller.UserController.logout()', null, '192.168.1.8', '2024-03-16 21:29:22');
INSERT INTO `sys_log` VALUES ('1768993038921183234', null, null, '用户管理-退出', '1', 'com.tian.controller.UserController.logout()', null, '192.168.1.8', '2024-03-16 21:29:41');
INSERT INTO `sys_log` VALUES ('1768998006851973121', null, null, '用户管理-退出', '1', 'com.tian.controller.UserController.logout()', null, '192.168.1.8', '2024-03-16 21:49:26');
INSERT INTO `sys_log` VALUES ('1769003201019355138', null, null, '用户管理-退出', '4', 'com.tian.controller.UserController.logout()', null, '192.168.1.8', '2024-03-16 22:10:04');
INSERT INTO `sys_log` VALUES ('1769003236666744833', '1', 'admin', '菜单权限管理-获取所有菜单权限', '113', 'com.tian.controller.PermissionController.getAllMenusPermission()', null, '192.168.1.8', '2024-03-16 22:10:13');
INSERT INTO `sys_log` VALUES ('1769003297098276866', '1', 'admin', '菜单权限管理-获取所有目录菜单树', '82', 'com.tian.controller.PermissionController.getAllMenusPermissionTree()', '[\"1768998042776186881\"]', '192.168.1.8', '2024-03-16 22:10:27');
INSERT INTO `sys_log` VALUES ('1769003453961052162', '1', 'admin', '菜单权限管理-更新菜单权限', '14', 'com.tian.controller.PermissionController.updatePermission()', '[{\"icon\":\"layui-icon-unlink\",\"id\":\"1768998042776186881\",\"name\":\"充电枪信息\",\"orderNum\":10,\"perms\":\"\",\"pid\":\"1768990368386785282\",\"pidName\":\"站点管理\",\"status\":1,\"target\":\"_self\",\"type\":2,\"updateTime\":1710598264563,\"url\":\"index/chargeStationGun\"}]', '192.168.1.8', '2024-03-16 22:11:05');
INSERT INTO `sys_log` VALUES ('1769003454393065474', '1', 'admin', '菜单权限管理-获取所有菜单权限', '92', 'com.tian.controller.PermissionController.getAllMenusPermission()', null, '192.168.1.8', '2024-03-16 22:11:05');
INSERT INTO `sys_log` VALUES ('1769003793594818561', '1', 'admin', '角色管理-分页获取角色信息', '37', 'com.tian.controller.RoleController.pageInfo()', '[{\"queryPage\":{\"current\":\"1\",\"hitCount\":false,\"optimizeCountSql\":true,\"orders\":[],\"pages\":\"0\",\"records\":[],\"searchCount\":true,\"size\":\"10\",\"total\":\"0\"}}]', '192.168.1.8', '2024-03-16 22:12:26');
INSERT INTO `sys_log` VALUES ('1769003806089650178', '1', 'admin', '角色管理-查询角色详情', '91', 'com.tian.controller.RoleController.detailInfo()', '[\"1768990677943197698\"]', '192.168.1.8', '2024-03-16 22:12:29');
INSERT INTO `sys_log` VALUES ('1769003812196556801', '1', 'admin', '角色管理-分页获取角色信息', '3', 'com.tian.controller.RoleController.pageInfo()', '[{\"queryPage\":{\"current\":\"1\",\"hitCount\":false,\"optimizeCountSql\":true,\"orders\":[],\"pages\":\"0\",\"records\":[],\"searchCount\":true,\"size\":\"10\",\"total\":\"0\"}}]', '192.168.1.8', '2024-03-16 22:12:30');
INSERT INTO `sys_log` VALUES ('1769003817561071618', '1', 'admin', '角色管理-查询角色详情', '70', 'com.tian.controller.RoleController.detailInfo()', '[\"1768990677943197698\"]', '192.168.1.8', '2024-03-16 22:12:31');
INSERT INTO `sys_log` VALUES ('1769003848238211074', '1', 'admin', '角色管理-更新角色信息', '34', 'com.tian.controller.RoleController.updateDept()', '[{\"description\":\"仅次于超级管理员\",\"id\":\"1768990677943197698\",\"name\":\"管理员\",\"permissions\":[\"51\",\"24\",\"23\",\"25\",\"10\",\"52\",\"56\",\"57\",\"42\",\"41\",\"22\",\"5\",\"12\",\"38\",\"9\",\"1768990368386785282\",\"1768893700559691777\",\"1768893700559691781\",\"1768893700559691778\",\"1768893700559691779\",\"1768893700559691780\",\"1768998042776186881\",\"1768998042776186883\",\"1768998042776186885\",\"1768998042776186884\",\"1768998042776186882\"],\"queryPage\":{\"current\":\"1\",\"hitCount\":false,\"optimizeCountSql\":true,\"orders\":[],\"pages\":\"0\",\"records\":[],\"searchCount\":true,\"size\":\"10\",\"total\":\"0\"},\"status\":1,\"updateTime\":1710598358552}]', '192.168.1.8', '2024-03-16 22:12:39');
INSERT INTO `sys_log` VALUES ('1769003848322097153', '1', 'admin', '角色管理-分页获取角色信息', '4', 'com.tian.controller.RoleController.pageInfo()', '[{\"name\":\"\",\"queryPage\":{\"current\":\"1\",\"hitCount\":false,\"optimizeCountSql\":true,\"orders\":[],\"pages\":\"0\",\"records\":[],\"searchCount\":true,\"size\":\"10\",\"total\":\"0\"}}]', '192.168.1.8', '2024-03-16 22:12:39');
INSERT INTO `sys_log` VALUES ('1769003879766794241', null, null, '用户管理-退出', '1', 'com.tian.controller.UserController.logout()', null, '192.168.1.8', '2024-03-16 22:12:46');
INSERT INTO `sys_log` VALUES ('1769003958925893634', '1', 'admin', '机构管理-获取所有组织机构', '4', 'com.tian.controller.DeptController.getDeptAll()', null, '192.168.1.8', '2024-03-16 22:13:05');
INSERT INTO `sys_log` VALUES ('1769003964688867329', '1', 'admin', '机构管理-树型组织列表', '2', 'com.tian.controller.DeptController.getTree()', '[null]', '192.168.1.8', '2024-03-16 22:13:06');
INSERT INTO `sys_log` VALUES ('1769003964743393281', '1', 'admin', '用户管理-分页获取用户列表', '9', 'com.tian.controller.UserController.pageInfo()', '[{\"queryPage\":{\"current\":\"1\",\"hitCount\":false,\"optimizeCountSql\":true,\"orders\":[],\"pages\":\"0\",\"records\":[],\"searchCount\":true,\"size\":\"10\",\"total\":\"0\"}}]', '192.168.1.8', '2024-03-16 22:13:06');
INSERT INTO `sys_log` VALUES ('1769003972116979714', '1', 'admin', '角色管理-分页获取角色信息', '3', 'com.tian.controller.RoleController.pageInfo()', '[{\"queryPage\":{\"current\":\"1\",\"hitCount\":false,\"optimizeCountSql\":true,\"orders\":[],\"pages\":\"0\",\"records\":[],\"searchCount\":true,\"size\":\"10\",\"total\":\"0\"}}]', '192.168.1.8', '2024-03-16 22:13:08');
INSERT INTO `sys_log` VALUES ('1769003977519243266', '1', 'admin', '菜单权限管理-获取所有菜单权限', '67', 'com.tian.controller.PermissionController.getAllMenusPermission()', null, '192.168.1.8', '2024-03-16 22:13:09');
INSERT INTO `sys_log` VALUES ('1769004002848645121', null, null, '用户管理-退出', '1', 'com.tian.controller.UserController.logout()', null, '192.168.1.8', '2024-03-16 22:13:15');
INSERT INTO `sys_log` VALUES ('1769004031818702849', '1', 'admin', '菜单权限管理-获取所有菜单权限', '49', 'com.tian.controller.PermissionController.getAllMenusPermission()', null, '192.168.1.8', '2024-03-16 22:13:22');
INSERT INTO `sys_log` VALUES ('1769004063322120193', '1', 'admin', '角色管理-分页获取角色信息', '4', 'com.tian.controller.RoleController.pageInfo()', '[{\"queryPage\":{\"current\":\"1\",\"hitCount\":false,\"optimizeCountSql\":true,\"orders\":[],\"pages\":\"0\",\"records\":[],\"searchCount\":true,\"size\":\"10\",\"total\":\"0\"}}]', '192.168.1.8', '2024-03-16 22:13:30');
INSERT INTO `sys_log` VALUES ('1769004074311196673', '1', 'admin', '角色管理-查询角色详情', '56', 'com.tian.controller.RoleController.detailInfo()', '[\"1768990677943197698\"]', '192.168.1.8', '2024-03-16 22:13:32');
INSERT INTO `sys_log` VALUES ('1769004104623431682', '1', 'admin', '角色管理-更新角色信息', '13', 'com.tian.controller.RoleController.updateDept()', '[{\"description\":\"仅次于超级管理员\",\"id\":\"1768990677943197698\",\"name\":\"管理员\",\"permissions\":[\"51\",\"24\",\"23\",\"25\",\"10\",\"52\",\"56\",\"57\",\"42\",\"41\",\"22\",\"5\",\"12\",\"38\",\"9\",\"1768990368386785282\",\"1768893700559691777\",\"1768893700559691781\",\"1768893700559691778\",\"1768893700559691779\",\"1768893700559691780\",\"1768998042776186881\",\"1768998042776186883\",\"1768998042776186885\",\"1768998042776186884\",\"1768998042776186882\"],\"queryPage\":{\"current\":\"1\",\"hitCount\":false,\"optimizeCountSql\":true,\"orders\":[],\"pages\":\"0\",\"records\":[],\"searchCount\":true,\"size\":\"10\",\"total\":\"0\"},\"status\":1,\"updateTime\":1710598419695}]', '192.168.1.8', '2024-03-16 22:13:40');
INSERT INTO `sys_log` VALUES ('1769004104698929154', '1', 'admin', '角色管理-分页获取角色信息', '3', 'com.tian.controller.RoleController.pageInfo()', '[{\"name\":\"\",\"queryPage\":{\"current\":\"1\",\"hitCount\":false,\"optimizeCountSql\":true,\"orders\":[],\"pages\":\"0\",\"records\":[],\"searchCount\":true,\"size\":\"10\",\"total\":\"0\"}}]', '192.168.1.8', '2024-03-16 22:13:40');
INSERT INTO `sys_log` VALUES ('1769004363315519490', null, null, '用户管理-退出', '1', 'com.tian.controller.UserController.logout()', null, '192.168.1.8', '2024-03-16 22:14:41');
INSERT INTO `sys_log` VALUES ('1769009314766897154', null, null, '用户管理-退出', '4', 'com.tian.controller.UserController.logout()', null, '192.168.1.8', '2024-03-16 22:34:22');
INSERT INTO `sys_log` VALUES ('1769009401626738690', '1', 'admin', '菜单权限管理-获取所有菜单权限', '107', 'com.tian.controller.PermissionController.getAllMenusPermission()', null, '192.168.1.8', '2024-03-16 22:34:43');
INSERT INTO `sys_log` VALUES ('1769009441728479234', null, null, '用户管理-退出', '2', 'com.tian.controller.UserController.logout()', null, '192.168.1.8', '2024-03-16 22:34:52');
INSERT INTO `sys_log` VALUES ('1769009534749753345', '1768989691405148161', 'tian', '机构管理-获取所有组织机构', '5', 'com.tian.controller.DeptController.getDeptAll()', null, '192.168.1.8', '2024-03-16 22:35:14');
INSERT INTO `sys_log` VALUES ('1769009540168794113', '1768989691405148161', 'tian', '机构管理-树型组织列表', '7', 'com.tian.controller.DeptController.getTree()', '[\"\"]', '192.168.1.8', '2024-03-16 22:35:16');
INSERT INTO `sys_log` VALUES ('1769010587578777602', null, null, '用户管理-退出', '1', 'com.tian.controller.UserController.logout()', null, '192.168.1.8', '2024-03-16 22:39:25');
INSERT INTO `sys_log` VALUES ('1769010613273083905', '1', 'admin', '角色管理-分页获取角色信息', '16', 'com.tian.controller.RoleController.pageInfo()', '[{\"queryPage\":{\"current\":\"1\",\"hitCount\":false,\"optimizeCountSql\":true,\"orders\":[],\"pages\":\"0\",\"records\":[],\"searchCount\":true,\"size\":\"10\",\"total\":\"0\"}}]', '192.168.1.8', '2024-03-16 22:39:31');
INSERT INTO `sys_log` VALUES ('1769010621263233025', '1', 'admin', '菜单权限管理-获取所有目录菜单树', '90', 'com.tian.controller.PermissionController.getAllPermissionTree()', null, '192.168.1.8', '2024-03-16 22:39:33');
INSERT INTO `sys_log` VALUES ('1769010782211260418', '1', 'admin', '机构管理-树型组织列表', '1', 'com.tian.controller.DeptController.getTree()', '[null]', '192.168.1.8', '2024-03-16 22:40:12');
INSERT INTO `sys_log` VALUES ('1769010782269980673', '1', 'admin', '用户管理-分页获取用户列表', '9', 'com.tian.controller.UserController.pageInfo()', '[{\"queryPage\":{\"current\":\"1\",\"hitCount\":false,\"optimizeCountSql\":true,\"orders\":[],\"pages\":\"0\",\"records\":[],\"searchCount\":true,\"size\":\"10\",\"total\":\"0\"}}]', '192.168.1.8', '2024-03-16 22:40:12');
INSERT INTO `sys_log` VALUES ('1769017706784866306', null, null, '用户管理-退出', '1', 'com.tian.controller.UserController.logout()', null, '192.168.1.8', '2024-03-16 23:07:43');
INSERT INTO `sys_log` VALUES ('1769017747901628418', null, null, '用户管理-退出', '0', 'com.tian.controller.UserController.logout()', null, '192.168.1.8', '2024-03-16 23:07:53');
INSERT INTO `sys_log` VALUES ('1769182664579371009', null, null, '用户管理-退出', '3', 'com.tian.controller.UserController.logout()', null, '192.168.1.8', '2024-03-17 10:03:12');
INSERT INTO `sys_log` VALUES ('1769187727595118593', null, null, '用户管理-退出', '4', 'com.tian.controller.UserController.logout()', null, '192.168.1.8', '2024-03-17 10:23:19');
INSERT INTO `sys_log` VALUES ('1769267025530150914', null, null, '用户管理-退出', '3', 'com.tian.controller.sys.UserController.logout()', null, '192.168.1.8', '2024-03-17 15:38:25');
INSERT INTO `sys_log` VALUES ('1769267084925689857', null, null, '用户管理-退出', '2', 'com.tian.controller.sys.UserController.logout()', null, '192.168.1.8', '2024-03-17 15:38:39');

-- ----------------------------
-- Table structure for `sys_permission`
-- ----------------------------
DROP TABLE IF EXISTS `sys_permission`;
CREATE TABLE `sys_permission` (
  `id` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '主键',
  `name` varchar(300) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '菜单权限名称',
  `perms` varchar(500) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '授权(多个用逗号分隔，如：sys:user:add,sys:user:edit)',
  `icon` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '图标',
  `url` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '访问地址URL',
  `target` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT 'a target属性:_self _blank',
  `pid` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '父级菜单权限名称',
  `order_num` int DEFAULT NULL COMMENT '排序',
  `type` tinyint DEFAULT NULL COMMENT '菜单权限类型(1:目录;2:菜单;3:按钮)',
  `status` tinyint DEFAULT NULL COMMENT '状态1:正常 0：禁用',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `deleted` tinyint DEFAULT NULL COMMENT '是否删除(1未删除；0已删除)',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 ROW_FORMAT=COMPACT COMMENT='系统权限';

-- ----------------------------
-- Records of sys_permission
-- ----------------------------
INSERT INTO `sys_permission` VALUES ('1', '删除', 'sysGenerator:delete', null, 'sysGenerator/delete', null, '15', '1', '3', '1', '2020-03-19 13:29:40', '2020-03-19 13:29:40', '1');
INSERT INTO `sys_permission` VALUES ('10', '赋予角色', 'sys:user:role:update', null, 'sys/user/roles/*', null, '24', '100', '3', '1', '2020-03-19 13:29:40', '2020-03-19 13:29:40', '1');
INSERT INTO `sys_permission` VALUES ('11', '菜单权限管理', null, null, 'index/menus', '_self', '51', '98', '2', '1', '2020-03-19 13:29:40', '2020-03-19 13:29:40', '1');
INSERT INTO `sys_permission` VALUES ('12', '列表', 'sys:dept:list', null, 'sys/depts', null, '41', '100', '3', '1', '2020-03-19 13:29:40', '2020-03-19 13:29:40', '1');
INSERT INTO `sys_permission` VALUES ('13', '删除', 'sys:role:deleted', null, 'sys/role/*', null, '53', '100', '3', '1', '2020-03-19 13:29:40', '2020-03-19 13:29:40', '1');
INSERT INTO `sys_permission` VALUES ('1311115974068449281', '数据权限', 'sys:role:bindDept', '', 'sys/role/bindDept', '_self', '53', '5', '3', '1', '2020-09-30 09:29:42', null, '1');
INSERT INTO `sys_permission` VALUES ('14', '定时任务立即执行', 'sysJob:run', null, 'sysJob/run', '_self', '59', '5', '3', '1', '2020-04-22 15:47:54', null, '1');
INSERT INTO `sys_permission` VALUES ('15', '代码生成', null, null, 'index/sysGenerator', '_self', '54', '1', '2', '1', '2020-03-19 13:29:40', '2020-03-19 13:29:40', '1');
INSERT INTO `sys_permission` VALUES ('16', '列表', 'sysGenerator:list', null, 'sysGenerator/listByPage', null, '15', '1', '3', '1', '2020-03-19 13:29:40', '2020-03-19 13:29:40', '1');
INSERT INTO `sys_permission` VALUES ('17', '详情', 'sys:permission:detail', null, 'sys/permission/*', null, '11', '100', '3', '1', '2020-03-19 13:29:40', '2020-03-19 13:29:40', '1');
INSERT INTO `sys_permission` VALUES ('1768893700559691777', '站点信息', '', 'layui-icon-component', 'index/chargeStationInfo', '_self', '1768990368386785282', '10', '2', '1', null, '2024-03-16 21:29:14', '1');
INSERT INTO `sys_permission` VALUES ('1768893700559691778', '新增', 'chargeStationInfo:add', null, 'chargeStationInfo/add', null, '1768893700559691777', null, '3', '1', null, null, '1');
INSERT INTO `sys_permission` VALUES ('1768893700559691779', '修改', 'chargeStationInfo:update', null, 'chargeStationInfo/update', null, '1768893700559691777', null, '3', '1', null, null, '1');
INSERT INTO `sys_permission` VALUES ('1768893700559691780', '删除', 'chargeStationInfo:delete', null, 'chargeStationInfo/delete', null, '1768893700559691777', null, '3', '1', null, null, '1');
INSERT INTO `sys_permission` VALUES ('1768893700559691781', '列表', 'chargeStationInfo:list', null, 'chargeStationInfo/listByPage', null, '1768893700559691777', null, '3', '1', null, null, '1');
INSERT INTO `sys_permission` VALUES ('1768990368386785282', '站点管理', '', 'layui-icon-app', '', '_self', '0', '5', '1', '1', '2024-03-16 21:19:05', '2024-03-16 21:19:05', '1');
INSERT INTO `sys_permission` VALUES ('1768998042776186881', '充电枪信息', '', 'layui-icon-unlink', 'index/chargeStationGun', '_self', '1768990368386785282', '10', '2', '1', null, '2024-03-16 22:11:05', '1');
INSERT INTO `sys_permission` VALUES ('1768998042776186882', '新增', 'chargeStationGun:add', null, 'chargeStationGun/add', null, '1768998042776186881', null, '3', '1', null, null, '1');
INSERT INTO `sys_permission` VALUES ('1768998042776186883', '修改', 'chargeStationGun:update', null, 'chargeStationGun/update', null, '1768998042776186881', null, '3', '1', null, null, '1');
INSERT INTO `sys_permission` VALUES ('1768998042776186884', '删除', 'chargeStationGun:delete', null, 'chargeStationGun/delete', null, '1768998042776186881', null, '3', '1', null, null, '1');
INSERT INTO `sys_permission` VALUES ('1768998042776186885', '列表', 'chargeStationGun:list', null, 'chargeStationGun/listByPage', null, '1768998042776186881', null, '3', '1', null, null, '1');
INSERT INTO `sys_permission` VALUES ('18', '定时任务恢复', 'sysJob:resume', null, 'sysJob/resume', '_self', '59', '4', '3', '1', '2020-04-22 15:48:40', null, '1');
INSERT INTO `sys_permission` VALUES ('19', '列表', 'sys:role:list', null, 'sys/roles', null, '53', '0', '3', '1', '2020-03-19 13:29:40', '2020-03-19 13:29:40', '1');
INSERT INTO `sys_permission` VALUES ('2', 'SQL 监控', '', '', 'druid/sql.html', '_self', '21', '98', '2', '1', '2020-03-19 13:29:40', '2020-05-07 13:36:59', '1');
INSERT INTO `sys_permission` VALUES ('20', '修改', 'sysGenerator:update', null, 'sysGenerator/update', null, '15', '1', '3', '1', '2020-03-19 13:29:40', '2020-03-19 13:29:40', '1');
INSERT INTO `sys_permission` VALUES ('21', '其他', null, 'layui-icon-list', null, null, '0', '200', '1', '1', '2020-03-19 13:29:40', '2020-03-19 13:29:40', '1');
INSERT INTO `sys_permission` VALUES ('22', '详情', 'sys:dept:detail', null, 'sys/dept/*', null, '41', '100', '3', '1', '2020-03-19 13:29:40', '2020-03-19 13:29:40', '1');
INSERT INTO `sys_permission` VALUES ('23', '列表', 'sys:user:list', null, 'sys/users', null, '24', '100', '3', '1', '2020-03-19 13:29:40', '2020-03-19 13:29:40', '1');
INSERT INTO `sys_permission` VALUES ('24', '用户管理', null, null, 'index/users', '_self', '51', '100', '2', '1', '2020-03-19 13:29:40', '2020-03-19 13:29:40', '1');
INSERT INTO `sys_permission` VALUES ('25', '详情', 'sys:user:detail', null, 'sys/user/*', null, '24', '100', '3', '1', '2020-03-19 13:29:40', '2020-03-19 13:29:40', '1');
INSERT INTO `sys_permission` VALUES ('26', '删除', 'sys:permission:deleted', null, 'sys/permission/*', null, '11', '100', '3', '1', '2020-03-19 13:29:40', '2020-03-19 13:29:40', '1');
INSERT INTO `sys_permission` VALUES ('27', '文件管理', '', '', 'index/sysFiles', '_self', '54', '10', '2', '1', null, '2020-06-15 16:00:29', '1');
INSERT INTO `sys_permission` VALUES ('28', '列表', 'sysFiles:list', null, 'sysFiles/listByPage', null, '27', '0', '3', '1', null, null, '1');
INSERT INTO `sys_permission` VALUES ('29', '新增', 'sysFiles:add', null, 'sysFiles/add', null, '27', '0', '3', '1', null, null, '1');
INSERT INTO `sys_permission` VALUES ('3', '新增', 'sys:role:add', null, 'sys/role', null, '53', '0', '3', '1', '2020-03-19 13:29:40', '2020-03-19 13:29:40', '1');
INSERT INTO `sys_permission` VALUES ('30', '删除', 'sysFiles:delete', null, 'sysFiles/delete', null, '27', '0', '3', '1', null, null, '1');
INSERT INTO `sys_permission` VALUES ('31', '文章管理', null, null, 'index/sysContent', '_self', '54', '10', '2', '1', null, null, '1');
INSERT INTO `sys_permission` VALUES ('32', '列表', 'sysContent:list', null, 'sysContent/listByPage', null, '31', '0', '3', '1', null, null, '1');
INSERT INTO `sys_permission` VALUES ('33', '新增', 'sysContent:add', null, 'sysContent/add', null, '31', '0', '3', '1', null, null, '1');
INSERT INTO `sys_permission` VALUES ('34', '修改', 'sysContent:update', null, 'sysContent/update', null, '31', '0', '3', '1', null, null, '1');
INSERT INTO `sys_permission` VALUES ('35', '删除', 'sysContent:delete', null, 'sysContent/delete', null, '31', '0', '3', '1', null, null, '1');
INSERT INTO `sys_permission` VALUES ('36', '更新', 'sys:role:update', null, 'sys/role', null, '53', '0', '3', '1', '2020-03-19 13:29:40', '2020-03-19 13:29:40', '1');
INSERT INTO `sys_permission` VALUES ('38', '更新', 'sys:dept:update', null, 'sys/dept', null, '41', '100', '3', '1', '2020-03-19 13:29:40', '2020-03-19 13:29:40', '1');
INSERT INTO `sys_permission` VALUES ('39', '详情', 'sys:role:detail', null, 'sys/role/*', null, '53', '100', '3', '1', '2020-03-19 13:29:40', '2020-03-19 13:29:40', '1');
INSERT INTO `sys_permission` VALUES ('4', '添加', 'sysGenerator:add', null, 'sysGenerator/add', null, '15', '1', '3', '1', '2020-03-19 13:29:40', '2020-03-19 13:29:40', '1');
INSERT INTO `sys_permission` VALUES ('40', '编辑', 'sys:permission:update', null, 'sys/permission', null, '11', '100', '3', '1', '2020-03-19 13:29:40', '2020-03-19 13:29:40', '1');
INSERT INTO `sys_permission` VALUES ('41', '部门管理', null, null, 'index/depts', '_self', '51', '100', '2', '1', '2020-03-19 13:29:40', '2020-03-19 13:29:40', '1');
INSERT INTO `sys_permission` VALUES ('42', '新增', 'sys:user:add', null, 'sys/user', null, '24', '100', '3', '1', '2020-03-19 13:29:40', '2020-03-19 13:29:40', '1');
INSERT INTO `sys_permission` VALUES ('43', '列表', 'sys:permission:list', null, 'sys/permissions', null, '11', '100', '3', '1', '2020-03-19 13:29:40', '2020-03-19 13:29:40', '1');
INSERT INTO `sys_permission` VALUES ('44', '新增', 'sys:permission:add', null, 'sys/permission', null, '11', '100', '3', '1', '2020-03-19 13:29:40', '2020-03-19 13:29:40', '1');
INSERT INTO `sys_permission` VALUES ('45', '字典管理', null, '', 'index/sysDict', null, '54', '10', '2', '1', null, null, '1');
INSERT INTO `sys_permission` VALUES ('46', '列表', 'sysDict:list', null, 'sysDict/listByPage', null, '45', '0', '3', '1', null, null, '1');
INSERT INTO `sys_permission` VALUES ('47', '新增', 'sysDict:add', null, 'sysDict/add', null, '45', '0', '3', '1', null, null, '1');
INSERT INTO `sys_permission` VALUES ('48', '修改', 'sysDict:update', null, 'sysDict/update', null, '45', '0', '3', '1', null, null, '1');
INSERT INTO `sys_permission` VALUES ('49', '删除', 'sysDict:delete', null, 'sysDict/delete', null, '45', '0', '3', '1', null, null, '1');
INSERT INTO `sys_permission` VALUES ('5', '删除', 'sys:dept:deleted', null, 'sys/dept/*', null, '41', '100', '3', '1', '2020-03-19 13:29:40', '2020-03-19 13:29:40', '1');
INSERT INTO `sys_permission` VALUES ('50', '表单构建', '', '', 'index/build', '_self', '21', '1', '2', '1', '2020-04-22 13:09:41', '2020-05-07 13:36:47', '1');
INSERT INTO `sys_permission` VALUES ('51', '组织管理', null, 'layui-icon-user', null, null, '0', '1', '1', '1', '2020-03-19 13:29:40', '2020-03-19 13:29:40', '1');
INSERT INTO `sys_permission` VALUES ('52', '拥有角色', 'sys:user:role:detail', null, 'sys/user/roles/*', null, '24', '100', '3', '1', '2020-03-19 13:29:40', '2020-03-19 13:29:40', '1');
INSERT INTO `sys_permission` VALUES ('53', '角色管理', null, null, 'index/roles', '_self', '51', '99', '2', '1', '2020-03-19 13:29:40', '2020-03-19 13:29:40', '1');
INSERT INTO `sys_permission` VALUES ('54', '系统管理', null, 'layui-icon-set-fill', null, null, '0', '98', '1', '1', '2020-03-19 13:29:40', '2020-03-19 13:29:40', '1');
INSERT INTO `sys_permission` VALUES ('55', '定时任务暂停', 'sysJob:pause', null, 'sysJob/pause', '_self', '59', '1', '3', '1', '2020-04-22 15:48:18', null, '1');
INSERT INTO `sys_permission` VALUES ('56', '更新', 'sys:user:update', null, 'sys/user', null, '24', '100', '3', '1', '2020-03-19 13:29:40', '2020-03-19 13:29:40', '1');
INSERT INTO `sys_permission` VALUES ('57', '删除', 'sys:user:deleted', null, 'sys/user', null, '24', '100', '3', '1', '2020-03-19 13:29:40', '2020-03-19 13:29:40', '1');
INSERT INTO `sys_permission` VALUES ('58', '删除', 'sys:log:deleted', null, 'sys/logs', null, '8', '100', '3', '1', '2020-03-19 13:29:40', '2020-03-19 13:29:40', '1');
INSERT INTO `sys_permission` VALUES ('59', '定时任务', null, null, 'index/sysJob', '_self', '54', '10', '2', '1', null, null, '1');
INSERT INTO `sys_permission` VALUES ('6', '接口管理', '', '', 'doc.html', '_blank', '21', '100', '2', '1', '2020-03-19 13:29:40', '2020-05-07 13:36:02', '1');
INSERT INTO `sys_permission` VALUES ('60', '列表', 'sysJob:list', null, 'sysJob/listByPage', null, '59', '0', '3', '1', null, null, '1');
INSERT INTO `sys_permission` VALUES ('61', '新增', 'sysJob:add', null, 'sysJob/add', null, '59', '0', '3', '1', null, null, '1');
INSERT INTO `sys_permission` VALUES ('62', '修改', 'sysJob:update', null, 'sysJob/update', null, '59', '0', '3', '1', null, null, '1');
INSERT INTO `sys_permission` VALUES ('63', '删除', 'sysJob:delete', null, 'sysJob/delete', null, '59', '0', '3', '1', null, null, '1');
INSERT INTO `sys_permission` VALUES ('7', '列表', 'sys:log:list', null, 'sys/logs', null, '8', '100', '3', '1', '2020-03-19 13:29:40', '2020-03-19 13:29:40', '1');
INSERT INTO `sys_permission` VALUES ('8', '日志管理', null, null, 'index/logs', '_self', '54', '97', '2', '1', '2020-03-19 13:29:40', '2020-03-19 13:29:40', '1');
INSERT INTO `sys_permission` VALUES ('9', '新增', 'sys:dept:add', null, 'sys/dept', null, '41', '100', '3', '1', '2020-03-19 13:29:40', '2020-03-19 13:29:40', '1');

-- ----------------------------
-- Table structure for `sys_role`
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `id` varchar(64) NOT NULL COMMENT '主键',
  `name` varchar(255) DEFAULT NULL COMMENT '角色名称',
  `description` varchar(300) DEFAULT NULL,
  `status` tinyint DEFAULT NULL COMMENT '状态(1:正常0:弃用)',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `deleted` tinyint DEFAULT NULL COMMENT '是否删除(1未删除；0已删除)',
  `data_scope` int DEFAULT NULL COMMENT '数据范围（1：所有 2：自定义 3： 本部门及以下部门 4：仅本部门 5:自己）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='系统角色';

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('1', '超级管理员', '拥有所有权限-不能删除', '1', '2019-11-01 19:26:29', '2020-03-19 13:29:51', '1', null);
INSERT INTO `sys_role` VALUES ('1768990677943197698', '管理员', '仅次于超级管理员', '1', '2024-03-16 21:20:19', '2024-03-16 22:13:40', '1', null);

-- ----------------------------
-- Table structure for `sys_role_dept`
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_dept`;
CREATE TABLE `sys_role_dept` (
  `id` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '主键',
  `role_id` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '角色id',
  `dept_id` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '部门id',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 ROW_FORMAT=COMPACT COMMENT='角色部门';

-- ----------------------------
-- Records of sys_role_dept
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_role_permission`
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_permission`;
CREATE TABLE `sys_role_permission` (
  `id` varchar(64) NOT NULL COMMENT '主键',
  `role_id` varchar(64) DEFAULT NULL COMMENT '角色id',
  `permission_id` varchar(64) DEFAULT NULL COMMENT '菜单权限id',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Records of sys_role_permission
-- ----------------------------
INSERT INTO `sys_role_permission` VALUES ('1', '1', '1', '2020-04-22 15:48:47');
INSERT INTO `sys_role_permission` VALUES ('10', '1', '10', '2020-04-22 15:48:47');
INSERT INTO `sys_role_permission` VALUES ('11', '1', '11', '2020-04-22 15:48:47');
INSERT INTO `sys_role_permission` VALUES ('12', '1', '12', '2020-04-22 15:48:47');
INSERT INTO `sys_role_permission` VALUES ('13', '1', '13', '2020-04-22 15:48:47');
INSERT INTO `sys_role_permission` VALUES ('1311116066716430339', '1', '1311115974068449281', '2020-09-30 09:30:04');
INSERT INTO `sys_role_permission` VALUES ('14', '1', '14', '2020-05-26 17:04:21');
INSERT INTO `sys_role_permission` VALUES ('15', '1', '15', '2020-04-22 15:48:47');
INSERT INTO `sys_role_permission` VALUES ('16', '1', '16', '2020-04-22 15:48:47');
INSERT INTO `sys_role_permission` VALUES ('17', '1', '17', '2020-04-22 15:48:47');
INSERT INTO `sys_role_permission` VALUES ('1768893700559691782', '1', '1768893700559691777', null);
INSERT INTO `sys_role_permission` VALUES ('1768893700559691783', '1', '1768893700559691778', null);
INSERT INTO `sys_role_permission` VALUES ('1768893700559691784', '1', '1768893700559691779', null);
INSERT INTO `sys_role_permission` VALUES ('1768893700559691785', '1', '1768893700559691780', null);
INSERT INTO `sys_role_permission` VALUES ('1768893700559691786', '1', '1768893700559691781', null);
INSERT INTO `sys_role_permission` VALUES ('1768998042776186886', '1', '1768998042776186881', null);
INSERT INTO `sys_role_permission` VALUES ('1768998042776186887', '1', '1768998042776186882', null);
INSERT INTO `sys_role_permission` VALUES ('1768998042776186888', '1', '1768998042776186883', null);
INSERT INTO `sys_role_permission` VALUES ('1768998042776186889', '1', '1768998042776186884', null);
INSERT INTO `sys_role_permission` VALUES ('1768998042776186890', '1', '1768998042776186885', null);
INSERT INTO `sys_role_permission` VALUES ('1769004104585682945', '1768990677943197698', '51', '2024-03-16 22:13:40');
INSERT INTO `sys_role_permission` VALUES ('1769004104585682946', '1768990677943197698', '24', '2024-03-16 22:13:40');
INSERT INTO `sys_role_permission` VALUES ('1769004104585682947', '1768990677943197698', '23', '2024-03-16 22:13:40');
INSERT INTO `sys_role_permission` VALUES ('1769004104585682948', '1768990677943197698', '25', '2024-03-16 22:13:40');
INSERT INTO `sys_role_permission` VALUES ('1769004104585682949', '1768990677943197698', '10', '2024-03-16 22:13:40');
INSERT INTO `sys_role_permission` VALUES ('1769004104594071553', '1768990677943197698', '52', '2024-03-16 22:13:40');
INSERT INTO `sys_role_permission` VALUES ('1769004104594071554', '1768990677943197698', '56', '2024-03-16 22:13:40');
INSERT INTO `sys_role_permission` VALUES ('1769004104594071555', '1768990677943197698', '57', '2024-03-16 22:13:40');
INSERT INTO `sys_role_permission` VALUES ('1769004104594071556', '1768990677943197698', '42', '2024-03-16 22:13:40');
INSERT INTO `sys_role_permission` VALUES ('1769004104594071557', '1768990677943197698', '41', '2024-03-16 22:13:40');
INSERT INTO `sys_role_permission` VALUES ('1769004104594071558', '1768990677943197698', '22', '2024-03-16 22:13:40');
INSERT INTO `sys_role_permission` VALUES ('1769004104594071559', '1768990677943197698', '5', '2024-03-16 22:13:40');
INSERT INTO `sys_role_permission` VALUES ('1769004104594071560', '1768990677943197698', '12', '2024-03-16 22:13:40');
INSERT INTO `sys_role_permission` VALUES ('1769004104598265857', '1768990677943197698', '38', '2024-03-16 22:13:40');
INSERT INTO `sys_role_permission` VALUES ('1769004104598265858', '1768990677943197698', '9', '2024-03-16 22:13:40');
INSERT INTO `sys_role_permission` VALUES ('1769004104598265859', '1768990677943197698', '1768990368386785282', '2024-03-16 22:13:40');
INSERT INTO `sys_role_permission` VALUES ('1769004104598265860', '1768990677943197698', '1768893700559691777', '2024-03-16 22:13:40');
INSERT INTO `sys_role_permission` VALUES ('1769004104598265861', '1768990677943197698', '1768893700559691781', '2024-03-16 22:13:40');
INSERT INTO `sys_role_permission` VALUES ('1769004104598265862', '1768990677943197698', '1768893700559691778', '2024-03-16 22:13:40');
INSERT INTO `sys_role_permission` VALUES ('1769004104602460161', '1768990677943197698', '1768893700559691779', '2024-03-16 22:13:40');
INSERT INTO `sys_role_permission` VALUES ('1769004104602460162', '1768990677943197698', '1768893700559691780', '2024-03-16 22:13:40');
INSERT INTO `sys_role_permission` VALUES ('1769004104602460163', '1768990677943197698', '1768998042776186881', '2024-03-16 22:13:40');
INSERT INTO `sys_role_permission` VALUES ('1769004104602460164', '1768990677943197698', '1768998042776186883', '2024-03-16 22:13:40');
INSERT INTO `sys_role_permission` VALUES ('1769004104602460165', '1768990677943197698', '1768998042776186885', '2024-03-16 22:13:40');
INSERT INTO `sys_role_permission` VALUES ('1769004104602460166', '1768990677943197698', '1768998042776186884', '2024-03-16 22:13:40');
INSERT INTO `sys_role_permission` VALUES ('1769004104602460167', '1768990677943197698', '1768998042776186882', '2024-03-16 22:13:40');
INSERT INTO `sys_role_permission` VALUES ('18', '1', '18', '2020-04-22 15:48:47');
INSERT INTO `sys_role_permission` VALUES ('19', '1', '19', '2020-04-22 15:48:47');
INSERT INTO `sys_role_permission` VALUES ('2', '1', '2', '2020-04-22 15:48:47');
INSERT INTO `sys_role_permission` VALUES ('20', '1', '20', '2020-05-26 17:04:21');
INSERT INTO `sys_role_permission` VALUES ('21', '1', '21', '2020-04-22 15:48:47');
INSERT INTO `sys_role_permission` VALUES ('22', '1', '22', '2020-04-22 15:48:47');
INSERT INTO `sys_role_permission` VALUES ('23', '1', '23', '2020-04-22 15:48:47');
INSERT INTO `sys_role_permission` VALUES ('24', '1', '24', '2020-04-22 15:48:47');
INSERT INTO `sys_role_permission` VALUES ('25', '1', '25', '2020-04-22 15:48:47');
INSERT INTO `sys_role_permission` VALUES ('26', '1', '26', '2020-04-22 15:48:47');
INSERT INTO `sys_role_permission` VALUES ('27', '1', '27', '2020-04-22 15:48:47');
INSERT INTO `sys_role_permission` VALUES ('28', '1', '28', '2020-04-22 15:48:47');
INSERT INTO `sys_role_permission` VALUES ('29', '1', '29', '2020-04-22 15:48:47');
INSERT INTO `sys_role_permission` VALUES ('3', '1', '3', '2020-04-22 15:48:47');
INSERT INTO `sys_role_permission` VALUES ('30', '1', '30', '2020-04-22 15:48:47');
INSERT INTO `sys_role_permission` VALUES ('31', '1', '31', '2020-05-26 17:04:21');
INSERT INTO `sys_role_permission` VALUES ('32', '1', '32', '2020-04-22 15:48:47');
INSERT INTO `sys_role_permission` VALUES ('33', '1', '33', '2020-04-22 15:48:47');
INSERT INTO `sys_role_permission` VALUES ('34', '1', '34', '2020-04-22 15:48:47');
INSERT INTO `sys_role_permission` VALUES ('35', '1', '35', '2020-05-26 17:04:21');
INSERT INTO `sys_role_permission` VALUES ('36', '1', '36', '2020-04-22 15:48:47');
INSERT INTO `sys_role_permission` VALUES ('38', '1', '38', '2020-04-22 15:48:47');
INSERT INTO `sys_role_permission` VALUES ('39', '1', '39', '2020-04-22 15:48:47');
INSERT INTO `sys_role_permission` VALUES ('4', '1', '4', '2020-04-22 15:48:47');
INSERT INTO `sys_role_permission` VALUES ('40', '1', '40', '2020-06-15 15:21:17');
INSERT INTO `sys_role_permission` VALUES ('41', '1', '41', '2020-06-15 15:21:17');
INSERT INTO `sys_role_permission` VALUES ('42', '1', '42', '2020-06-15 15:21:17');
INSERT INTO `sys_role_permission` VALUES ('43', '1', '43', '2020-06-15 15:21:17');
INSERT INTO `sys_role_permission` VALUES ('44', '1', '44', '2020-04-22 15:48:47');
INSERT INTO `sys_role_permission` VALUES ('45', '1', '45', '2020-04-22 15:48:47');
INSERT INTO `sys_role_permission` VALUES ('46', '1', '46', '2020-04-22 15:48:47');
INSERT INTO `sys_role_permission` VALUES ('47', '1', '47', '2020-04-22 15:48:47');
INSERT INTO `sys_role_permission` VALUES ('48', '1', '48', '2020-04-22 15:48:47');
INSERT INTO `sys_role_permission` VALUES ('49', '1', '49', '2020-04-22 15:48:47');
INSERT INTO `sys_role_permission` VALUES ('5', '1', '5', '2020-04-22 15:48:47');
INSERT INTO `sys_role_permission` VALUES ('50', '1', '50', '2020-04-22 15:48:47');
INSERT INTO `sys_role_permission` VALUES ('51', '1', '51', '2020-04-22 15:48:47');
INSERT INTO `sys_role_permission` VALUES ('52', '1', '52', '2020-04-22 15:48:47');
INSERT INTO `sys_role_permission` VALUES ('53', '1', '53', '2020-04-22 15:48:47');
INSERT INTO `sys_role_permission` VALUES ('54', '1', '54', '2020-04-22 15:48:47');
INSERT INTO `sys_role_permission` VALUES ('55', '1', '55', '2020-04-22 15:48:47');
INSERT INTO `sys_role_permission` VALUES ('56', '1', '56', '2020-05-26 17:04:21');
INSERT INTO `sys_role_permission` VALUES ('57', '1', '57', '2020-04-22 15:48:47');
INSERT INTO `sys_role_permission` VALUES ('58', '1', '58', '2020-04-22 15:48:47');
INSERT INTO `sys_role_permission` VALUES ('59', '1', '59', '2020-04-22 15:48:47');
INSERT INTO `sys_role_permission` VALUES ('6', '1', '6', '2020-04-22 15:48:47');
INSERT INTO `sys_role_permission` VALUES ('60', '1', '60', '2020-04-22 15:48:47');
INSERT INTO `sys_role_permission` VALUES ('61', '1', '61', '2020-05-26 14:21:56');
INSERT INTO `sys_role_permission` VALUES ('62', '1', '62', '2020-04-22 15:48:47');
INSERT INTO `sys_role_permission` VALUES ('63', '1', '63', '2020-04-22 15:48:47');
INSERT INTO `sys_role_permission` VALUES ('7', '1', '7', '2020-04-22 15:48:47');
INSERT INTO `sys_role_permission` VALUES ('8', '1', '8', '2020-04-22 15:48:47');
INSERT INTO `sys_role_permission` VALUES ('9', '1', '9', '2020-04-22 15:48:47');

-- ----------------------------
-- Table structure for `sys_user`
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `id` varchar(64) NOT NULL COMMENT '用户id',
  `username` varchar(50) NOT NULL COMMENT '账户名称',
  `salt` varchar(20) DEFAULT NULL COMMENT '加密盐值',
  `password` varchar(200) NOT NULL COMMENT '用户密码密文',
  `phone` varchar(20) DEFAULT NULL COMMENT '手机号码',
  `dept_id` varchar(64) DEFAULT NULL COMMENT '部门id',
  `real_name` varchar(60) DEFAULT NULL COMMENT '真实名称',
  `nick_name` varchar(60) DEFAULT NULL COMMENT '昵称',
  `email` varchar(50) DEFAULT NULL COMMENT '邮箱(唯一)',
  `status` tinyint DEFAULT NULL COMMENT '账户状态(1.正常 0.锁定 )',
  `sex` tinyint DEFAULT NULL COMMENT '性别(1.男 2.女)',
  `deleted` tinyint DEFAULT NULL COMMENT '是否删除(1未删除；0已删除)',
  `create_id` varchar(64) DEFAULT NULL COMMENT '创建人',
  `update_id` varchar(64) DEFAULT NULL COMMENT '更新人',
  `create_where` tinyint DEFAULT NULL COMMENT '创建来源(1.web 2.android 3.ios )',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='系统用户';

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('1', 'admin', '324ce32d86224b00a02b', '2102b59a75ab87616b62d0b9432569d0', '13888888888', '1', '爱糖宝', '爱糖宝', 'xxxxxx@163.com', '1', '2', '1', '1', '1', '3', '2019-09-22 19:38:05', '2020-03-18 09:15:22');
INSERT INTO `sys_user` VALUES ('1768989691405148161', 'tian', 'f42fd8c183224644aeec', '4cf00bebb18f1366754d7c1bf1ce47a0', '18257160372', '1', null, null, null, '1', null, '1', null, null, '1', '2024-03-16 21:16:23', '2024-03-16 21:16:23');

-- ----------------------------
-- Table structure for `sys_user_role`
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `id` varchar(64) NOT NULL COMMENT '用户id',
  `user_id` varchar(64) DEFAULT NULL,
  `role_id` varchar(64) DEFAULT NULL COMMENT '角色id',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='系统用户角色';

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES ('1', '1', '1', '2020-03-19 02:23:13');
INSERT INTO `sys_user_role` VALUES ('1768990723577225217', '1768989691405148161', '1768990677943197698', '2024-03-16 21:20:29');
