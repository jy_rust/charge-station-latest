/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 80200
Source Host           : localhost:3306
Source Database       : charge-pay

Target Server Type    : MYSQL
Target Server Version : 80200
File Encoding         : 65001

Date: 2024-03-28 15:51:19
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `charge_pay_order`
-- ----------------------------
DROP TABLE IF EXISTS `charge_pay_order`;
CREATE TABLE `charge_pay_order` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `user_id` bigint NOT NULL,
  `order_no` varchar(32) NOT NULL,
  `order_type` int NOT NULL,
  `charge_record_id` bigint DEFAULT NULL,
  `amount` decimal(10,2) NOT NULL,
  `pay_order_no` varchar(64) DEFAULT NULL,
  `pay_status` int NOT NULL,
  `update_time` datetime NOT NULL,
  `create_time` datetime NOT NULL,
  `user_coupon_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of charge_pay_order
-- ----------------------------

-- ----------------------------
-- Table structure for `retry_message`
-- ----------------------------
DROP TABLE IF EXISTS `retry_message`;
CREATE TABLE `retry_message` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `type` int DEFAULT NULL,
  `content` text,
  `retried_times` int DEFAULT NULL,
  `retry` int NOT NULL DEFAULT '1',
  `create_time` datetime DEFAULT NULL,
  `status` int DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of retry_message
-- ----------------------------
INSERT INTO `retry_message` VALUES ('1', '1', '{\"userId\":1,\"point\":5', '0', '1', '2024-03-24 14:35:17', '0');
INSERT INTO `retry_message` VALUES ('2', '0', '{\"point\":9,\"type\":0,\"userId\":1}', '0', '5', '2024-03-24 22:13:37', '1');
INSERT INTO `retry_message` VALUES ('3', '0', '{\"point\":9,\"type\":0,\"userId\":1}', '0', '5', '2024-03-24 22:15:13', '1');
INSERT INTO `retry_message` VALUES ('4', '0', '{\"point\":9,\"type\":0,\"userId\":1}', '0', '5', '2024-03-24 22:27:18', '1');
INSERT INTO `retry_message` VALUES ('5', '0', '{\"point\":9,\"type\":0,\"userId\":1}', '0', '5', '2024-03-24 22:52:58', '1');
INSERT INTO `retry_message` VALUES ('6', '0', '{\"point\":9,\"reqId\":\"USER_POINT52a0e5ba-59dd-4155-8481-ef414244bc0820240325093006999\",\"type\":0,\"userId\":1}', '0', '5', '2024-03-25 09:30:07', '1');
INSERT INTO `retry_message` VALUES ('7', '0', '{\"point\":9,\"reqId\":\"USER_POINTbfa6eb65-45ca-4f7c-becf-05003f3a76bb20240325110240646\",\"type\":0,\"userId\":1}', '0', '5', '2024-03-25 11:02:41', '1');
INSERT INTO `retry_message` VALUES ('8', '0', '{\"point\":9,\"reqId\":\"USER_POINTe59356ba-0d87-4a1c-83ca-c0b3a1c6934720240325110813817\",\"type\":0,\"userId\":1}', '0', '5', '2024-03-25 11:08:14', '1');
INSERT INTO `retry_message` VALUES ('9', '0', '{\"point\":9,\"reqId\":\"USER_POINTacf3bdc2-c980-44db-985b-626f27327c7220240325110815229\",\"type\":0,\"userId\":1}', '0', '5', '2024-03-25 11:08:15', '1');
INSERT INTO `retry_message` VALUES ('10', '0', '{\"point\":9,\"reqId\":\"USER_POINT7fb25b3f-1703-4440-a2e0-53b3783c411120240325110815890\",\"type\":0,\"userId\":1}', '0', '5', '2024-03-25 11:08:16', '1');
INSERT INTO `retry_message` VALUES ('11', '0', '{\"point\":9,\"reqId\":\"USER_POINT274b93eb-34a7-4662-80cf-9c3794ecdb5a20240325110816384\",\"type\":0,\"userId\":1}', '0', '5', '2024-03-25 11:08:16', '1');
INSERT INTO `retry_message` VALUES ('12', '0', '{\"point\":9,\"reqId\":\"USER_POINTe3e75f69-9ba7-4dc0-ba3b-619db540bc0c20240325110816885\",\"type\":0,\"userId\":1}', '0', '5', '2024-03-25 11:08:17', '1');
INSERT INTO `retry_message` VALUES ('13', '0', '{\"point\":9,\"reqId\":\"USER_POINT4f51f1db-ccb2-4374-a997-a97a0776cbd420240325111129484\",\"type\":0,\"userId\":1}', '0', '5', '2024-03-25 11:11:30', '1');
INSERT INTO `retry_message` VALUES ('14', '0', '{\"point\":9,\"reqId\":\"USER_POINT374030cb-de1b-4ed1-805b-79227c735bd620240325111131663\",\"type\":0,\"userId\":1}', '0', '5', '2024-03-25 11:11:32', '1');
INSERT INTO `retry_message` VALUES ('15', '0', '{\"point\":9,\"reqId\":\"USER_POINT788ade08-3d04-4a7e-ba63-11c3c00fa59e20240325111132497\",\"type\":0,\"userId\":1}', '0', '5', '2024-03-25 11:11:32', '1');
INSERT INTO `retry_message` VALUES ('16', '0', '{\"point\":9,\"reqId\":\"USER_POINT113c9dd4-d181-4d0b-a511-4d547930359a20240325111132970\",\"type\":0,\"userId\":1}', '0', '5', '2024-03-25 11:11:33', '1');
INSERT INTO `retry_message` VALUES ('17', '0', '{\"point\":9,\"reqId\":\"USER_POINTf69df843-a878-4b98-bc9b-3eeca478a2ce20240325111133318\",\"type\":0,\"userId\":1}', '0', '5', '2024-03-25 11:11:33', '1');
INSERT INTO `retry_message` VALUES ('18', '0', '{\"point\":9,\"reqId\":\"USER_POINTf80270ec-f59a-4302-82e8-ff47bd79e4f720240325111133581\",\"type\":0,\"userId\":1}', '0', '5', '2024-03-25 11:11:34', '1');
INSERT INTO `retry_message` VALUES ('19', '0', '{\"point\":9,\"reqId\":\"USER_POINTb0742ba8-57a6-4ab5-ae77-54f017dbad3520240325111133813\",\"type\":0,\"userId\":1}', '0', '5', '2024-03-25 11:11:34', '1');

-- ----------------------------
-- Table structure for `undo_log`
-- ----------------------------
DROP TABLE IF EXISTS `undo_log`;
CREATE TABLE `undo_log` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `branch_id` bigint NOT NULL COMMENT '分支事务ID',
  `xid` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '全局事务ID',
  `context` varchar(128) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '上下文',
  `rollback_info` longblob NOT NULL COMMENT '回滚信息',
  `log_status` int NOT NULL COMMENT '状态，0正常，1全局已完成',
  `log_created` datetime NOT NULL COMMENT '创建时间',
  `log_modified` datetime NOT NULL COMMENT '修改时间',
  `ext` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '扩展信息',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_undo_log` (`xid`,`branch_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb3 COMMENT='AT transaction mode undo table';

-- ----------------------------
-- Records of undo_log
-- ----------------------------
