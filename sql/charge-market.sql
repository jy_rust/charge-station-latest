/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 80200
Source Host           : localhost:3306
Source Database       : charge-market

Target Server Type    : MYSQL
Target Server Version : 80200
File Encoding         : 65001

Date: 2024-03-28 15:51:05
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `charge_coupon`
-- ----------------------------
DROP TABLE IF EXISTS `charge_coupon`;
CREATE TABLE `charge_coupon` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `condition_id` int NOT NULL,
  `use_type` int NOT NULL,
  `overlay` int NOT NULL,
  `period` int NOT NULL,
  `times` int NOT NULL,
  `period_unit` int NOT NULL,
  `status` int NOT NULL,
  `point` int DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of charge_coupon
-- ----------------------------
INSERT INTO `charge_coupon` VALUES ('1', '测试优惠券', '1', '1', '0', '0', '100', '1', '1', '10');

-- ----------------------------
-- Table structure for `charge_coupon_condition`
-- ----------------------------
DROP TABLE IF EXISTS `charge_coupon_condition`;
CREATE TABLE `charge_coupon_condition` (
  `id` int NOT NULL AUTO_INCREMENT,
  `condition` int NOT NULL,
  `face_value` decimal(10,0) NOT NULL,
  `type` int NOT NULL,
  `create_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of charge_coupon_condition
-- ----------------------------

-- ----------------------------
-- Table structure for `charge_exceptional_retroaction`
-- ----------------------------
DROP TABLE IF EXISTS `charge_exceptional_retroaction`;
CREATE TABLE `charge_exceptional_retroaction` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `exceptional_type` int NOT NULL,
  `user_id` bigint NOT NULL,
  `order_no` varchar(255) DEFAULT NULL,
  `device_no` varchar(255) DEFAULT NULL,
  `exception_message` varchar(255) DEFAULT NULL,
  `status` int NOT NULL DEFAULT '0',
  `url1` varchar(255) DEFAULT NULL,
  `url2` varchar(255) DEFAULT NULL,
  `url3` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of charge_exceptional_retroaction
-- ----------------------------

-- ----------------------------
-- Table structure for `charge_global_property`
-- ----------------------------
DROP TABLE IF EXISTS `charge_global_property`;
CREATE TABLE `charge_global_property` (
  `id` int NOT NULL AUTO_INCREMENT,
  `busi_type` int DEFAULT NULL COMMENT '业务类型',
  `busi_default` int DEFAULT NULL,
  `busi_desc` varchar(255) DEFAULT NULL,
  `status` int NOT NULL DEFAULT '0',
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of charge_global_property
-- ----------------------------
INSERT INTO `charge_global_property` VALUES ('1', '1', '10', '注册收益', '0', '2024-01-28 11:25:33');

-- ----------------------------
-- Table structure for `charge_message_template`
-- ----------------------------
DROP TABLE IF EXISTS `charge_message_template`;
CREATE TABLE `charge_message_template` (
  `id` int NOT NULL AUTO_INCREMENT,
  `type` int NOT NULL,
  `content` text NOT NULL,
  `params` text,
  `status` int NOT NULL DEFAULT '0',
  `create_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of charge_message_template
-- ----------------------------

-- ----------------------------
-- Table structure for `charge_user_coupon`
-- ----------------------------
DROP TABLE IF EXISTS `charge_user_coupon`;
CREATE TABLE `charge_user_coupon` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `user_id` bigint NOT NULL,
  `coupon_id` int NOT NULL,
  `period` datetime NOT NULL,
  `status` int NOT NULL,
  `use_time` datetime DEFAULT NULL,
  `create_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of charge_user_coupon
-- ----------------------------
INSERT INTO `charge_user_coupon` VALUES ('1', '1', '1', '2024-03-15 16:37:24', '0', null, '2024-03-15 16:37:26');
INSERT INTO `charge_user_coupon` VALUES ('5', '1', '1', '2024-03-15 16:50:44', '0', null, '2024-03-15 16:50:44');
INSERT INTO `charge_user_coupon` VALUES ('6', '1', '1', '2024-03-15 16:51:20', '0', null, '2024-03-15 16:51:20');
INSERT INTO `charge_user_coupon` VALUES ('7', '1', '1', '2024-03-15 16:51:21', '0', null, '2024-03-15 16:51:21');
INSERT INTO `charge_user_coupon` VALUES ('8', '1', '1', '2024-03-15 16:51:22', '0', null, '2024-03-15 16:51:22');
INSERT INTO `charge_user_coupon` VALUES ('9', '1', '1', '2024-03-15 16:51:23', '0', null, '2024-03-15 16:51:23');
INSERT INTO `charge_user_coupon` VALUES ('10', '1', '1', '2024-03-15 16:51:24', '0', null, '2024-03-15 16:51:24');
INSERT INTO `charge_user_coupon` VALUES ('11', '1', '1', '2024-03-15 16:51:25', '0', null, '2024-03-15 16:51:25');
INSERT INTO `charge_user_coupon` VALUES ('12', '1', '1', '2024-03-15 16:51:26', '0', null, '2024-03-15 16:51:26');
INSERT INTO `charge_user_coupon` VALUES ('13', '1', '1', '2024-03-15 16:51:50', '0', null, '2024-03-15 16:51:50');
INSERT INTO `charge_user_coupon` VALUES ('14', '1', '1', '2024-03-15 16:51:53', '0', null, '2024-03-15 16:51:53');
INSERT INTO `charge_user_coupon` VALUES ('15', '1', '1', '2024-03-15 18:48:44', '0', null, '2024-03-15 18:48:44');

-- ----------------------------
-- Table structure for `undo_log`
-- ----------------------------
DROP TABLE IF EXISTS `undo_log`;
CREATE TABLE `undo_log` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `branch_id` bigint NOT NULL COMMENT '分支事务ID',
  `xid` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '全局事务ID',
  `context` varchar(128) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '上下文',
  `rollback_info` longblob NOT NULL COMMENT '回滚信息',
  `log_status` int NOT NULL COMMENT '状态，0正常，1全局已完成',
  `log_created` datetime NOT NULL COMMENT '创建时间',
  `log_modified` datetime NOT NULL COMMENT '修改时间',
  `ext` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '扩展信息',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_undo_log` (`xid`,`branch_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb3 COMMENT='AT transaction mode undo table';

-- ----------------------------
-- Records of undo_log
-- ----------------------------
