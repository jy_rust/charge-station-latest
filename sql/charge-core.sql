/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 80200
Source Host           : localhost:3306
Source Database       : charge-core

Target Server Type    : MYSQL
Target Server Version : 80200
File Encoding         : 65001

Date: 2024-03-28 15:50:58
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `charge_gun_monitor`
-- ----------------------------
DROP TABLE IF EXISTS `charge_gun_monitor`;
CREATE TABLE `charge_gun_monitor` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `gun_id` int NOT NULL,
  `status` int NOT NULL,
  `create_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of charge_gun_monitor
-- ----------------------------

-- ----------------------------
-- Table structure for `charge_record`
-- ----------------------------
DROP TABLE IF EXISTS `charge_record`;
CREATE TABLE `charge_record` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `user_id` bigint NOT NULL,
  `station_gun_id` int NOT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime DEFAULT NULL,
  `quantity` int DEFAULT NULL,
  `price` decimal(10,2) NOT NULL,
  `charge_status` int NOT NULL,
  `pay_status` int NOT NULL,
  `amount` decimal(10,2) DEFAULT NULL,
  `pay_time` datetime DEFAULT NULL,
  `create_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of charge_record
-- ----------------------------
INSERT INTO `charge_record` VALUES ('1', '1', '1', '2024-03-23 13:04:56', '2024-03-23 16:05:04', '1', '10.00', '1', '1', '9.00', '2024-03-23 16:05:19', '2024-03-23 15:05:22');

-- ----------------------------
-- Table structure for `charge_station_dedicated`
-- ----------------------------
DROP TABLE IF EXISTS `charge_station_dedicated`;
CREATE TABLE `charge_station_dedicated` (
  `id` int NOT NULL AUTO_INCREMENT,
  `dedicated_name` varchar(255) NOT NULL,
  `status` int NOT NULL,
  `create_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of charge_station_dedicated
-- ----------------------------

-- ----------------------------
-- Table structure for `charge_station_gun`
-- ----------------------------
DROP TABLE IF EXISTS `charge_station_gun`;
CREATE TABLE `charge_station_gun` (
  `id` int NOT NULL AUTO_INCREMENT,
  `gun_name` varchar(255) NOT NULL,
  `gun_no` varchar(255) NOT NULL,
  `station_id` int NOT NULL,
  `gun_type` int NOT NULL,
  `gun_power` int NOT NULL,
  `status` int NOT NULL,
  `create_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of charge_station_gun
-- ----------------------------

-- ----------------------------
-- Table structure for `charge_station_info`
-- ----------------------------
DROP TABLE IF EXISTS `charge_station_info`;
CREATE TABLE `charge_station_info` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` bigint DEFAULT NULL,
  `station_name` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `dedicated_type` int NOT NULL,
  `charging_speed_type` int NOT NULL,
  `original_price` decimal(10,0) NOT NULL,
  `price` decimal(10,0) NOT NULL,
  `park_fee` int NOT NULL,
  `lounge` int NOT NULL,
  `toilet` int NOT NULL,
  `shopping_mall` int NOT NULL,
  `wash_car` int NOT NULL,
  `wash_fee` decimal(10,0) NOT NULL,
  `business_start_time` time NOT NULL,
  `business_end_time` time NOT NULL,
  `diet` int NOT NULL,
  `longitude` decimal(16,8) NOT NULL,
  `latitude` decimal(16,8) NOT NULL,
  `status` int NOT NULL,
  `create_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of charge_station_info
-- ----------------------------
INSERT INTO `charge_station_info` VALUES ('1', '1', '光光充电站', '广州市白云区1号', '1', '1', '10', '5', '1', '1', '1', '1', '1', '1', '00:00:01', '00:00:01', '1', '1.00000000', '1.00000000', '0', '2024-03-15 10:25:52');
INSERT INTO `charge_station_info` VALUES ('6', null, '广州市', '白云区1号', '1', '1', '111', '11', '1', '1', '1', '1', '1', '11', '19:17:21', '19:17:21', '1', '1111.00000000', '1111.00000000', '0', '2024-03-17 19:17:21');
INSERT INTO `charge_station_info` VALUES ('7', null, '222', '222222222', '1', '1', '111', '1', '1', '1', '1', '1', '1', '1', '23:39:39', '23:39:39', '1', '222.00000000', '222.00000000', '0', '2024-03-17 23:39:39');
