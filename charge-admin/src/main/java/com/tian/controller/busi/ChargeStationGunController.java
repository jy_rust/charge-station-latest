package com.tian.controller.busi;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.stereotype.Controller;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import java.util.List;
import com.tian.common.utils.DataResult;

import com.tian.entity.ChargeStationGunEntity;
import com.tian.service.ChargeStationGunService;



/**
 * 充电枪信息
 *
 * @author tian
 * @email *****@mail.com
 * @date 2024-03-16 21:49:34
 */
@Controller
@RequestMapping("/")
public class ChargeStationGunController {
    @Autowired
    private ChargeStationGunService chargeStationGunService;


    /**
    * 跳转到页面
    */
    @GetMapping("/index/chargeStationGun")
    public String chargeStationGun() {
        return "chargestationgun/list";
        }

    @ApiOperation(value = "新增")
    @PostMapping("chargeStationGun/add")
    @RequiresPermissions("chargeStationGun:add")
    @ResponseBody
    public DataResult add(@RequestBody ChargeStationGunEntity chargeStationGun){
        chargeStationGunService.save(chargeStationGun);
        return DataResult.success();
    }

    @ApiOperation(value = "删除")
    @DeleteMapping("chargeStationGun/delete")
    @RequiresPermissions("chargeStationGun:delete")
    @ResponseBody
    public DataResult delete(@RequestBody @ApiParam(value = "id集合") List<String> ids){
        chargeStationGunService.removeByIds(ids);
        return DataResult.success();
    }

    @ApiOperation(value = "更新")
    @PutMapping("chargeStationGun/update")
    @RequiresPermissions("chargeStationGun:update")
    @ResponseBody
    public DataResult update(@RequestBody ChargeStationGunEntity chargeStationGun){
        chargeStationGunService.updateById(chargeStationGun);
        return DataResult.success();
    }

    @ApiOperation(value = "查询分页数据")
    @PostMapping("chargeStationGun/listByPage")
    @RequiresPermissions("chargeStationGun:list")
    @ResponseBody
    public DataResult findListByPage(@RequestBody ChargeStationGunEntity chargeStationGun){
        LambdaQueryWrapper<ChargeStationGunEntity> queryWrapper = Wrappers.lambdaQuery();
        //查询条件示例
        queryWrapper.eq(ChargeStationGunEntity::getId, chargeStationGun.getId());
        queryWrapper.orderByDesc(ChargeStationGunEntity::getId);
        IPage<ChargeStationGunEntity> iPage = chargeStationGunService.page(chargeStationGun.getQueryPage(), queryWrapper);
        return DataResult.success(iPage);
    }

}
