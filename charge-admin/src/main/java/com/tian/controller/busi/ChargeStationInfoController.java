package com.tian.controller.busi;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.stereotype.Controller;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

import java.util.List;

import com.tian.common.utils.DataResult;

import com.tian.entity.ChargeStationInfoEntity;
import com.tian.service.ChargeStationInfoService;

import javax.annotation.Resource;


/**
 * @author tian
 * @email *****@mail.com
 * @date 2024-03-16 14:54:57
 */
@Controller
@RequestMapping("/")
public class ChargeStationInfoController {
    @Resource
    private ChargeStationInfoService chargeStationInfoService;


    /**
     * 跳转到页面
     */
    @GetMapping("/index/chargeStationInfo")
    public String chargeStationInfo() {
        return "chargestationinfo/list";
    }

    @ApiOperation(value = "新增")
    @PostMapping("chargeStationInfo/add")
    @RequiresPermissions("chargeStationInfo:add")
    @ResponseBody
    public DataResult add(@RequestBody ChargeStationInfoEntity chargeStationInfo) {
        return chargeStationInfoService.add(chargeStationInfo);
    }

    @ApiOperation(value = "删除")
    @DeleteMapping("chargeStationInfo/delete")
    @RequiresPermissions("chargeStationInfo:delete")
    @ResponseBody
    public DataResult delete(@RequestBody @ApiParam(value = "id集合") List<String> ids) {
        chargeStationInfoService.removeByIds(ids);
        return DataResult.success();
    }

    @ApiOperation(value = "更新")
    @PutMapping("chargeStationInfo/update")
    @RequiresPermissions("chargeStationInfo:update")
    @ResponseBody
    public DataResult update(@RequestBody ChargeStationInfoEntity chargeStationInfo) {
        chargeStationInfoService.updateById(chargeStationInfo);
        return DataResult.success();
    }

    @ApiOperation(value = "查询分页数据")
    @PostMapping("chargeStationInfo/listByPage")
    @RequiresPermissions("chargeStationInfo:list")
    @ResponseBody
    public DataResult findListByPage(@RequestBody ChargeStationInfoEntity chargeStationInfo) {
        return chargeStationInfoService.listByPage(chargeStationInfo);
    }
}
