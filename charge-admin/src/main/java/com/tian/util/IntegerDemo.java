package com.tian.util;

/**
 * {@code @description:} TODO
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-05-24 15:27
 * {@code @version:} 1.0
 */
public class IntegerDemo {
    public static void main(String[] args) {
        Integer a=100;
//        Integer a=Integer.valueOf(100)
        Integer b=100;
        //Integer b=Integer.valueOf(100)
        System.out.println(a==b);//true


        Integer c=200;
        //Integer c=Integer.valueOf(200)
        Integer d=200;
        //Integer d=Integer.valueOf(200)
        System.out.println(c==d);//false
        //== 封装类型，运算符比较的是两个对象的地址值，如果两个对象的地址值一样，则认为两个对象是相同的。基本类型 数值
    }
}
