package com.tian.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.List;

/**
 * BaseEntity
 *
 * @author tian
 * @version V1.0
 * @date 2023年3月18日
 */
@Data
@JsonIgnoreProperties(value = {"page", "limit"})
public class BaseEntity {
    @JSONField(serialize = false)
    @TableField(exist = false)
    private Integer page = 1;

    @JSONField(serialize = false)
    @TableField(exist = false)
    private Integer limit = 10;

    /**
     * 数据权限：用户id
     */
    @TableField(exist = false)
    private List<String> createIds;

    /**
     * page条件
     * @param <T>
     * @return
     */
    public <T> Page getQueryPage() {
        return new Page<T>(page, limit);
    }
}
