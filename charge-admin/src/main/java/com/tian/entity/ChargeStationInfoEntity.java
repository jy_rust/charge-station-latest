package com.tian.entity;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalTime;
import java.util.Date;

/**
 * @author tian
 * @email *****@mail.com
 * @date 2024-03-16 14:54:57
 */
@Data
public class ChargeStationInfoEntity extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    private Integer id;

    private Long userId;

    private String userName;

    private String stationName;

    private String address;

    private Integer dedicatedType;

    private Integer chargingSpeedType;

    private BigDecimal originalPrice;

    private BigDecimal price;


    private Integer parkFee;


    private Integer lounge;


    private Integer toilet;


    private Integer shoppingMall;


    private Integer washCar;


    private BigDecimal washFee;


    private LocalTime businessStartTime;


    private LocalTime businessEndTime;


    private Integer diet;


    private BigDecimal longitude;


    private BigDecimal latitude;


    private Integer status;


    private Date createTime;


}
