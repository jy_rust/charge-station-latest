package com.tian.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;
import com.tian.entity.BaseEntity;


import java.io.Serializable;
import java.util.Date;

import lombok.Data;

/**
 * 充电枪信息
 *
 * @author tian
 * @email *****@mail.com
 * @date 2024-03-16 21:49:34
 */
@Data
@TableName("charge_station_gun")
public class ChargeStationGunEntity extends BaseEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	@TableId("id")
	private Integer id;

	/**
	 * 终端名称
	 */
	@TableField("gun_name")
	private String gunName;

	/**
	 * 终端编号
	 */
	@TableField("gun_no")
	private String gunNo;

	/**
	 * 充电站id
	 */
	@TableField("station_id")
	private Integer stationId;

	/**
	 * 终端类型
	 */
	@TableField("gun_type")
	private Integer gunType;

	/**
	 * 终端功率
	 */
	@TableField("gun_power")
	private Integer gunPower;

	/**
	 * 状态
	 */
	@TableField("status")
	private Integer status;

	/**
	 * 录入时间
	 */
	@TableField("create_time")
	private Date createTime;


}
