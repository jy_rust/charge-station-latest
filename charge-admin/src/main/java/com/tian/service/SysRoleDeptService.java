package com.tian.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.tian.entity.SysRoleDeptEntity;

/**
 * 角色部门
 *
 * @author tian
 * @email *****@mail.com
 * @date 2020-09-27 17:30:15
 */
public interface SysRoleDeptService extends IService<SysRoleDeptEntity> {

}

