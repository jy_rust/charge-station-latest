package com.tian.service.impl;

import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.tian.mapper.ChargeStationGunMapper;
import com.tian.entity.ChargeStationGunEntity;
import com.tian.service.ChargeStationGunService;


@Service("chargeStationGunService")
public class ChargeStationGunServiceImpl extends ServiceImpl<ChargeStationGunMapper, ChargeStationGunEntity> implements ChargeStationGunService {


}