package com.tian.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tian.entity.SysLog;
import com.tian.mapper.SysLogMapper;
import com.tian.service.LogService;
import org.springframework.stereotype.Service;

/**
 * 系统日志
 *
 * @author tian
 * @version V1.0
 * @date 2023年3月18日
 */
@Service
public class LogServiceImpl extends ServiceImpl<SysLogMapper, SysLog> implements LogService {
}
