package com.tian.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tian.entity.SysContentEntity;
import com.tian.mapper.SysContentMapper;
import com.tian.service.SysContentService;
import org.springframework.stereotype.Service;

/**
 * 内容 服务类
 *
 * @author tian
 * @version V1.0
 * @date 2023年3月18日
 */
@Service("sysContentService")
public class SysContentServiceImpl extends ServiceImpl<SysContentMapper, SysContentEntity> implements SysContentService {


}