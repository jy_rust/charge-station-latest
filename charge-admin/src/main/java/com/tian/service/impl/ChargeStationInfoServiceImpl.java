package com.tian.service.impl;
import java.math.BigDecimal;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tian.client.ChargeUserClient;
import com.tian.client.StationClient;
import com.tian.common.CommonResult;
import com.tian.common.utils.DataResult;
import com.tian.dto.*;
import com.tian.enums.ResultCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.tian.mapper.ChargeStationInfoMapper;
import com.tian.entity.ChargeStationInfoEntity;
import com.tian.service.ChargeStationInfoService;

import javax.annotation.Resource;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@Service
public class ChargeStationInfoServiceImpl extends ServiceImpl<ChargeStationInfoMapper, ChargeStationInfoEntity> implements ChargeStationInfoService {

    @Resource
    private StationClient stationClient;
    @Resource
    private ChargeUserClient chargeUserClient;

    @Override
    public DataResult add(ChargeStationInfoEntity chargeStationInfo) {
        StationDto stationDto = new StationDto();
        stationDto.setStationName(chargeStationInfo.getStationName());
        stationDto.setAddress(chargeStationInfo.getAddress());
        stationDto.setDedicatedType(chargeStationInfo.getDedicatedType());
        stationDto.setChargingSpeedType(chargeStationInfo.getChargingSpeedType());
        stationDto.setOriginalPrice(chargeStationInfo.getOriginalPrice());
        stationDto.setPrice(chargeStationInfo.getPrice());
        stationDto.setParkFee(chargeStationInfo.getParkFee());
        stationDto.setLounge(chargeStationInfo.getLounge());
        stationDto.setToilet(chargeStationInfo.getToilet());
        stationDto.setShoppingMall(chargeStationInfo.getShoppingMall());
        stationDto.setWashCar(chargeStationInfo.getWashCar());
        stationDto.setWashFee(chargeStationInfo.getWashFee());

        LocalTime date = LocalTime.now();

        stationDto.setBusinessStartTime(date);
        stationDto.setBusinessEndTime(date);

        stationDto.setDiet(chargeStationInfo.getDiet());
        stationDto.setLongitude(chargeStationInfo.getLongitude());
        stationDto.setLatitude(chargeStationInfo.getLatitude());
        stationDto.setStatus(chargeStationInfo.getStatus());
        stationDto.setCreateTime(new Date());
        CommonResult<Boolean> addResult = stationClient.add(stationDto);
        if (addResult.getCode() == ResultCode.SUCCESS.getCode()) {
            return DataResult.success();
        }
        return DataResult.fail(addResult.getMessage());
    }

    @Override
    public DataResult listByPage(ChargeStationInfoEntity chargeStationInfo) {
        long current = chargeStationInfo.getPage();
        long limit = chargeStationInfo.getLimit();

        ChargeStationQueryPageReqDto chargeStationQueryPageReqDto = new ChargeStationQueryPageReqDto();
        chargeStationQueryPageReqDto.setCurrentPage((int) current - 1);
        chargeStationQueryPageReqDto.setPageSize((int) limit);
        CommonResult<StationQueryPageRespDto> stationQueryPageRespDtoCommonResult = stationClient.listByPage(chargeStationQueryPageReqDto);
        StationQueryPageRespDto data = stationQueryPageRespDtoCommonResult.getData();

        List<ChargeStationInfoEntity> chargeStationInfoEntities = new ArrayList<>();
        List<StationRespDto> stationRespDtoList = data.getData();
        List<Long> userList = new ArrayList<>();
        for (StationRespDto stationRespDto : stationRespDtoList) {
            ChargeStationInfoEntity stationInfoEntity = new ChargeStationInfoEntity();
            BeanUtils.copyProperties(stationRespDto, stationInfoEntity);
            chargeStationInfoEntities.add(stationInfoEntity);
            userList.add(stationRespDto.getUserId());
        }
        ChargeUsersReqDto chargeUsersReqDto = new ChargeUsersReqDto();
        chargeUsersReqDto.setUserIdList(userList);
        CommonResult<ChargeUsersRespDto> chargeUsersRespDtoCommonResult = chargeUserClient.findUserByUserIdList(chargeUsersReqDto);
        if (chargeUsersRespDtoCommonResult.getCode() == ResultCode.SUCCESS.getCode()) {
            Map<Long, ChargeUserInfoDto> resultMap = chargeUsersRespDtoCommonResult.getData().getChargeUserInfoDtoList().stream()
                    .collect(Collectors.toMap(ChargeUserInfoDto::getId, dto -> dto));
            for (ChargeStationInfoEntity chargeStationInfoEntity : chargeStationInfoEntities) {
                ChargeUserInfoDto chargeUserInfoDto = resultMap.get(chargeStationInfoEntity.getUserId());
                chargeStationInfoEntity.setUserName(chargeUserInfoDto.getNickName());
            }
        }
        IPage<ChargeStationInfoEntity> iPage = new Page<>();
        iPage.setPages((data.getTotal() / data.getPageSize()) + 1);
        iPage.setCurrent(current);
        iPage.setRecords(chargeStationInfoEntities);
        iPage.setTotal(data.getTotal());
        iPage.setSize(data.getTotal());
        return DataResult.success(iPage);
    }

    @Override
    public DataResult update(ChargeStationInfoEntity chargeStationInfo) {
        StationDto stationDto=new StationDto();
        stationDto.setId(chargeStationInfo.getId());
        stationDto.setStationName(chargeStationInfo.getStationName());
        stationDto.setAddress(chargeStationInfo.getAddress());
        stationDto.setDedicatedType(chargeStationInfo.getDedicatedType());
        stationDto.setChargingSpeedType(chargeStationInfo.getChargingSpeedType());
        stationDto.setOriginalPrice(chargeStationInfo.getOriginalPrice());
        stationDto.setPrice(chargeStationInfo.getPrice());
        stationDto.setParkFee(chargeStationInfo.getParkFee());
        stationDto.setLounge(chargeStationInfo.getLounge());
        stationDto.setToilet(chargeStationInfo.getToilet());
        stationDto.setShoppingMall(chargeStationInfo.getShoppingMall());
        stationDto.setWashCar(chargeStationInfo.getWashCar());
        stationDto.setWashFee(chargeStationInfo.getWashFee());
        stationDto.setBusinessStartTime(chargeStationInfo.getBusinessStartTime());
        stationDto.setBusinessEndTime(chargeStationInfo.getBusinessEndTime());
        stationDto.setDiet(chargeStationInfo.getDiet());
        stationDto.setLongitude(chargeStationInfo.getLongitude());
        stationDto.setLatitude(chargeStationInfo.getLatitude());
        stationDto.setStatus(chargeStationInfo.getStatus());
        CommonResult<Boolean> updateResult = stationClient.update(stationDto);
        if (updateResult.getCode() == ResultCode.SUCCESS.getCode()) {
            return DataResult.success();
        }
        return DataResult.fail(updateResult.getMessage());
    }
}