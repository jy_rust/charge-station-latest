package com.tian.service;

import com.tian.vo.resp.HomeRespVO;

/**
 * 首页
 *
 * @author tian
 * @version V1.0
 * @date 2023年3月18日
 */
public interface HomeService {

    /**
     * 获取首页信息
     *
     * @param userId userId
     * @return HomeRespVO
     */
    HomeRespVO getHomeInfo(String userId);
}
