package com.tian.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.tian.entity.SysLog;

/**
 * 系统日志
 *
 * @author tian
 * @version V1.0
 * @date 2023年3月18日
 */
public interface LogService extends IService<SysLog> {
}
