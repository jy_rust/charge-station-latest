package com.tian.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.tian.entity.SysDictEntity;

/**
 * 数据字典 服务类
 *
 * @author tian
 * @version V1.0
 * @date 2023年3月18日
 */
public interface SysDictService extends IService<SysDictEntity> {

}

