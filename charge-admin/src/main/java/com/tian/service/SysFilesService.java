package com.tian.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.tian.common.utils.DataResult;
import com.tian.entity.SysFilesEntity;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * 文件上传 服务类
 *
 * @author tian
 * @version V1.0
 * @date 2023年3月18日
 */
public interface SysFilesService extends IService<SysFilesEntity> {

    DataResult saveFile(MultipartFile file);

    void removeByIdsAndFiles(List<String> ids);
}

