package com.tian.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.tian.entity.SysRolePermission;
import com.tian.vo.req.RolePermissionOperationReqVO;

/**
 * 角色权限关联
 *
 * @author tian
 * @version V1.0
 * @date 2023年3月18日
 */
public interface RolePermissionService extends IService<SysRolePermission> {

    /**
     * 角色绑定权限
     *
     * @param vo vo
     */
    void addRolePermission(RolePermissionOperationReqVO vo);
}
