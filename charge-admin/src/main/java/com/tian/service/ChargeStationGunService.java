package com.tian.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.tian.entity.ChargeStationGunEntity;

/**
 * 充电枪信息
 *
 * @author tian
 * @email *****@mail.com
 * @date 2024-03-16 21:49:34
 */
public interface ChargeStationGunService extends IService<ChargeStationGunEntity> {

}

