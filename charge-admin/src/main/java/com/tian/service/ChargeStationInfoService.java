package com.tian.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.tian.common.utils.DataResult;
import com.tian.entity.ChargeStationInfoEntity;

/**
 * 
 *
 * @author tian
 * @email *****@mail.com
 * @date 2024-03-16 14:54:57
 */
public interface ChargeStationInfoService extends IService<ChargeStationInfoEntity> {

    DataResult add(ChargeStationInfoEntity chargeStationInfo);

    DataResult listByPage(ChargeStationInfoEntity chargeStationInfo);
    DataResult update(ChargeStationInfoEntity chargeStationInfo);
}

