package com.tian.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tian.entity.SysRolePermission;

/**
 * 角色权限 Mapper
 *
 * @author tian
 * @version V1.0
 * @date 2023年3月18日
 */
public interface SysRolePermissionMapper extends BaseMapper<SysRolePermission> {

}