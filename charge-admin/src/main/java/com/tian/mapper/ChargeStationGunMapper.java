package com.tian.mapper;

import com.tian.entity.ChargeStationGunEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 充电枪信息
 * 
 * @author tian
 * @email *****@mail.com
 * @date 2024-03-16 21:49:34
 */
public interface ChargeStationGunMapper extends BaseMapper<ChargeStationGunEntity> {
	
}
