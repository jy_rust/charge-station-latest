package com.tian.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tian.entity.SysFilesEntity;

/**
 * 文件上传 Mapper
 *
 * @author tian
 * @version V1.0
 * @date 2023年3月18日
 */
public interface SysFilesMapper extends BaseMapper<SysFilesEntity> {

}
