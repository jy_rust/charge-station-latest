package com.tian.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tian.entity.SysRoleDeptEntity;

/**
 * 角色部门
 *
 * @author tian
 * @email *****@mail.com
 * @date 2020-09-27 17:30:15
 */
public interface SysRoleDeptMapper extends BaseMapper<SysRoleDeptEntity> {

}
