package com.tian.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tian.entity.SysJobEntity;

/**
 * 定时任务 Mapper
 *
 * @author tian
 * @version V1.0
 * @date 2023年3月18日
 */
public interface SysJobMapper extends BaseMapper<SysJobEntity> {

}
