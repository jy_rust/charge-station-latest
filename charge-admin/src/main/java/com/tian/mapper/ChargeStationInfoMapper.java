package com.tian.mapper;

import com.tian.entity.ChargeStationInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author tian
 * @email *****@mail.com
 * @date 2024-03-16 14:54:57
 */
public interface ChargeStationInfoMapper extends BaseMapper<ChargeStationInfoEntity> {
	
}
