package com.tian.controller;//package com.xxl.job.executor.mvc.controller;

import com.tian.client.ChargeUserClient;
import com.tian.common.CommonResult;
import com.tian.dto.ChargeUserDto;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RBucket;
import org.redisson.api.RedissonClient;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

/**
 * {@code @description:} demo
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024/1/18 15:19
 * {@code @version:} 1.0
 */
@Slf4j
@Controller
@EnableAutoConfiguration
public class IndexController {

    @Resource
    private RedissonClient redissonClient;
    @Resource
    private ChargeUserClient chargeUserClient;

    @RequestMapping("/")
    @ResponseBody
    String index() {
      /*  RBucket<String> bucket = redissonClient.getBucket("my_name");
        bucket.set("tian");
        log.info(bucket.get());*/
        CommonResult<ChargeUserDto> chargeUserDtoCommonResult = chargeUserClient.queryById();
        System.out.println(chargeUserDtoCommonResult);
        return "xxl job executor running.";
    }

}