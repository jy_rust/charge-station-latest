package com.tian.util;

/**
 * {@code @description:} TODO
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024/1/20 20:40
 * {@code @version:} 1.0
 */
public interface RocketMQConstant {
    String TOP_NAME="charge-station";
    String TOPIC_INCOME="topic-income";
    String TOPIC_INVITED_RECORD="topic-invited-record";
}
