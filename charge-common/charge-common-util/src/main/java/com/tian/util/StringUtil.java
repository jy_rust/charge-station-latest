package com.tian.util;

import java.util.Random;

/**
 * {@code @description:} 字符串相关处理工具类
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024/1/18 19:00
 * {@code @version:} 1.0
 */
public class StringUtil {
    public static String getStringRandom6() {
        Random random = new Random();
        //把随机生成的数字转成字符串
        StringBuilder code = new StringBuilder("");
        for (int i = 0; i < 5; i++) {
            code.append(random.nextInt(9));
        }
        return code.toString();
    }

    public static String getStringRandom4() {
        Random random = new Random();
        //把随机生成的数字转成字符串
        StringBuilder code = new StringBuilder("");
        for (int i = 0; i < 5; i++) {
            code.append(random.nextInt(9));
        }
        return code.toString();
    }

    public static String getStringRandom(int num) {
        Random random = new Random();
        //把随机生成的数字转成字符串
        StringBuilder code = new StringBuilder("");
        for (int i = 0; i < num - 1; i++) {
            code.append(random.nextInt(9));
        }
        return code.toString();
    }

    public static boolean isEmpty(String str) {
        return str == null || str.length() == 0;
    }

    public static boolean isBlank(String str) {
        int strLen;
        if (str != null && (strLen = str.length()) != 0) {
            for (int i = 0; i < strLen; ++i) {
                if (!Character.isWhitespace(str.charAt(i))) {
                    return false;
                }
            }

            return true;
        } else {
            return true;
        }
    }

    public static String join(String[] strs, String s) {
        StringBuilder stringBuilder = new StringBuilder();
        for (String str : strs) {
            stringBuilder.append(str).append(s);
        }
        return stringBuilder.toString();
    }

    public static void main(String[] args) {

    }
}
