package com.tian.exception;

import com.tian.enums.ResultCode;

/**
 * {@code @description:} 验证异常
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-04-07 23:07
 * {@code @version:} 1.0
 */
public class ValidException extends BusinessException {

    public ValidException(String message, Integer code, Object object) {
        super(message, code, object);
    }

    public ValidException(ResultCode resultCode) {
        super(resultCode);
    }

    public ValidException(ResultCode resultCode, String message) {
        super(resultCode, message);
    }

    public ValidException(String message) {
        super(message);
    }
}
