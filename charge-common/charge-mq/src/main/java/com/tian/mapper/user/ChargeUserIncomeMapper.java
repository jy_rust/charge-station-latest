package com.tian.mapper.user;

import com.tian.entity.user.ChargeUserIncome;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ChargeUserIncomeMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ChargeUserIncome record);

    ChargeUserIncome selectByPrimaryKey(Integer id);

    List<ChargeUserIncome> selectAll();

    int updateByPrimaryKey(ChargeUserIncome record);
}