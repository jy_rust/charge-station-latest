package com.tian.mapper.market;

import com.tian.entity.market.ChargeGlobalProperty;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ChargeGlobalPropertyMapper {
    int deleteByPrimaryKey(Integer Long);

    int insert(ChargeGlobalProperty record);

    ChargeGlobalProperty selectByPrimaryKey(Long id);

    List<ChargeGlobalProperty> selectAll();

    int updateByPrimaryKey(ChargeGlobalProperty record);
}