package com.tian.mapper.market;

import com.tian.entity.market.ChargeCoupon;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
@Mapper
public interface ChargeCouponMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ChargeCoupon record);

    ChargeCoupon selectByPrimaryKey(Integer id);

    List<ChargeCoupon> selectAll();

    int updateByPrimaryKey(ChargeCoupon record);
}