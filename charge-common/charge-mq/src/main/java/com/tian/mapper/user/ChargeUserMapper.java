package com.tian.mapper.user;

import com.tian.entity.user.ChargeUser;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ChargeUserMapper {
    int deleteByPrimaryKey(Long id);

    int insert(ChargeUser record);

    ChargeUser selectByPrimaryKey(Long id);

    List<ChargeUser> selectAll();

    int updateByPrimaryKey(ChargeUser record);
}