package com.tian.consumer;

import com.alibaba.fastjson.JSON;
import com.rabbitmq.client.Channel;
import com.tian.constant.RabbitMQConstant;
import com.tian.message.InvitedRegistryMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * {@code @description:}  邀请记录
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024/1/26 11:16
 * {@code @version:} 1.0
 */
@Slf4j
@Component
@RabbitListener(queues = RabbitMQConstant.INVITED_RECORD_QUEUE)
public class InvitedRecordConsumer {

    @RabbitHandler
    public void process(String str, Channel channel, Message message) {
        try {
            InvitedRegistryMessage invitedRegistryMessage = JSON.parseObject(str, InvitedRegistryMessage.class);
            System.out.println("InvitedRecordConsumer receive  : " + JSON.toJSONString(invitedRegistryMessage));
            channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}

