package com.tian.consumer;

import com.alibaba.fastjson.JSON;
import com.tian.constant.RabbitMQConstant;
import com.tian.message.IncomeMessage;
import com.tian.service.ChargeUserIncomeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;
import com.rabbitmq.client.Channel;

import javax.annotation.Resource;
import java.io.IOException;

/**
 * {@code @description:}  收益增加+收益记录
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024/1/26 11:16
 * {@code @version:} 1.0
 */
@Slf4j
@Component
@RabbitListener(queues = RabbitMQConstant.INCOME_QUEUE)
public class IncomeConsumer {
    @Resource
    private ChargeUserIncomeService chargeUserIncomeService;

    @RabbitHandler
    public void process(String str, Channel channel, Message message) {
        try {
            IncomeMessage incomeMessage = JSON.parseObject(str, IncomeMessage.class);
            System.out.println("IncomeConsumer receive  : " + JSON.toJSONString(incomeMessage));
            chargeUserIncomeService.income(incomeMessage);
            channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
        } catch (Exception e) {
            log.error("收益增加+收益记录", e);
        }
    }
}

