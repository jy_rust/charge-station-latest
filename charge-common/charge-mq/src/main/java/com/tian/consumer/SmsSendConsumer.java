package com.tian.consumer;

import com.alibaba.fastjson.JSON;
import com.rabbitmq.client.Channel;
import com.tian.constant.RabbitMQConstant;
import com.tian.message.SmsSendMessage;
import com.tian.service.SmsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.IOException;

/**
 * {@code @description:} 异步发送短信
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024/1/22 21:23
 * {@code @version:} 1.0
 */
@Slf4j
@Component
@RabbitListener(queues = RabbitMQConstant.SMS_QUEUE)
public class SmsSendConsumer {

    @Resource
    private SmsService smsService;

    @RabbitHandler
    public void process(String str, Channel channel, Message message) {
        try {
            SmsSendMessage smsSendMessage = JSON.parseObject(str, SmsSendMessage.class);
            log.info("SmsSendConsumer receive msg={} : ", JSON.toJSONString(smsSendMessage));
            smsService.sendSms(smsSendMessage);
            channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
