package com.tian.service;

import com.tian.message.IncomeMessage;

/**
 * {@code @description:} 用户收益和记录
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024/1/28 11:20
 * {@code @version:} 1.0
 */
public interface ChargeUserIncomeService {

    /**
     * 用户收益和记录
     *
     * @param incomeMessage
     */
    void income(IncomeMessage incomeMessage);
}
