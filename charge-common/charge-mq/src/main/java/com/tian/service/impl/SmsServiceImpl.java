package com.tian.service.impl;

import com.tian.SmsResult;
import com.tian.SmsTemplateEnum;
import com.tian.factory.ApplicationContextFactory;
import com.tian.message.SmsSendMessage;
import com.tian.sender.SmsSender;
import com.tian.service.SmsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.LinkedHashMap;

/**
 * {@code @description:} 发送短信和短信验证码验证
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024/1/22 18:58
 * {@code @version:} 1.0
 */
@Slf4j
@Service
public class SmsServiceImpl implements SmsService {

    @Value("${sms.type}")
    private String smsType;

    @Override
    public void sendSms(SmsSendMessage smsSendMessage) {
        SmsSender smsSender = (SmsSender) ApplicationContextFactory.getBeanByName(smsType);
        if (smsSender == null) {
            log.error("短信厂商为配置，请速度配置~");
            return;
        }
        LinkedHashMap<String, String> params = new LinkedHashMap<>();
        SmsTemplateEnum smsTemplateEnum = smsSendMessage::getMsgTemplate;
        SmsResult smsResult = smsSender.send(smsTemplateEnum, params, smsSendMessage.getPhone());
        log.info("发送结果：" + smsResult);
    }
}
