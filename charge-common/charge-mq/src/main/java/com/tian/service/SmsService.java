package com.tian.service;

import com.tian.message.SmsSendMessage;

/**
 * {@code @description:} TODO
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024/1/22 16:57
 * {@code @version:} 1.0
 */
public interface SmsService {

    /**
     * 发送手机验证码
     *
     * @param smsSendMessage 短信类型  手机号  缓存前缀 等
     * @return 发送成功
     */
    void sendSms(SmsSendMessage smsSendMessage);
}
