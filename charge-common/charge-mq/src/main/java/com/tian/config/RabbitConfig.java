package com.tian.config;

import com.tian.constant.RabbitMQConstant;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * {@code @description:} rabbitMQ常量
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024/1/26 17:16
 * {@code @version:} 1.0
 */
@Configuration
public class RabbitConfig {

    @Bean
    public Queue queueMessageIncome() {
        return new Queue(RabbitMQConstant.INCOME_QUEUE);
    }

    @Bean
    TopicExchange exchangeIncome() {
        return new TopicExchange(RabbitMQConstant.INCOME_EXCHANGE);
    }

    @Bean
    Binding bindingExchangeMessageIncome(Queue queueMessageIncome, TopicExchange exchangeIncome) {
        return BindingBuilder.bind(queueMessageIncome).to(exchangeIncome).with(RabbitMQConstant.INCOME_ROUTING_KEY);
    }


    @Bean
    public Queue queueMessageInvited() {
        return new Queue(RabbitMQConstant.INVITED_RECORD_QUEUE);
    }

    @Bean
    TopicExchange exchangeInvited() {
        return new TopicExchange(RabbitMQConstant.INVITED_RECORD_EXCHANGE);
    }

    @Bean
    Binding bindingExchangeMessageInvited(Queue queueMessageInvited, TopicExchange exchangeInvited) {
        return BindingBuilder.bind(queueMessageInvited).to(exchangeInvited).with(RabbitMQConstant.INVITED_RECORD_ROUTING_KEY);
    }


    @Bean
    public Queue queueMessageSms() {
        return new Queue(RabbitMQConstant.SMS_QUEUE);
    }

    @Bean
    TopicExchange exchangeSms() {
        return new TopicExchange(RabbitMQConstant.SMS_EXCHANGE);
    }

    @Bean
    Binding bindingExchangeMessageSms(Queue queueMessageSms, TopicExchange exchangeSms) {
        return BindingBuilder.bind(queueMessageSms).to(exchangeSms).with(RabbitMQConstant.SMS_ROUTING_KEY);
    }

}
