package com.tian.controller;

import com.tian.entity.market.ChargeCoupon;
import com.tian.entity.user.ChargeUser;
import com.tian.mapper.market.ChargeCouponMapper;
import com.tian.mapper.user.ChargeUserMapper;
import org.redisson.api.RBucket;
import org.redisson.api.RedissonClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Date;

/**
 * {@code @description:} TODO
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024/1/27 15:58
 * {@code @version:} 1.0
 */
@RestController
@RequestMapping("/test")
public class TestController {

    @Resource
    private RedissonClient redissonClient;
    @Resource
    private ChargeUserMapper chargeUserMapper;
    @Resource
    private ChargeCouponMapper chargeCouponMapper;

    @GetMapping("/redis")
    public void test() {
     /*   RBucket<Date> bucket = redissonClient.getBucket("demo.2024-01-27");
        bucket.set(new Date());
        System.out.println(bucket.get());*/
        ChargeUser chargeUser = chargeUserMapper.selectByPrimaryKey(1L);
        System.out.println(chargeUser);
        ChargeCoupon chargeCoupon = chargeCouponMapper.selectByPrimaryKey(1);
        System.out.println(chargeCoupon);
    }
}
