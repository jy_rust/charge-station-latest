package com.tian;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * {@code @description:} 网关项目启动
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024/1/18 15:19
 * {@code @version:} 1.0
 */
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class,scanBasePackages = {"com.tian.client","com.tian.config","com.tian.filter"})
@EnableFeignClients
public class Application {
    //eyJhbGciOiJIUzI1NiJ9.eyJ1aWQiOjEsImV4cCI6MTcxNTMzMDA2M30.4K0EV3kk02KjNTQL_Kj5fvgy5Oaw71dhK9apSo1hsJI
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
