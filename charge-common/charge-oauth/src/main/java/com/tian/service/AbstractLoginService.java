package com.tian.service;

import com.alibaba.fastjson.JSON;
import com.tian.common.UserInfoCache;
import com.tian.dto.AuthLoginDto;
import com.tian.common.CommonResult;
import com.tian.dto.ChargeUserLoginResDto;
import com.tian.entity.ChargeUser;
import com.tian.enums.ResultCode;
import com.tian.exception.BusinessException;
import com.tian.util.JwtGeneratorUtil;
import com.tian.util.RedisConstantPre;
import com.tian.util.StringUtil;
import com.tian.util.UUID;
import lombok.extern.slf4j.Slf4j;
import org.joda.time.DateTime;
import org.redisson.api.RBucket;
import org.redisson.api.RedissonClient;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * {@code @description:} 登录抽象类
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-04-07 16:42
 * {@code @version:} 1.0
 */
@Slf4j
@Component
public abstract class AbstractLoginService implements LoginService {

    public static ConcurrentHashMap<Integer, AbstractLoginService> LOGIN_TYPE_MAP = new ConcurrentHashMap<>();
    @Resource
    private RedissonClient redissonClient;

    @Value("${singleton.login}")
    private boolean singletonLogin;

    @PostConstruct
    public void init() {
        LOGIN_TYPE_MAP.put(getLoginType(), this);
    }

    @Override
    public CommonResult<String> doLogin(AuthLoginDto authLoginDto) throws BusinessException {
        log.info("begin AbstractLogin.doLogin:" + authLoginDto);
        validate(authLoginDto);
        ChargeUser user = doProcessor(authLoginDto);
        Map<String, Object> payLoad = new HashMap<>();
        payLoad.put("uid", user.getId());
        payLoad.put("exp", DateTime.now().plusHours(1).toDate().getTime() / 1000);
        String token = JwtGeneratorUtil.generatorToken(payLoad);

        ChargeUserLoginResDto chargeUserLoginResDto = new ChargeUserLoginResDto();
        BeanUtils.copyProperties(user, chargeUserLoginResDto);
        RBucket<String> bucket = redissonClient.getBucket(RedisConstantPre.USER_INFO_PHONE_PRE + user.getPhone());
        if(singletonLogin){
            bucket.delete();
        }

        bucket.set(JSON.toJSONString(chargeUserLoginResDto));

        RBucket<String> bucketId = redissonClient.getBucket(RedisConstantPre.USER_INFO_ID_PRE + user.getId());
        if(singletonLogin){
            bucketId.delete();
        }
        bucketId.set(JSON.toJSONString(chargeUserLoginResDto));

        RBucket<String> bucketToken = redissonClient.getBucket(RedisConstantPre.TOKEN_PRE + token);
        if(singletonLogin){
            bucketToken.delete();
        }
        bucketToken.set(JSON.toJSONString(chargeUserLoginResDto));

        return CommonResult.success(token);
    }

    public abstract int getLoginType();

    public abstract void validate(AuthLoginDto authLoginDto);

    public abstract ChargeUser doProcessor(AuthLoginDto authLoginDto);

}
