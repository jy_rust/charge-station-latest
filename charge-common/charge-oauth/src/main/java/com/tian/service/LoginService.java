package com.tian.service;

import com.tian.dto.AuthLoginDto;
import com.tian.common.CommonResult;
import com.tian.exception.BusinessException;

/**
 * {@code @description:} 登录接口
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-04-07 16:42
 * {@code @version:} 1.0
 */
public interface LoginService {
    CommonResult<String>  doLogin(AuthLoginDto authLoginDto) throws BusinessException;
}
