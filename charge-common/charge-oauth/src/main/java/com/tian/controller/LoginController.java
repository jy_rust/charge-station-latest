package com.tian.controller;

import com.tian.common.CommonResult;
import com.tian.dto.AuthLoginDto;
import com.tian.exception.BusinessException;
import com.tian.exception.ValidException;
import com.tian.service.AbstractLoginService;
import com.tian.service.LoginService;
import com.tian.util.JwtGeneratorUtil;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.SignatureException;
import org.apache.commons.lang.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * {@code @description:}  用户登录token获取和验证
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-04-07 16:42
 * {@code @version:} 1.0
 */
@RestController
@RequestMapping("/user")
public class LoginController {

    @PostMapping("/login")
    public CommonResult<String> getToken(@RequestBody @Validated AuthLoginDto authLoginDto, BindingResult bindingResult) {
        authLoginDto.validData(bindingResult);
        LoginService login = AbstractLoginService.LOGIN_TYPE_MAP.get(authLoginDto.getLoginType());
        if (login == null) {
            throw new BusinessException("暂不支持该种登录类型");
        }
        return login.doLogin(authLoginDto);
    }
}
