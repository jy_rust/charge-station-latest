package com.tian.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年05月18日 14:51
 * 博客地址：<a href="http://woaijava.cc/">博客地址</a>
 */
@Data
@Builder
@ToString
@AllArgsConstructor
public class SysLog implements Serializable {
    private static final long serialVersionUID = 1L;

    private String methodDesc;

    private Date createTime;

    private String methodName;

    private Long costTime;

    private String requestParam;

    private String responseParam;

    private String ip;

    private String url;
}
