package com.tian.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年05月18日 14:51
 * 博客地址：<a href="http://woaijava.cc/">博客地址</a>
 */
@Data
@Builder
@AllArgsConstructor
public class SysLogMessage implements Serializable {
    private static final long serialVersionUID = 1L;

    private SysLog sysLog;
    private String methodDesc;
    private Integer sysLogType;
}
