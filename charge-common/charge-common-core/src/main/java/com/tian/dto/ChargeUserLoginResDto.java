package com.tian.dto;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * {@code @description:} TODO
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024/1/25 19:25
 * {@code @version:} 1.0
 */
@Data
@ToString
public class ChargeUserLoginResDto implements Serializable {
    private Long id;

    private String nickName;

    private String password;

    private String avatarImg;

    private String phone;

    private String email;

    private Integer authentication;

    private Integer vip;

    private String openId;

    private BigDecimal balance;

    private Integer point;

    private String inviteCode;

    private Integer type;

    private Integer status;

    private Date createTime;
}
