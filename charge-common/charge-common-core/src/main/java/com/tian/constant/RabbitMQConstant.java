package com.tian.constant;

/**
 * {@code @description:} TODO
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024/1/26 20:01
 * {@code @version:} 1.0
 */
public interface RabbitMQConstant {
    /**
     * 收益
     */
    String INCOME_QUEUE = "income.queue";
    String INCOME_EXCHANGE = "income.exchange";
    String INCOME_ROUTING_KEY = "income.routing.key";

    /**
     * 邀请记录
     */
    String INVITED_RECORD_QUEUE = "invited.record.queue";
    String INVITED_RECORD_EXCHANGE = "invited.record.exchange";
    String INVITED_RECORD_ROUTING_KEY = "invited.record.routing.key";

    /**
     * 发送短信
     */
    String SMS_QUEUE = "sms.queue";
    String SMS_EXCHANGE = "sms.exchange";
    String SMS_ROUTING_KEY = "sms.routing.key";

    /**
     * 收益
     */
    String SYS_LOG_QUEUE = "sys.log.queue";
    String SYS_LOG_EXCHANGE = "sys.log.exchange";
    String SYS_LOG_ROUTING_KEY = "sys.log.routing.key";

}
