package com.tian.constant;

/**
 * {@code @description:} TODO
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024/1/26 20:01
 * {@code @version:} 1.0
 */
public interface RabbitMqQueueConstant {
    /**
     * 邀请注册
     */
    String INCOME_QUEUE = "user.invited.registry.queue";

    /**
     * 邀请记录
     */
    String USER_INVITED_REGISTRY_QUEUE = "user.invited.registry.queue";

    /**
     * 发送短信
     */
    String SMS_QUEUE = "sms.queue";

    /**
     * 用户积分
     */
    String USER_POINT_QUEUE = "user.point.queue";

    /**
     * 发送短信验证码
     */
    String SEND_CODE = "send.code.queue";
    /**
     * 发送短信验证码
     */
    String SYS_LOG_QUEUE = "sys.log.queue";
}
