package com.tian.enums;

/**
 * @author tianwc 公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年05月12日 16:16
 * 博客地址：<a href="http://woaijava.cc/">博客地址</a>
 * <p>
 * 微信支付状态
 */
public enum WxPayStatusEnum {
    /**
     * 预支付--也叫支付中
     */
    INIT(0, "预支付单"),
    SUCCESS(1, "支付成功"),
    FAILED(2, "支付失败");
    private final int status;
    private final String desc;

    WxPayStatusEnum(int status, String desc) {
        this.status = status;
        this.desc = desc;
    }

    public int getStatus() {
        return status;
    }

    public String getDesc() {
        return desc;
    }
}
