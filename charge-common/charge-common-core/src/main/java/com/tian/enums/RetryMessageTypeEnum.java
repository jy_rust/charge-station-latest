package com.tian.enums;

/**
 * {@code @description:} 重试message类型
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-03-24 20:39
 * {@code @version:} 1.0
 */
public enum RetryMessageTypeEnum {
    USER_POINT(0, "用户积分变动消息");
    private final int type;
    private final String desc;

    RetryMessageTypeEnum(int type, String desc) {
        this.type = type;
        this.desc = desc;
    }

    public int getType() {
        return type;
    }

    public String getDesc() {
        return desc;
    }
}
