package com.tian.enums;

import com.tian.util.RedisConstantPre;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * {@code @description:} 短信类型
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024/1/28 19:14
 * {@code @version:} 1.0
 */
@Getter
public enum SendSmsTypeEnum {
    LOGIN(0, "登录", RedisConstantPre.CODE_LOGIN_PRE,"1111"),
    REGISTRY(1, "注册",RedisConstantPre.CODE_REGISTRY_PRE,"1111");

    private final int type;
    private final String desc;
    /**
     * 缓存前缀 参照： RedisConstantPre
     */
    private final String cacheKeyPre;
    /**
     * 短信模板
     */
    private final String msgTemplate;
    private static final Map<Integer, SendSmsTypeEnum> SEND_SMS_TYPE_ENUM_MAP = new HashMap<>();

    static {
        for (SendSmsTypeEnum value : SendSmsTypeEnum.values()) {
            SEND_SMS_TYPE_ENUM_MAP.put(value.getType(), value);
        }
    }

    SendSmsTypeEnum(int type, String desc, String cacheKeyPre, String msgTemplate) {
        this.type = type;
        this.desc = desc;
        this.cacheKeyPre = cacheKeyPre;
        this.msgTemplate = msgTemplate;
    }

    public static SendSmsTypeEnum getSendSmsTypeEnumType(int type) {
        return SEND_SMS_TYPE_ENUM_MAP.get(type);
    }
}
