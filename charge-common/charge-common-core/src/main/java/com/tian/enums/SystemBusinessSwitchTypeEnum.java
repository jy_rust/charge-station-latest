package com.tian.enums;

import lombok.Getter;

/**
 * {@code @description:} 系统开关类型，运营可以针对功能进行开启和关闭
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-03-30 16:51
 * {@code @version:} 1.0
 */
@Getter
public enum SystemBusinessSwitchTypeEnum {
    REGISTRY_SEND_POINT(0, "注册送积分"),
    INVITED_REGISTRY_INCOME(1, "邀请注册收益");
    private final int type;
    private final String desc;

    SystemBusinessSwitchTypeEnum(int type, String desc) {
        this.type = type;
        this.desc = desc;
    }
}
