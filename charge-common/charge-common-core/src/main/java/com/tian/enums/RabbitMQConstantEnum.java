package com.tian.enums;

import com.tian.constant.RabbitMqQueueConstant;

/**
 * {@code @description:} TODO
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-03-24 20:57
 * {@code @version:} 1.0
 */
public enum RabbitMQConstantEnum {
    /**
     * 用户积分
     */
    USER_POINT(RabbitMqQueueConstant.USER_POINT_QUEUE,"user.point.exchange","user.point.routing.key",RetryMessageTypeEnum.USER_POINT.getType()),
    /**
     * 邀请用户注册
     */
    USER_INVITED_REGISTRY(RabbitMqQueueConstant.USER_INVITED_REGISTRY_QUEUE,"user.invited.registry.exchange","user.invited.registry.routing.key",0),
    /**
     * 发生短信验证码
     */
    SEND_CODE(RabbitMqQueueConstant.SEND_CODE,"send.code.exchange","send.code.registry.routing.key",0);
    private final String queue;
    private final String exchange;
    private final String routingKey;
    private final int type;

    RabbitMQConstantEnum(String queue, String exchange, String routingKey, int type) {
        this.queue = queue;
        this.exchange = exchange;
        this.routingKey = routingKey;
        this.type = type;
    }

    public String getQueue() {
        return queue;
    }

    public String getExchange() {
        return exchange;
    }

    public String getRoutingKey() {
        return routingKey;
    }

    public int getType() {
        return type;
    }
}
