package com.tian.enums;

import lombok.Getter;

/**
 * {@code @description:} 枚举了一些常用API操作码
 * 200000  操作成功
 * 500000---599999  系统出错
 * 400001---499999  参数问题和业务提示
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024/1/18 19:03
 * {@code @version:} 1.0
 */
@Getter
public enum ResultCode {
    /**
     * 营销中心相关
     */
    TREMAIN_COUNT_LIMITER(400101, "活动优惠券已领完"),
    COUPON_USED(400102, "优惠券已使用过，请勿重复使用！"),
    COUPON_EXPIRED(400103, "该优惠券已过期！"),
    COUPON_OVERLAY(400104, "优惠券不能叠加使用！"),
    ACTIVITY_DELETE(400105, "活动已下架"),
    ACTIVITY_NO_COUPON(400106, "活动没有此奖项"),
    GOODS_STOCK_NO_ENOUGH(400107, "商品库存不足"),
    ACTIVITY_EXPIRED(400108, "活动已失效"),
    COUPON_DOWN(400109, "优惠券已下架，不可领取！"),

    /**
     * 验证码
     */
    CODE_ERROR(400201, "验证码错误"),
    CODE_ERROR_VALID_TIMES(400202, "验证码错误"),
    CODE_EXPIRE(400203, "验证码已过期"),
    SMS_SEND_TIMES_LIMIT(400022, "已超发送短信次数限制！"),
    SEND_MESSAGE_LIMIT(400015, "短信发送太频繁~"),

    /**
     * 用户相关
     */
    UN_AUTHENTICATION(400301, "用户未认证"),
    ACTIVITY_NO_ENOUGH(400302, "用户积分不够"),
    USER_COUPON_LIMITER(400303, "用户领取已上达上线"),
    USER_NO_EXIST(400304, "用户不存在！"),
    USER_DELETE(400305, "用户已注销！"),
    FORBIDDEN(400306, "没有相关权限"),
    UNAUTHORIZED(400307, "暂未登录或token已经过期"),
    INVITED_CODE_NO_EXIST(400308, "邀请码不存在！"),
    USER_EXISTED(400309, "用户手机号已存在！"),
    CAR_NO_EXIST(400310, "该车牌号不存在！"),

    /**
     * 充电桩
     */
    GUN_USING(400600, "枪正在充电中，不能重复充电！"),
    GUN_ERROR(400601, "充电枪异常，暂不能使用！"),
    CHARGE_DATA_NOT_EXIST(400602, "充电数据异常"),
    CHARGING_START_FAILED(400603, "充电失败"),

    PARAMETER_EMPTY(400501, "参数为空"),
    VALIDATE_FAILED(400502, "参数检验失败"),
    PARAMETER_ERROR(400503, "参数有误"),

    FAILED(500000, "操作失败"),
    FALLBACK_FAILED(500001, "服务熔断"),

    SUCCESS(200000, "操作成功");

    private final int code;
    private final String message;

    ResultCode(int code, String message) {
        this.code = code;
        this.message = message;
    }

}