package com.tian.enums;

import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * {@code @description:} 收益类型枚举
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024/1/26 11:07
 * {@code @version:} 1.0
 */
@Getter
public enum IncomeTypeEnum {
    INVITED_INCOME(0, "邀请获取收益");

    private final int type;
    private final String desc;

    IncomeTypeEnum(int type, String desc) {
        this.type = type;
        this.desc = desc;
    }

    private static final Map<Integer, IncomeTypeEnum> INCOME_TYPE_ENUM_MAP = new HashMap<>();

    static {
        for (IncomeTypeEnum value : IncomeTypeEnum.values()) {
            INCOME_TYPE_ENUM_MAP.put(value.getType(), value);
        }
    }

    public static String getDescByType(int type) {
        return INCOME_TYPE_ENUM_MAP.get(type).desc;
    }
}
