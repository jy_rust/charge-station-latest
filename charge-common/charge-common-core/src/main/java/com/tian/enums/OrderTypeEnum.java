package com.tian.enums;

/**
 * @author tianwc 公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 2023年11月07日 21:37
 * 在线刷题 1200+题和1000+篇干货文章：<a href="https://woaijava.cc/">博客地址</a>
 * <p>
 * 支付业务类型
 */
public enum OrderTypeEnum {
    /**
     * 充值支付
     */
    CHARGE(0, "充值"),
    /**
     * 付款支付
     */
    PAY_CHARGER(1, "充电支付");
    private final int business;
    private final String desc;

    OrderTypeEnum(int business, String desc) {
        this.business = business;
        this.desc = desc;
    }

    public int getBusiness() {
        return business;
    }

    public String getDesc() {
        return desc;
    }
}
