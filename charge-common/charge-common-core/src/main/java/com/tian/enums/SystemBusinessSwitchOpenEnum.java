package com.tian.enums;

import lombok.Getter;

/**
 * {@code @description:} TODO
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-03-30 19:44
 * {@code @version:} 1.0
 */
@Getter
public enum SystemBusinessSwitchOpenEnum {
    OPEN(0, "开启"),
    CLOSE(1, "关闭");
    private final int type;
    private final String desc;

    SystemBusinessSwitchOpenEnum(int type, String desc) {
        this.type = type;
        this.desc = desc;
    }
}
