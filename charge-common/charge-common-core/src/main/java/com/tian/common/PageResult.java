package com.tian.common;

import lombok.Data;

/**
 * {@code @description:} TODO
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024/1/23 10:37
 * {@code @version:} 1.0
 */
public class PageResult<T> extends PageBase {

    private T data;

    public PageResult(int total, int pageSize, int pages, int currentPage, T data) {
        super(total, pageSize, pages, currentPage);
        this.data = data;
    }

    public static <T> PageResult<T> setData(int total, int pageSize, int pages, int currentPage, T data) {
        return new PageResult<T>(total, pageSize, pages, currentPage, data);
    }
}
