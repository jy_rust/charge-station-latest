package com.tian.common;

import lombok.Data;

/**
 * {@code @description:} 分页基类
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024/1/23 10:35
 * {@code @version:} 1.0
 */
@Data
public class PageBase {
    protected int total;
    protected int pageSize;
    protected int pages;
    protected int currentPage;
    protected int start;

    public PageBase(int total, int pageSize, int pages, int currentPage) {
        this.total = total;
        this.pageSize = pageSize;
        this.pages = pages;
        this.currentPage = currentPage;
        this.start = currentPage * pages;
    }
}
