package com.tian.message;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * {@code @description:} 用户积分消息
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-03-24 18:49
 * {@code @version:} 1.0
 */
@Data
@ToString
public class UserPointMessage extends BaseMessage {

    private Long userId;

    private Integer point;

    private Integer type;
}
