package com.tian.message;

import lombok.Data;

import java.io.Serializable;
import java.util.LinkedHashMap;

/**
 * {@code @description:} 短信消息体
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024/1/22 17:42
 * {@code @version:} 1.0
 */
@Data
public class SmsSendMessage implements Serializable {

    /**
     * 手机号码
     */
    private String phone;
    /**
     * 缓存前缀 参照： RedisConstantPre
     */
    private String cacheKeyPre;
    /**
     * 短信模板
     */
    private String msgTemplate;
    private LinkedHashMap<String, String> params;
}
