package com.tian.message;

/**
 * {@code @description:} 请求唯一id前缀
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-03-25 9:18
 * {@code @version:} 1.0
 */
public interface MessageReqIdPrefixConstant {

    String USER_POINT_UPDATE_REQ_ID_PREFIX ="USER_POINT_UPDATE";
    String USER_INVITED_REGISTRY_REQ_ID_PREFIX ="USER_INVITED_REGISTRY";
}
