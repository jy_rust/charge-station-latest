package com.tian.message;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * {@code @description:} 短信消息体
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024/1/22 17:42
 * {@code @version:} 1.0
 */
@NoArgsConstructor
@ToString
@Data
public class InvitedRegistryMessage extends BaseMessage implements Serializable {

    /**
     * 邀请用户id
     */
    private Long userId;
    /**
     * 注册用户id
     */
    private Long newUserId;
    /**
     * 收益
     */
    private BigDecimal amount;
    /**
     * 邀请时间
     */
    private Date createTime;


}
