package com.tian.message;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * {@code @description:} 用户获取收益消息体
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024/1/22 17:42
 * {@code @version:} 1.0
 */
@NoArgsConstructor
@Data
public class IncomeMessage implements Serializable {

    /**
     * 用户id
     */
    private Long userId;
    /**
     * 收益
     */
    private Integer incomeType;
    /**
     * 备注
     */
    private String msg;

    private Date createTime;
}
