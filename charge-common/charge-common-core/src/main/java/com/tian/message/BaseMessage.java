package com.tian.message;

import lombok.Data;

import java.io.Serializable;

/**
 * {@code @description:} 每个message都有一个reqId 表示消息的唯一性 主要是可以用于 重复消费问题
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-03-25 9:13
 * {@code @version:} 1.0
 */
@Data
public class BaseMessage implements Serializable {
    protected String reqId;
}
