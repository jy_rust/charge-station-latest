package com.tian.util;

/**
 * {@code @description:} 统一规范化Redis 的key 前缀
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024/1/20 16:47
 * {@code @version:} 1.0
 */
public interface RedisConstantPre {
    String COUPON_CONDITION_PRE = "coupon.condition.pre:";
    /**
     * 分布式锁，锁住用户
     */
    String USER_INFO_ID_LOCK_PRE = "user.info.id.lock.pre_";
    String COUPON_PRE = "coupon.pre:";
    /**
     * id 缓存用户数据
     */
    String USER_INFO_ID_PRE = "user.info.id.pre_";
    /**
     * 手机号缓存用户数据
     */
    String USER_INFO_PHONE_PRE = "user.info.phone.pre:";
    /**
     * token 缓存用户数据
     */
    String TOKEN_PRE = "token.pre:";

    String CODE_LOGIN_PRE = "code.login.pre:";
    String CODE_REGISTRY_PRE = "code.registry.pre:";

    String ORDER_PRE = "WX_PAY_";

    String USE_USER_COUPON_PRE = "use_user_coupon_pre_";

    String MONTH_COUNT_STATISTICS_CHARGE = "month.count.statistics.charge.times_";

    String MESSAGE_LIMIT_KEY_PRE = "message_limit_key_pre";


    String SEND_CODE_PRE = "send.code.redis_pre";

    String MESSAGE_TEMPLATE_KEY_PRE = "message.template.key_pre";

    String CHARGE_STATION_PRE="charge.station_pre";
    String CHARGE_STATION_GUN_NO_PRE ="charge.station.gun.no_pre";
    String CHARGE_STATION_GUN_ID_PRE ="charge.station.gun.id_pre";
    String CHARGING_RECORD_PRE ="charging.record.id_pre";
}
