package com.tian.util;


import java.util.UUID;

/**
 * {@code @description:}  消息唯一id
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-03-30 16:59
 * {@code @version:} 1.0
 */
public class MessageUniqueIdUtil {

    public static String getId(String preFix) {
        return preFix +
                UUID.randomUUID() +
                DateUtils.formatDefaultDateMs();
    }
}
