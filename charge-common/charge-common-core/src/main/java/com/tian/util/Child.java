package com.tian.util;

/**
 * {@code @description:} TODO
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-05-20 17:47
 * {@code @version:} 1.0
 */
public class Child extends Parent {
    // 子类的静态变量
    static String childStaticVar = "子类-静态变量";

    // 子类的静态代码块
    static {
        System.out.println("子类-静态代码块: " + childStaticVar);
    }

    // 子类的实例变量
    String childInstanceVar = "子类-实例变量";

    // 子类的构造方法
    public  Child() {
        System.out.println("子类-构造方法: " + childInstanceVar);
    }
}
