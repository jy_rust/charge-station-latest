package com.tian.util;

/**
 * {@code @description:} 父类
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-05-20 17:46
 * {@code @version:} 1.0
 */
public class Parent {

    // 父类的静态变量
    static String parentStaticVar = "父类-静态变量";
    // 父类的静态代码块
    static {
        System.out.println("父类-静态代码块: " + parentStaticVar);
    }

    // 父类的实例变量
    String parentInstanceVar = "父类-实例变量";

    // 父类的构造方法
   public Parent() {
        System.out.println("父类-构造方法: " + parentInstanceVar);
    }

}
