package com.tian.interceptor;

import com.alibaba.fastjson.JSON;
import com.tian.annotation.SysLogAnnotation;
import com.tian.dto.SysLog;
import com.tian.dto.SysLogMessage;
import com.tian.enums.SysLogTypeEnum;
import com.tian.producer.SysLogProducer;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.*;

/**
 * {@code @description:} 系统日志切面
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-06-29 17:33
 * {@code @version:} 1.0
 */
@Aspect
@Component
@Slf4j
public class SysLogAspect {

    @Resource
    private SysLogProducer sysLogProducer;
    @Pointcut("@annotation(com.tian.annotation.SysLogAnnotation)")
    public void sysLog() {

    }

    @Around("sysLog()")
    public Object doAround(ProceedingJoinPoint joinPoint) throws Throwable {
        long startTime = System.currentTimeMillis();
        //获取当前请求对象
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();

        Object result = joinPoint.proceed();

        String className = joinPoint.getTarget().getClass().getName();
        Signature signature = joinPoint.getSignature();
        MethodSignature methodSignature = (MethodSignature) signature;
        Method method = methodSignature.getMethod();

        SysLogAnnotation argsLogAnnotation = method.getAnnotation(SysLogAnnotation.class);

        String methodDescription = argsLogAnnotation.methodDescription();
        SysLogTypeEnum sysLogTypeEnum = argsLogAnnotation.type();
        long endTime = System.currentTimeMillis();
        String url = request.getRequestURL().toString();

        SysLog sysLog = SysLog.builder().methodDesc(methodDescription).methodName(className + "#" + method.getName())
                .requestParam(JSON.toJSONString(getParameter(method, joinPoint.getArgs()))).ip(request.getRemoteAddr())
                .responseParam(JSON.toJSONString(result)).costTime((endTime - startTime)).createTime(new Date())
                .url(url).build();
        SysLogMessage sysLogMessage= SysLogMessage.builder().sysLog(sysLog).sysLogType(sysLogTypeEnum.getType()).build();
        sysLogProducer.send(sysLogMessage);
        return result;
    }

    /**
     * 根据方法和传入的参数获取请求参数
     */
    private Object getParameter(Method method, Object[] args) {
        List<Object> argList = new ArrayList<>();
        Parameter[] parameters = method.getParameters();
        for (int i = 0; i < parameters.length; i++) {
            //将RequestBody注解修饰的参数作为请求参数
            RequestBody requestBody = parameters[i].getAnnotation(RequestBody.class);
            if (requestBody != null) {
                argList.add(args[i]);
            }
            //将RequestParam注解修饰的参数作为请求参数
            RequestParam requestParam = parameters[i].getAnnotation(RequestParam.class);
            if (requestParam != null) {
                Map<String, Object> map = new HashMap<>();
                String key = parameters[i].getName();
                if (!StringUtils.isEmpty(requestParam.value())) {
                    key = requestParam.value();
                }
                map.put(key, args[i]);
                argList.add(map);
            }
        }
        if (argList.isEmpty()) {
            return null;
        } else if (argList.size() == 1) {
            return argList.get(0);
        } else {
            return argList;
        }
    }
}
