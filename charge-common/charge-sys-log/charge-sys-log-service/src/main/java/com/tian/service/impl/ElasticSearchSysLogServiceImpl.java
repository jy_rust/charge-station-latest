package com.tian.service.impl;

import com.tian.dto.SysLog;
import com.tian.dto.SysLogMessage;
import com.tian.service.SysLogService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * 博客地址：<a href="http://woaijava.cc/">博客地址</a>
 * <p>
 * 日志输出到ES中
 */
@Slf4j
@Service
public class ElasticSearchSysLogServiceImpl implements SysLogService {

    @Override
    public void logRecord(SysLog sysLog) {
        log.info("日志输出到ES中：{}", sysLog);
    }
}
