package com.tian.service;

import com.tian.dto.SysLog;
import com.tian.dto.SysLogMessage;

/**
 * @author tianwc 公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年06月14日 10:42
 * 博客地址：<a href="http://woaijava.cc/">博客地址</a>
 */
public interface SysLogService {
    /**
     * 日志记录
     *
     * @param sysLog 日志参数
     */
    void logRecord(SysLog sysLog);
}
