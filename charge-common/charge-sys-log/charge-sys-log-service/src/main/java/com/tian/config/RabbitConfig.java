package com.tian.config;

import com.tian.constant.RabbitMQConstant;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * {@code @description:} rabbitMQ常量
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024/6/26 17:16
 * {@code @version:} 1.0
 */
@Configuration
public class RabbitConfig {

    @Bean
    public Queue queueMessageSysLog() {
        return new Queue(RabbitMQConstant.SYS_LOG_QUEUE);
    }

    @Bean
    TopicExchange exchangeSysLog() {
        return new TopicExchange(RabbitMQConstant.SYS_LOG_EXCHANGE);
    }

    @Bean
    Binding bindingExchangeMessageSysLog(Queue queueMessageSysLog, TopicExchange exchangeSysLog) {
        return BindingBuilder.bind(queueMessageSysLog).to(exchangeSysLog).with(RabbitMQConstant.SYS_LOG_ROUTING_KEY);
    }


}
