package com.tian;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 系统日志 服务启动类
 */
@SpringBootApplication
public class SysLogApplication {
    public static void main(String[] args) {
        SpringApplication.run(SysLogApplication.class, args);
    }
}
