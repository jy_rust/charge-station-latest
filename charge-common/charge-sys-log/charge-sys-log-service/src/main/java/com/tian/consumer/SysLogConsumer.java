package com.tian.consumer;

import com.alibaba.fastjson.JSON;
import com.rabbitmq.client.Channel;
import com.tian.constant.RabbitMqQueueConstant;
import com.tian.dto.SysLogMessage;
import com.tian.factory.SysLogStrategyFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.IOException;

/**
 * {@code @description:}  日志消费者
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024-06-29 19:13
 * {@code @version:} 1.0
 */
@RabbitListener(queues = RabbitMqQueueConstant.SYS_LOG_QUEUE)
@Component
@Slf4j
public class SysLogConsumer {
    @Resource
    private SysLogStrategyFactory strategyFactory;

    @RabbitHandler
    public void process(Object data, Channel channel, Message message) throws IOException {
        try {
            log.info("消费者接受到的消息是：{},消息体为：{}", data, message);
            SysLogMessage sysLogMessage = JSON.parseObject(new String(message.getBody()), SysLogMessage.class);
            strategyFactory.getInvokeStrategy(sysLogMessage.getSysLogType()).logRecord(sysLogMessage.getSysLog());
            channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
        } catch (Exception exception) {
            channel.basicNack(message.getMessageProperties().getDeliveryTag(), false, false);
        }
    }
}
