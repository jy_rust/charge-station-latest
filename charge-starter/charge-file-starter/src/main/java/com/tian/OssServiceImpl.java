package com.tian;

import com.aliyun.oss.OSSClient;
import com.aliyun.oss.common.utils.IOUtils;
import com.aliyun.oss.model.*;
import com.tian.util.DateUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;

import java.io.InputStream;
import java.util.Date;

@Slf4j
public class OssServiceImpl implements OssService{
    /**
     * endpoint
     */
    private String endpoint;

    /**
     * access key id
     */
    private String accessKeyId;

    /**
     * access key secret
     */
    private String accessKeySecret;

    /**
     * bucket name (namespace)
     */
    private String bucketName;

    /**
     * file host (dev/demo/prod)
     */
    private String fileHost;

    public OssServiceImpl(String endpoint, String accessKeyId, String accessKeySecret, String bucketName, String fileHost) {
        this.endpoint = endpoint;
        this.accessKeyId = accessKeyId;
        this.accessKeySecret = accessKeySecret;
        this.bucketName = bucketName;
        this.fileHost = fileHost;
    }

    /**
     * 以文件流的方式上传文件
     *
     * @param fileName    文件名称
     * @param filePath    文件路径
     * @param inputStream 文件输入流
     * @return
     */
    @Override
    public String uploadFile(String fileName, String filePath, InputStream inputStream) {
        return coreUpload(fileName, filePath, inputStream);
    }

    /**
     * 核心上传功能
     *
     * @param fileName    文件名
     * @param filePath    文件路径
     * @param inputStream 文件输入流
     * @return
     */
    private String coreUpload(String fileName, String filePath, InputStream inputStream) {
        log.info("Start to upload file....");
        if (StringUtils.isEmpty(fileName) || inputStream == null) {
            log.error("Filename Or inputStream is lack when upload file.");
            return null;
        }
        if (StringUtils.isEmpty(filePath)) {
            log.warn("File path is lack when upload file but we automatically generated");
            String dateCategory = DateUtils.format(new Date(), "yyyyMMdd");
            filePath = OssConstant.FLAG_SLANTING_ROD.concat(dateCategory).concat(OssConstant.FLAG_SLANTING_ROD);
        }
        String fileUrl;
        OSSClient ossClient = null;
        try {

            // If the upload file size exceeds the limit
           /* long maxSizeAllowed = getMaximumFileSizeAllowed();
            if(Long.valueOf(inputStream.available()) > maxSizeAllowed) {
                log.error("Uploaded file is too big.");
                return null;
            }*/

            // Create OSS instance
            ossClient = new OSSClient(endpoint, accessKeyId, accessKeySecret);

            // Create bucket if not exists
            if (!ossClient.doesBucketExist(bucketName)) {
                log.info("Bucket '{}' is not exists and create it now.", bucketName);
                ossClient.createBucket(bucketName);
                CreateBucketRequest createBucketRequest = new CreateBucketRequest(bucketName);
                createBucketRequest.setCannedACL(CannedAccessControlList.PublicRead);
                ossClient.createBucket(createBucketRequest);
            }

            /*********************************/
            // List the bucket in my account
            //listBuckets(ossClient);
            /*********************************/

            // File path format
            if (!filePath.startsWith(OssConstant.FLAG_SLANTING_ROD)) {
                filePath = OssConstant.FLAG_SLANTING_ROD.concat(filePath);
            }
            if (!filePath.endsWith(OssConstant.FLAG_SLANTING_ROD)) {
                filePath = filePath.concat(OssConstant.FLAG_SLANTING_ROD);
            }

            // File url
            StringBuilder buffer = new StringBuilder();
            buffer.append(fileHost).append(filePath).append(fileName);
            fileUrl = buffer.toString();
            log.info("After format the file url is {}", fileUrl);

            // Upload file and set ACL
            PutObjectResult result = ossClient.putObject(new PutObjectRequest(bucketName, fileUrl, inputStream));
            ossClient.setBucketAcl(bucketName, CannedAccessControlList.PublicRead);
            if (result != null) {
                log.info("Upload result:{}", result.getETag());
                log.info("Upload file {} successfully.", fileName);
            }
            fileUrl = getHostUrl().concat(fileUrl);
            log.info("Call path is {}", fileUrl);

            /***********************************/
            // List objects in your bucket
            //listObjects(ossClient);
            /***********************************/

        } catch (Exception e) {
            log.error("Upload file failed.", e);
            fileUrl = null;
        } finally {
            if (ossClient != null) {
                ossClient.shutdown();
            }
        }
        return fileUrl;
    }

    /**
     * 列出buckets下的所有文件
     *
     * @param ossClient
     * @Author: Captain&D
     * @cnblogs: https://www.cnblogs.com/captainad
     */
    private void listObjects(OSSClient ossClient) {
        System.out.println("Listing objects");
        ObjectListing objectListing = ossClient.listObjects(bucketName);
        for (OSSObjectSummary objectSummary : objectListing.getObjectSummaries()) {
            System.out.println(" - " + objectSummary.getKey() + "  " +
                    "(size = " + objectSummary.getSize() + ")");
        }
        System.out.println();
    }

    /**
     * 列出当前用户下的所有bucket
     *
     * @param ossClient
     * @Author: Captain&D
     * @cnblogs: https://www.cnblogs.com/captainad
     */
    private void listBuckets(OSSClient ossClient) {
        System.out.println("Listing buckets");
        ListBucketsRequest listBucketsRequest = new ListBucketsRequest();
        listBucketsRequest.setMaxKeys(500);
        for (Bucket bucket : ossClient.listBuckets()) {
            System.out.println(" - " + bucket.getName());
        }
        System.out.println();
    }

    /**
     * 以文件的形式上传文件
     *
     * @param fileName
     * @return
     * @Author: Captain&D
     * @cnblogs: https://www.cnblogs.com/captainad
     */
    @Override
    public String uploadFile(String fileName, InputStream inputStream) {

        String fileUrl = null;
        try {
            fileUrl = uploadFile(fileName, null, inputStream);
        } catch (Exception e) {
            log.error("Upload file error.", e);
        } finally {
            IOUtils.safeClose(inputStream);
        }
        return fileUrl;
    }

    /**
     * 获取访问的base地址
     *
     * @return
     * @Author: Captain&D
     * @cnblogs: https://www.cnblogs.com/captainad
     */
    private String getHostUrl() {
        String hostUrl = null;
        if (this.endpoint.startsWith(OssConstant.FLAG_HTTP)) {
            hostUrl = OssConstant.FLAG_HTTP.concat(this.bucketName).concat(OssConstant.FLAG_DOT)
                    .concat(this.endpoint.replace(OssConstant.FLAG_HTTP, OssConstant.FLAG_EMPTY_STRING)).concat(OssConstant.FLAG_SLANTING_ROD);
        } else if (this.endpoint.startsWith(OssConstant.FLAG_HTTPS)) {
            return OssConstant.FLAG_HTTPS.concat(this.bucketName).concat(OssConstant.FLAG_DOT)
                    .concat(this.endpoint.replace(OssConstant.FLAG_HTTPS, OssConstant.FLAG_EMPTY_STRING)).concat(OssConstant.FLAG_SLANTING_ROD);
        }
        return hostUrl;
    }

    /**
     * 删除文件
     *
     * @param fileUrl 文件访问的全路径
     */
    public void deleteFile(String fileUrl) {
        log.info("Start to delete file from OSS.{}", fileUrl);
        if (StringUtils.isEmpty(fileUrl)
                || (!fileUrl.startsWith(OssConstant.FLAG_HTTP)
                && !fileUrl.startsWith(OssConstant.FLAG_HTTPS))) {
            log.error("Delete file failed because the invalid file address. -> {}", fileUrl);
            return;
        }
        OSSClient ossClient = null;
        try {
            /**
             * http:// bucketname                                dev/demo/pic/abc.jpg = key
             * http:// captainad.oss-ap-southeast-1.aliyuncs.com/dev/demo/pic/abc.jpg
             */
            String key = fileUrl.replace(getHostUrl(), OssConstant.FLAG_EMPTY_STRING);
            if (log.isDebugEnabled()) {
                log.debug("Delete file key is {}", key);
            }
            ossClient = new OSSClient(endpoint, accessKeyId, accessKeySecret);
            ossClient.deleteObject(bucketName, key);
        } catch (Exception e) {
            log.error("Delete file error.", e);
        } finally {
            if (ossClient != null) {
                ossClient.shutdown();
            }
        }
    }

}
