package com.tian;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.io.Serializable;

/**
 * {@code @description:} TODO
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024/2/22 16:52
 * {@code @version:} 1.0
 */
@Data
@Component
@ConfigurationProperties(prefix = "charge.oss")
public class OssProperties implements Serializable {
    private String endpoint;
    private String keyId;
    private String keySecret;
    private String bucketName;
    private String fileHost;
}
