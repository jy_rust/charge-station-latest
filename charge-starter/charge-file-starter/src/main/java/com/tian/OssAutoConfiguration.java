package com.tian;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;

/**
 * {@code @description:} TODO
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024/2/22 16:53
 * {@code @version:} 1.0
 */
@Configuration
@EnableConfigurationProperties(OssProperties.class)
public class OssAutoConfiguration {

    @Resource
    private OssProperties ossProperties;

    @Bean
    public OssService ossService() {
        return new OssServiceImpl(ossProperties.getEndpoint(), ossProperties.getKeyId(), ossProperties.getKeySecret(),
                ossProperties.getBucketName(), ossProperties.getFileHost());
    }
}
