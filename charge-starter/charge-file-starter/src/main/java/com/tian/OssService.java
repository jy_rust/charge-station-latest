package com.tian;

import java.io.InputStream;

/**
 * {@code @description:} TODO
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024/2/23 10:49
 * {@code @version:} 1.0
 */
public interface OssService {
    String uploadFile(String fileName, String filePath, InputStream inputStream);

    String uploadFile(String fileName, InputStream inputStream);
}
