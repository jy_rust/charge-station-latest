package com.tian;

/**
 * {@code @description:} TODO
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024/2/22 17:09
 * {@code @version:} 1.0
 */
public class OssConstant {
    /**
     * 斜杠
     */
    public final static String FLAG_SLANTING_ROD = "/";
    /**
     * http://
     */
    public final static String FLAG_HTTP = "http://";
    /**
     * https://
     */
    public final static String FLAG_HTTPS = "https://";
    /**
     * 空字符串
     */
    public final static String FLAG_EMPTY_STRING = "";
    /**
     * 点号
     */
    public final static String FLAG_DOT = ".";

    /**
     * 横杠
     */
    public final static String FLAG_CROSSBAR = "-";

    /**
     * 缺省的最大上传文件大小：20M
     */
    public final static int DEFAULT_MAXIMUM_FILE_SIZE = 20;
}
