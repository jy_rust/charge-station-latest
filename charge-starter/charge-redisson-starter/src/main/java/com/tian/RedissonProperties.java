package com.tian;


import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * {@code @description:} redis配置信息
 *
 * @author tianwc 公众号：Java后端技术全栈
 * 在线刷题 1200+java面试题和1000+篇技术文章：<a href="https://woaijava.cc/">博客地址</a>
 * {@code @date:} 2024/1/18 15:03
 * {@code @version:} 1.0
 */
@Data
@ConfigurationProperties(prefix = "charge.redisson")
public class RedissonProperties {

    private String host = "localhost";
    private int port = 6379;
    private int timeout = 1000;
}
