package com.tian.sender;

import com.baidubce.auth.DefaultBceCredentials;
import com.baidubce.services.sms.SmsClient;
import com.baidubce.services.sms.SmsClientConfiguration;
import com.baidubce.services.sms.model.SendMessageV3Request;
import com.baidubce.services.sms.model.SendMessageV3Response;
import lombok.extern.slf4j.Slf4j;

import java.util.LinkedHashMap;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年07月20日 15:36
 * 在线刷题 1200+题和1000+篇干货文章：<a href="http://woaijava.cc/">博客地址</a>
 *
 * 发送百度
 */
@Slf4j
public class BaiduSmsSender extends AbstractSmsSender {
    private static final String ENDPOINT = "http://smsv3.bj.baidubce.com";
    private final String accessKeyId;
    private final String secretAccessKey;
    private final String signatureId;

    public BaiduSmsSender( String accessKeyId, String secretAccessKey, String signatureId) {

        this.accessKeyId = accessKeyId;
        this.secretAccessKey = secretAccessKey;
        this.signatureId = signatureId;
    }



    @Override
    protected String doSend(String templateCode, LinkedHashMap<String, String> params, String phoneNumber) {
        String result = "1";
        SmsClientConfiguration config = new SmsClientConfiguration();
        config.setCredentials(new DefaultBceCredentials(accessKeyId, secretAccessKey));
        config.setEndpoint(ENDPOINT);
        SmsClient client = new SmsClient(config);

        SendMessageV3Request request = new SendMessageV3Request();
        request.setMobile(phoneNumber);
        request.setSignatureId(signatureId);
        request.setTemplate(templateCode);
        request.setContentVar(params);
        SendMessageV3Response response = client.sendMessage(request);
        // 解析请求响应 response.isSuccess()为true 表示成功
        if (response != null && response.isSuccess()) {
            return "0";
        } else {
            String reason = response == null ? "unknown" : response.getMessage();
            log.error("sms send error,phoneNumber: {}, reason : {}", phoneNumber, reason);
            return result;
        }
    }

}