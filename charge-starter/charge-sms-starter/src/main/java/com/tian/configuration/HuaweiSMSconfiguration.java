package com.tian.configuration;
import com.tian.SmsProperties;
import com.tian.sender.HuaweiSmsSender;
import com.tian.sender.SmsSender;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;


/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年07月20日 15:39
 * 在线刷题 1200+题和1000+篇干货文章：<a href="http://woaijava.cc/">博客地址</a>
 *
 * 对接华为短信发送
 */
@Configuration(proxyBeanMethods = false)
@ConditionalOnClass({RestTemplate.class})
@EnableConfigurationProperties(SmsProperties.class)
@ConditionalOnProperty(prefix = "sms.huawei", name = "sign-name")
public class HuaweiSMSconfiguration {

    @Bean
    public SmsSender huaweiSmsSender(SmsProperties smsProperties) {
        SmsProperties.Huawei huawei = smsProperties.getHuawei();
        RestTemplate restTemplate = new RestTemplate();
        return new HuaweiSmsSender(restTemplate, huawei);
    }
}