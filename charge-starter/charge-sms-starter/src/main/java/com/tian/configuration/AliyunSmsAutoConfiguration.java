package com.tian.configuration;

import com.aliyuncs.IAcsClient;
import com.tian.SmsProperties;
import com.tian.sender.AliSmsSender;
import com.tian.sender.SmsSender;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年07月20日 10:33
 * 在线刷题 1200+题和1000+篇干货文章：<a href="http://woaijava.cc/">博客地址</a>
 *
 * 对接阿里云短信发送
 */
@ConditionalOnProperty(prefix = "sms.aliyun",name = "sign-name")
//条件装载  必须IAcsClient.class在我们的classpath目录下才装载
@ConditionalOnClass(IAcsClient.class)
@EnableConfigurationProperties(SmsProperties.class)
@Configuration
public class AliyunSmsAutoConfiguration {
    @Bean
    public SmsSender aliyunSmsSender(SmsProperties smsProperties) {
        SmsProperties.Aliyun aliyun = smsProperties.getAliyun();
        return new AliSmsSender( aliyun.getSignName(), aliyun.getAccessKeyId(), aliyun.getAccessKeySecret());
    }
}
