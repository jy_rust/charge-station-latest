package com.tian.configuration;

import com.baidubce.services.sms.SmsClient;
import com.tian.SmsProperties;
import com.tian.sender.BaiduSmsSender;
import com.tian.sender.SmsSender;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author tianwc  公众号：java后端技术全栈、面试专栏
 * @version 1.0.0
 * @date 2023年07月20日 15:34
 * 在线刷题 1200+题和1000+篇干货文章：<a href="http://woaijava.cc/">博客地址</a>
 *
 * 对接百度短信发送
 */
@Configuration(proxyBeanMethods = false)
@ConditionalOnClass({SmsClient.class})
@EnableConfigurationProperties(SmsProperties.class)
@ConditionalOnProperty(prefix = "sms.baidu",name = "signature-id")
public class BaiduSMSConfiguration {

    @Bean
    public SmsSender baiduSmsSender(SmsProperties smsProperties) {
        SmsProperties.Baidu baidu = smsProperties.getBaidu();
        return new BaiduSmsSender( baidu.getAccessKeyId(), baidu.getSecretAccessKey(), baidu.getSignatureId());
    }
}